<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group( [ 'prefix' => 'admin', 'middleware' => [ 'auth' ] ], function () {

    Route::prefix( 'property' )->group( function() {
        Route::get( '/', 'PropertyController@index' )->name('property_index');
        Route::get( '/all', 'PropertyController@index' );
        Route::get( '/add', 'PropertyController@create' );
        Route::post( '/store', 'PropertyController@store' );
        Route::get( '/{id}', 'PropertyController@show' );
        Route::get( '/edit/{id}', 'PropertyController@edit' );
        Route::post( '/update/{id}', 'PropertyController@update' );
        Route::post( '/trash/{id}', 'PropertyController@destroy' );
    });

    Route::prefix( 'property-type' )->group( function() {
        Route::get( '/', 'PropertyTypeController@index' )->name('property-type-index');
        Route::get( '/all', 'PropertyTypeController@index' );
        Route::get( '/add', 'PropertyTypeController@create' );
        Route::post( '/store', 'PropertyTypeController@store' );
        Route::get( '/{id}', 'PropertyTypeController@show');
        Route::get( '/edit/{id}', 'PropertyTypeController@edit' );
        Route::post( '/update/{id}', 'PropertyTypeController@update' );
        Route::post( '/trash/{id}', 'PropertyTypeController@destroy' )->name('property-type-destroy');
    });

    Route::prefix( 'property-group' )->group( function() {
        Route::get( '/', 'PropertyGroupController@index' )->name('property-group-index');
        Route::get( '/all', 'PropertyGroupController@index' );
        Route::get( '/add', 'PropertyGroupController@create' );
        Route::post( '/store', 'PropertyGroupController@store' );
        Route::get( '/{id}', 'PropertyGroupController@show' );
        Route::get( '/edit/{id}', 'PropertyGroupController@edit' );
        Route::post( '/update/{id}', 'PropertyGroupController@update' );
        Route::post( '/trash/{id}', 'PropertyGroupController@destroy' )->name('property-group-destroy');
    });

    Route::prefix( 'property-tag' )->group( function() {
        Route::get( '/', 'PropertyTagController@index' )->name('property-tag-index');
        Route::get( '/all', 'PropertyTagController@index' );
        Route::get( '/add', 'PropertyTagController@create' );
        Route::post( '/store', 'PropertyTagController@store' );
        Route::get( '/{id}', 'PropertyTagController@show' );
        Route::get( '/edit/{id}', 'PropertyTagController@edit' );
        Route::post( '/update/{id}', 'PropertyTagController@update' );
        Route::post( '/trash/{id}', 'PropertyTagController@destroy' );
    });
});
