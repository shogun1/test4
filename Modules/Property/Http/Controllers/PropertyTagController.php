<?php

namespace Modules\Property\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Property\Entities\PropertyTag;
use Illuminate\Support\Facades\DB;
use Exception;

class PropertyTagController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $record = $request->record ?? 10;
        $search = $request->search ?? null;
        $data = PropertyTag::where('status','<>','trash');
        if(!empty($search)){
            $data = $data->where('title','like',$search);
        }
        $data = $data->paginate($record);
        return view('theme::admin.property.tag.all',compact('record','search'))->with('datas',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('property::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{

            $data = [
                'title' => $request->title,
                'slug' => $request->slug,
                'author' => $request->author,
                'status' => $request->status
            ];
            
            PropertyTag::insert($data);
            DB::commit();
            return true;
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $propertyTag = PropertyTag::findOrFail($id);
        return $propertyTag;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $propertyTag = PropertyTag::findOrFail($id);
        return view('theme::admin.property.tag.all',compact('propertyTag'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $propertyTag = PropertyTag::findOrFail($id);
            $propertyTag->update([
                'title' => $request->title,
                'slug' => $request->slug,
                'author' => $request->author,
                'status' => $request->status
            ]);
            DB::commit();
            return true;
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $propertyTag = PropertyTag::find($id)->update(['status' => 'trash']);
        return true;
    }
}
