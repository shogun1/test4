<?php

namespace Modules\Property\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Exception;
use Modules\Property\Entities\PropertyGroup;

class PropertyGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $record = $request->record ?? 10;
        $search = $request->search ?? null;
        $data = PropertyGroup::where('status','<>','trash');
        if(!empty($search)){
            $data = $data->where('title','like',$search);
        }
        $data = $data->paginate($record);
        return view('theme::admin.property.group.all',compact('record','search'))->with('datas',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('property::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{

            $data = [
                'title' => $request->title,
                'slug' => $request->slug,
                'parent' => $request->parent,
                'description' => $request->description,
                'author' => $request->author,
                'status' => $request->status
            ];
            
            PropertyGroup::insert($data);
            DB::commit();
            return true;
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $propertyGroup = PropertyGroup::findOrFail($id);
        return $propertyGroup;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $propertyGroup = PropertyGroup::findOrFail($id);
        return view('theme::admin.property.group.all',compact('propertyGroup'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
        $propertyGroup = PropertyGroup::findOrFail($id);
        $propertyGroup->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'parent' => $request->parent,
            'description' => $request->description,
            'author' => $request->author,
            'status' => $request->status
        ]);
        DB::commit();
        return true;
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $propertyGroup = PropertyGroup::find($id)->update(['status' => 'trash']);
        return true;
    }
}
