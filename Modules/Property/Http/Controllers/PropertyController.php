<?php

namespace Modules\Property\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Database\Query\Expression;
use Modules\Property\Entities\Property;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = Property::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        DB::beginTransaction();
        try{
            $input = Request::all();
            Property::created($input);
            DB::commit();
            return $input;
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{          
            Property::create([
                'title' => $request->title,
                'slug' => $request->slug,
                'post_code' => $request->post_code,
                'post_membership_level' => $request->post_membership_level,
                'post_membership_expiration' => $request->post_membership_expiration,
                'excerpt' => $request->excerpt,
                'tags' => $request->tags,
                'content' => $request->content,
                'city' => $request->city,
                'district' => $request->district,
                'ward' => $request->ward,
                'address' => $request->address,
                'map_lng' => $request->map_lng,
                'map_lat' => $request->map_lat,
                'thumbnail_image_id' => $request->thumbnail_image_id,
                'gallery_image_ids' => $request->gallery_image_ids,
                'video_ids' => $request->video_ids,
                'price_value' => $request->price_value,
                'price_unit' => $request->price_unit,
                'square_root' => $request->square_root,
                'floors' => $request->floors,
                'bedrooms' => $request->bedrooms,
                'toilets' => $request->toilets,
                'balcony_direction' => $request->balcony_direction,
                'front_door_direction' => $request->front_door_direction,
                'property_facade' => $request->property_facade,
                'property_status' => $request->property_status,
                'legal_status' => $request->legal_status,
                'property_documents' => $request->property_documents,
                'basements' => $request->basements,
                'utilities' => $request->utilities,
                'furniture' => $request->furniture,
                'devices' => $request->devices,
                'property_type_id' => $request->property_type_id,
                'property_group_id' => $request->property_group_id,
                'project_id' => $request->project_id,
                'agent_id' => $request->agent_id,
                'author' => $request->author,
                'status' => $request->status
            ]);

            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $property = Property::findOrFail($id);
        return view('property::show',compact('property'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $property = Property::findOrFail($id);
        return view('property::edit',compact('property'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
        $property = Property::findOrFail($id);
        $property->update([
                'title' => $request->title,
                'slug' => $request->slug,
                'post_code' => $request->post_code,
                'post_membership_level' => $request->post_membership_level,
                'post_membership_expiration' => $request->post_membership_expiration,
                'excerpt' => $request->excerpt,
                'tags' => $request->tags,
                'content' => $request->content,
                'city' => $request->city,
                'district' => $request->district,
                'ward' => $request->ward,
                'address' => $request->address,
                'map_lng' => $request->map_lng,
                'map_lat' => $request->map_lat,
                'thumbnail_image_id' => $request->thumbnail_image_id,
                'gallery_image_ids' => $request->gallery_image_ids,
                'video_ids' => $request->video_ids,
                'price_value' => $request->price_value,
                'price_unit' => $request->price_unit,
                'square_root' => $request->square_root,
                'floors' => $request->floors,
                'bedrooms' => $request->bedrooms,
                'toilets' => $request->toilets,
                'balcony_direction' => $request->balcony_direction,
                'front_door_direction' => $request->front_door_direction,
                'property_facade' => $request->property_facade,
                'property_status' => $request->property_status,
                'legal_status' => $request->legal_status,
                'property_documents' => $request->property_documents,
                'basements' => $request->basements,
                'utilities' => $request->utilities,
                'furniture' => $request->furniture,
                'devices' => $request->devices,
                'property_type_id' => $request->property_type_id,
                'property_group_id' => $request->property_group_id,
                'project_id' => $request->project_id,
                'agent_id' => $request->agent_id,
                'author' => $request->author,
                'status' => $request->status
        ]);
        DB::commit();
        return redirect($this->redirectPath());
        }catch(Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $Property = Property::find($id);
        $Property->update(['status' => 'trash']);
        return redirect(route('property_index'))->with('SuccessMessage', "Delete Success");
    }
}
