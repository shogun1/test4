let PropertyPost = {};

PropertyPost.savePost = function() {

    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxUrl = thisForm.attr( 'action' );
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();
        formData.author = $( '#post-author', thisForm ).val();
        formData.status = $( '#post-status', thisForm ).val();
        formData.description = $( '#post-description', thisForm ).val();

        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: ajaxUrl,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: PropertyPost.savePost()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    location.reload();
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }


};

$( document ).on( 'click', 'button.save-post-type', function() {
    PropertyPost.savePost();
});

PropertyPost.deletePost = function(id,token) {
   /** Setup CSRF Token in header */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });

    $.ajax({
        url: '/admin/property-type/trash/'+id,
        type: 'POST',
        dataType: 'json',
        data: {
            "id": id
        },
        beforeSend: function( jqXHR, settings ) {

        },
        error: function( jqXHR, textStatus, errorThrown ) {
            console.log( '### TASK: PropertyPost.savePost()' );
            console.log( 'textStatus: ' + textStatus );
            console.log( 'errorThrown: ' + errorThrown );
            console.log( jqXHR.getAllResponseHeaders() );
        },
        success: function( response ) {
            location.reload();
        }
    });

};
$( document ).on('click', 'button#delete-post', function() {
    if(confirm('Are you sure ?')){
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
        PropertyPost.deletePost(id,token);
    }
});

PropertyPost.getPost = function(id,token) {
    /** Setup CSRF Token in header */
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': token
         }
     });
 
     $.ajax({
         url: '/admin/property-type/'+id,
         type: 'GET',
         dataType: 'json',
         data: {
             "id": id
         },
         beforeSend: function( jqXHR, settings ) {
 
         },
         error: function( jqXHR, textStatus, errorThrown ) {
             console.log( '### TASK: PropertyPost.savePost()' );
             console.log( 'textStatus: ' + textStatus );
             console.log( 'errorThrown: ' + errorThrown );
             console.log( jqXHR.getAllResponseHeaders() );
         },
         success: function( response ) {
            $('#post-title').val(response['title']);
            $('#post-slug').val(response['slug']);
            $('#post-author').val(response['author']);
            $('#post-status').val(response['status']);
            $('#post-description').val(response['description']);
            $('#post-id').val(response['id']);
            $('#save-post').css('display','none');
            $('#save-edit').css('display','block');
         }
     });
 };

$( document ).on( 'click', 'button#edit-post', function() {
    var id = $(this).data("id");
    PropertyPost.getPost(id);
});

PropertyPost.savePostEdit = function() {
    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        var id = formData.id = $( '#post-id', thisForm ).val();
        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();
        formData.author = $( '#post-author', thisForm ).val();
        formData.status = $( '#post-status', thisForm ).val();
        formData.description = $( '#post-description', thisForm ).val();

        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: 'update/'+id,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: PropertyPost.savePost()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    location.reload();
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }
};

$( document ).on( 'click', 'button#save-edit', function() {
    PropertyPost.savePostEdit();
});



