let PropertyPostTag = {};

PropertyPostTag.savePost = function() {
    
    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxUrl = thisForm.attr( 'action' );
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();
        formData.status = $( '#post-status', thisForm ).val();
        formData.author = $( '#post-author', thisForm ).val();

        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: ajaxUrl,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: PropertyPostTag.savePost()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    location.reload();
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }


};

$( document ).on( 'click', 'button.save-post-tag', function() {
    PropertyPostTag.savePost();
});

PropertyPostTag.deletePost = function(id,token) {
   /** Setup CSRF Token in header */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });

    $.ajax({
        url: '/admin/property-tag/trash/'+id,
        type: 'POST',
        dataType: 'json',
        data: {
            "id": id
        },
        beforeSend: function( jqXHR, settings ) {

        },
        error: function( jqXHR, textStatus, errorThrown ) {
            console.log( '### TASK: PropertyPostTag.deletePost()' );
            console.log( 'textStatus: ' + textStatus );
            console.log( 'errorThrown: ' + errorThrown );
            console.log( jqXHR.getAllResponseHeaders() );
        },
        success: function( response ) {
            location.reload();
        }
    });

};
$( document ).on('click', 'button#delete-post-tag', function() {
    if(confirm('Are you sure ?')){
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
        PropertyPostTag.deletePost(id,token);
    }
});

PropertyPostTag.getPost = function(id,token) {
    /** Setup CSRF Token in header */
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': token
         }
     });
 
     $.ajax({
         url: '/admin/property-tag/'+id,
         type: 'GET',
         dataType: 'json',
         data: {
             "id": id
         },
         beforeSend: function( jqXHR, settings ) {
 
         },
         error: function( jqXHR, textStatus, errorThrown ) {
             console.log( '### TASK: PropertyPostTag.getPost()' );
             console.log( 'textStatus: ' + textStatus );
             console.log( 'errorThrown: ' + errorThrown );
             console.log( jqXHR.getAllResponseHeaders() );
         },
         success: function( response ) {
            $('#post-title').val(response['title']);
            $('#post-slug').val(response['slug']);
            $('#post-author').val(response['author']);
            $('#post-status').val(response['status']);
            $('#post-id').val(response['id']);

            $('#save-post').css('display','none');
            $('#save-edit-tag').css('display','block');
         }
     });
 };

$( document ).on( 'click', 'button#edit-post-tag', function() {
    var id = $(this).data("id");
    PropertyPostTag.getPost(id);
});

PropertyPostTag.savePostEdit = function() {
    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        var id = formData.id = $( '#post-id', thisForm ).val();

        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();
        formData.status = $( '#post-status', thisForm ).val();
        formData.author = $( '#post-author', thisForm ).val();

        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: 'update/'+id,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: PropertyPostTag.savePostEdit()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    location.reload();
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }
};

$( document ).on( 'click', 'button#save-edit-tag', function() {
    PropertyPostTag.savePostEdit();
});
