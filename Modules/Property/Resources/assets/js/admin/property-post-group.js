let PropertyPostGroup = {};

PropertyPostGroup.savePost = function() {
    
    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxUrl = thisForm.attr( 'action' );
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();
        formData.parent = $( '#post-parent', thisForm ).val();
        formData.status = $( '#post-status', thisForm ).val();
        formData.author = $( '#post-author', thisForm ).val();
        formData.description = $('#post-description', thisForm ).val();

        console.log(formData);
        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: ajaxUrl,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: PropertyPostGroup.savePost()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    location.reload();
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }


};

$( document ).on( 'click', 'button.save-post-group', function() {
    PropertyPostGroup.savePost();
});

PropertyPostGroup.deletePost = function(id,token) {
   /** Setup CSRF Token in header */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });

    $.ajax({
        url: '/admin/property-group/trash/'+id,
        type: 'POST',
        dataType: 'json',
        data: {
            "id": id
        },
        beforeSend: function( jqXHR, settings ) {

        },
        error: function( jqXHR, textStatus, errorThrown ) {
            console.log( '### TASK: PropertyPostGroup.deletePost()' );
            console.log( 'textStatus: ' + textStatus );
            console.log( 'errorThrown: ' + errorThrown );
            console.log( jqXHR.getAllResponseHeaders() );
        },
        success: function( response ) {
            location.reload();
        }
    });

};
$( document ).on('click', 'button#delete-post-type', function() {
    if(confirm('Are you sure ?')){
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
        PropertyPostGroup.deletePost(id,token);
    }
});

PropertyPostGroup.getPost = function(id,token) {
    /** Setup CSRF Token in header */
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': token
         }
     });
 
     $.ajax({
         url: '/admin/property-group/'+id,
         type: 'GET',
         dataType: 'json',
         data: {
             "id": id
         },
         beforeSend: function( jqXHR, settings ) {
 
         },
         error: function( jqXHR, textStatus, errorThrown ) {
             console.log( '### TASK: PropertyPostGroup.getPost()' );
             console.log( 'textStatus: ' + textStatus );
             console.log( 'errorThrown: ' + errorThrown );
             console.log( jqXHR.getAllResponseHeaders() );
         },
         success: function( response ) {
            $('#post-title').val(response['title']);
            $('#post-slug').val(response['slug']);
            $('#post-parent').val(response['parent']);
            $('#post-author').val(response['author']);
            $('#post-status').val(response['status']);
            $('#post-description').val(response['description']);
            $('#post-id').val(response['id']);

            $('#save-post-type').css('display','none');
            $('#save-edit-type').css('display','block');
         }
     });
 };

$( document ).on( 'click', 'button#edit-post-type', function() {
    var id = $(this).data("id");
    PropertyPostGroup.getPost(id);
});

PropertyPostGroup.savePostEdit = function() {
    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        var id = formData.id = $( '#post-id', thisForm ).val();

        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();
        formData.parent = $( '#post-parent', thisForm ).val();
        formData.status = $( '#post-status', thisForm ).val();
        formData.author = $( '#post-author', thisForm ).val();
        formData.description = $('#post-description', thisForm ).val();

        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: 'update/'+id,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: PropertyPostGroup.savePostEdit()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    location.reload();
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }
};

$( document ).on( 'click', 'button#save-edit-type', function() {
    PropertyPostGroup.savePostEdit();
});
