<?php

namespace Modules\Property\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PropertyTag extends Model
{
    use HasFactory;

    protected $table = 'property_tag';

    protected $fillable = ['id','title','slug','author','status'];

    protected $hidden = ['created_at','updated_at'];
    
    protected static function newFactory()
    {
        return \Modules\Property\Database\factories\PropertyTagFactory::new();
    }
}
