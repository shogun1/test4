<?php

namespace Modules\Property\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PropertyType extends Model
{
    use HasFactory;

    protected $table = 'property_type';

    protected $fillable = ['id','title','slug','descripttion','author','status'];

    protected $hidden = ['created_at','updated_at'];
    
    protected static function newFactory()
    {
        return \Modules\Property\Database\factories\PropertyTypeFactory::new();
    }
}
