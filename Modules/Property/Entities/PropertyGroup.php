<?php

namespace Modules\Property\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PropertyGroup extends Model
{
    use HasFactory;

    protected $table = 'property_group';
    
    protected $fillable = ['id','title','slug','parent','description','author','status'];

    protected $hidden = ['created_at','updated_at'];

    
    protected static function newFactory()
    {
        return \Modules\Property\Database\factories\PropertyGroupFactory::new();
    }
}
