<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {

            $table->id();

            $table->string( 'title' );
            $table->string( 'slug' );
            $table->char( 'post_code', 100 );
            $table->char( 'post_membership_level', 100 );
            $table->date( 'post_membership_expiration' );
            $table->text( 'excerpt' );
            $table->text( 'tags' );
            $table->longText( 'content' );
            $table->string( 'city' );
            $table->string( 'district' );
            $table->string( 'ward' );
            $table->string( 'address' );
            $table->float( 'map_lng' );
            $table->float( 'map_lat' );
            $table->integer( 'thumbnail_image_id' );
            $table->json( 'gallery_image_ids' );
            $table->json( 'video_ids' );
            $table->integer( 'price_value' );
            $table->char( 'price_unit' );
            $table->integer( 'square_root' );
            $table->integer( 'floors' );
            $table->integer( 'bedrooms' );
            $table->integer( 'toilets' );
            $table->text( 'balcony_direction' );
            $table->text( 'front_door_direction' );
            $table->text( 'property_facade' );
            $table->text( 'property_status' );
            $table->text( 'legal_status' );
            $table->text( 'property_documents' );
            $table->text( 'basements' );
            $table->json( 'utilities' );
            $table->json( 'furniture' );
            $table->json( 'devices' );
            $table->integer( 'property_type_id' );
            $table->integer( 'property_group_id' );
            $table->integer( 'project_id' );
            $table->integer( 'agent_id' );
            $table->integer( 'author' );

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
