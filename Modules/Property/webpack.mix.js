const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('public').mergeManifest();

mix.styles(
    [
        __dirname + '/Resources/assets/css/admin/property.css'
    ],
    'public/assets/css/admin/property.css'
);

mix.scripts(
    [
        __dirname + '/Resources/assets/js/admin/property-sections.js',
        __dirname + '/Resources/assets/js/admin/property-post.js',
        __dirname + '/Resources/assets/js/admin/property-post-group.js',
        __dirname + '/Resources/assets/js/admin/property-post-tag.js',
    ],
    'public/assets/js/admin/property.js'
);

if (mix.inProduction()) {
    mix.version();
}
