<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Permissions\Entities\RolesGroup;
use Modules\Permissions\Entities\Roles;

class Departments extends Model
{
    use HasFactory;

    protected $table = "departments";
    protected $fillable = ['name', 'is_delete'];
    
    protected static function newFactory()
    {
        return \Modules\Permissions\Database\factories\DepartmentsFactory::new();
    }

    public function Roles($department_id) {
        $departments = RolesGroup::where('department_id', $department_id)->pluck('role_id')->toArray();
        return Roles::whereIn('id', $departments)->where('is_delete', 0)->get();
    }
}
