<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RolesGroup extends Model
{
    use HasFactory;

    protected $table = "roles_group";
    protected $fillable = ['department_id', 'role_id'];
    
    protected static function newFactory()
    {
        return \Modules\Permissions\Database\factories\RolesGroupFactory::new();
    }
}
