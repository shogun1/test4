<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use DB;

class Roles extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $table = "roles";
    protected $fillable = ['name', 'slug', 'type', 'sponsor_slug', 'is_delete'];

    public function Users() {
        return $this->belongsTo(User::class, 'id', 'role');
    }
    
    protected static function newFactory()
    {
        return \Modules\Permissions\Database\factories\RolesFactory::new();
    }
}
