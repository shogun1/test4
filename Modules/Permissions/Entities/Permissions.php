<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Permissions extends Model
{
    use HasFactory;

    protected $table = "permissions";
    protected $fillable = ['role_id', 'module'];
    
    protected static function newFactory()
    {
        return \Modules\Permissions\Database\factories\PermissionsFactory::new();
    }
}
