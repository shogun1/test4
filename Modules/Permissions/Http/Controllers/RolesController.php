<?php

namespace Modules\Permissions\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Permissions\Entities\Permissions;
use Modules\Permissions\Entities\Roles;
use App\User;
use Gate;
use Auth;

class RolesController extends Controller
{
    use ValidatesRequests;
    public function index()
    {
        Gate::allows('modules', 'roles_access');

        $data = [
            'role_all' => Auth::user()->permission == 'super-admin' ? 1 : 0,
            'roles' => Roles::where('is_delete', 0)->orderByDESC('id')->paginate(10),
        ];

        return view('permissions::admin.roles.index', $data);
    }

    public function getAdd()
    {
        Gate::allows('modules', 'roles_get_add');

        $modules = config('permissions.modules');

        $data = [
            'modules' => $modules,
        ];

        return view('permissions::admin.roles.add', $data);
    }

    public function postAdd(Request $request)
    {
        Gate::allows('modules', 'roles_post_add');

        $this->validate($request,
        [
            'name' => 'required|unique:roles,name',
            'permissions' => 'required',
        ]);

        $role = Roles::create([
            'name' => $request->name,
            'slug' => checkSlug_exists('roles', $request->name),
        ]);

        foreach($request->permissions as $value) {
            if(!is_null($value)) {
                Permissions::create([
                    'role_id' => $role->id,
                    'module' => $value,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }

        return redirect()->route('admin.roles')->with('alert_success', 'Thêm quyền thành công.');
    }

    public function getEdit($id) {
        Gate::allows('modules', 'roles_get_edit');

        $role = Roles::where('id', $id)->where('is_delete', 0)->firstOrFail();
        if($role->slug == 'super-admin') {
            return redirect()->route('admin.roles')->with('alert_success', 'Supper admin được sử dụng toàn quyền');
        }
        $data = [
            'role' => $role,
            'modules' => config('permissions.modules'),
            'module' => Permissions::where('role_id', $role->id)->pluck('module')->toArray(),
        ];

        return view('permissions::admin.roles.edit', $data);
    }

    public function postEdit(Request $request, $id) {
        Gate::allows('modules', 'roles_post_edit');

        $role = Roles::findOrFail($id);

        $this->validate($request,
        [
            'name' => 'required|unique:roles,name,'.$role->id,
            'permissions' => 'required',
        ]);

        $role->update([
            'name' => $request->name,
            'slug' => checkSlug_exists('roles', $request->name, $role->id),
        ]);
        Permissions::where('role_id', $role->id)->delete();

        foreach($request->permissions as $value) {
            if(!is_null($value)) {
                Permissions::create([
                    'role_id' => $role->id,
                    'module' => $value,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }

        return redirect()->route('admin.roles')->with('alert_success', 'Cập nhật quyền thành công.');
    }

    public function getDelete($id) {
        Gate::allows('modules', 'roles_delete');

        $role = Roles::findOrFail($id);
        $user = User::where('permission', $role->slug)->first();
        // $position = Roles::where('slug', '!=', $department->slug)->where('sponsor_slug', 'LIKE', '%' . $department->slug . '%')->first();
        if($user) {
            return redirect()->route('admin.departments')->with('alert_error', 'User is authorized '.$department->name);
        }
        $role->update([
            'is_delete' => 1
        ]);

        return redirect()->route('admin.roles')->with('alert_success', 'Delete role successfully');
    }

    public function getTrash() {
        Gate::allows('modules', 'departments_delete');

        $roles = Roles::where('is_delete', 1)->orderBy('created_at', 'desc')->paginate('10');

        $data = [
            'roles' => $roles,
        ];

        return view('permissions::admin.roles.trash', $data);
    }

    public function getRestore($id) {
        Gate::allows('modules', 'departments_delete');

        $role = Roles::where('id', $id)->where('is_delete', 1)->firstOrFail();
        $role->update([
            'is_delete' => 0
        ]);

        return redirect()->back()->with('alert_success', 'Role Restore successful.');
    }
}
