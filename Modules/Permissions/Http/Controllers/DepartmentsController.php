<?php

namespace Modules\Permissions\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Permissions\Entities\Permissions;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Modules\Permissions\Entities\Roles;
use Modules\Permissions\Entities\Departments;
use Modules\Permissions\Entities\RolesGroup;
use App\User;
use DB;
use Str;
use Auth;
use Session;
use Gate;

class DepartmentsController extends Controller
{
    use ValidatesRequests;
    public function index()
    {
        Gate::allows('modules', 'departments_access');

        $data = [
            'departments' => Departments::where('is_delete', 0)->orderByDESC('id')->paginate(10),
        ];

        return view('permissions::admin.departments.index', $data);
    }

    public function getAdd()
    {
        Gate::allows('modules', 'departments_get_add');

        $roles_group = RolesGroup::pluck('role_id')->toArray();

        $data = [
            'roles' => Roles::whereNotIn('id', $roles_group)->where('is_delete', 0)->orderByDESC('id')->get(),
        ];

        return view('permissions::admin.departments.add', $data);
    }

    public function postAdd(Request $request)
    {
        Gate::allows('modules', 'departments_post_add');

        $this->validate($request,
        [
            'name' => 'required|unique:roles,name',
            'roles' => 'required',
        ]);

        $roles = Roles::whereIn('id', $request->roles)->pluck('id')->toArray();

        $department =Departments::create([
            'name' => $request->name,
            'is_delete' => 0
        ]);

        foreach($roles as $role) {
            $checkRole = RolesGroup::where('role_id', $role)->first();
            if(!$checkRole) {
                RolesGroup::create([
                    'department_id' => $department->id,
                    'role_id' => $role
                ]);
            }
        }

        return redirect()->route('admin.departments')->with('alert_success', 'Add department successfully');
    }

    public function getEdit($id) {
        Gate::allows('modules', 'departments_get_edit');

        $department = Departments::where('id', $id)->where('is_delete', 0)->firstOrFail();
        $roles_group = RolesGroup::where('department_id', '!=', $department->id)->pluck('role_id')->toArray();
        
        
        $data = [
            'department' => $department,
            'roles' => Roles::whereNotIn('id', $roles_group)->where('is_delete', 0)->get(),
            'roles_check' => RolesGroup::where('department_id', $department->id)->pluck('role_id')->toArray(),
        ];

        return view('permissions::admin.departments.edit', $data);
    }

    public function postEdit(Request $request, $id) {
        Gate::allows('modules', 'departments_post_edit');

        $department = Departments::findOrFail($id);

        $this->validate($request,
        [
            'name' => 'required|unique:roles,name,'.$department->id,
        ]);

        $department->update([
            'name' => $request->name,
        ]);
        if($request->roles == '') {
            RolesGroup::where('department_id', $department->id)->delete();
            return redirect()->route('admin.departments')->with('alert_success', 'Add department successfully');
        }

        $roles = Roles::whereIn('id', $request->roles)->pluck('id')->toArray();
        RolesGroup::where('department_id', $department->id)->delete();
        foreach($roles as $role) {
            $checkRole = RolesGroup::where('role_id', $role)->first();
            if(!$checkRole) {
                RolesGroup::create([
                    'department_id' => $department->id,
                    'role_id' => $role
                ]);
            }
        }

        return redirect()->route('admin.departments')->with('alert_success', 'Add department successfully');
    }

    public function getDelete($id) {
        Gate::allows('modules', 'departments_delete');

        $department = Departments::where('id', $id)->where('is_delete', 0)->firstOrFail();
        
        RolesGroup::where('department_id', $department->id)->delete();
        $department->delete();

        return redirect()->route('admin.departments')->with('alert_success', 'Delete department successfully');
    }
}
