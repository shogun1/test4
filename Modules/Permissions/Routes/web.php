<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::group(['prefix' => 'roles'], function() {
        Route::get('/', 'RolesController@index')->name('admin.roles');
        Route::group(['prefix' => 'add'], function() {
            Route::get('', 'RolesController@getAdd')->name('admin.roles.add');
            Route::post('', 'RolesController@postAdd');
        });
        Route::group(['prefix' => 'edit'], function() {
            Route::get('/{id}', 'RolesController@getEdit')->name('admin.roles.edit');
            Route::post('/{id}', 'RolesController@postEdit');
        });

        Route::get('/delete/{id}', 'RolesController@getDelete')->name('admin.roles.delete');

        Route::get('/trash', 'RolesController@getTrash')->name('admin.roles.trash');
        Route::get('/restore/{id}', 'RolesController@getRestore')->name('admin.roles.restore');
        Route::get('/deleteTrash/{id}', 'RolesController@deleteTrash')->name('admin.roles.deleteTrash');
    });

    Route::group(['prefix' => 'departments'], function () {
        Route::get('/', 'DepartmentsController@index')->name('admin.departments');
        Route::group(['prefix' => 'add'], function() {
            Route::get('', 'DepartmentsController@getAdd')->name('admin.departments.add');
            Route::post('', 'DepartmentsController@postAdd');
        });

        Route::group(['prefix' => 'edit'], function() {
            Route::get('/{id}', 'DepartmentsController@getEdit')->name('admin.departments.edit');
            Route::post('/{id}', 'DepartmentsController@postEdit');
        });

        Route::get('/delete/{id}', 'DepartmentsController@getDelete')->name('admin.departments.delete');
    });
});