@extends('admin.app')
@push('css')
<link href="{{ asset('assets\libs\bootstrap-tagsinput\bootstrap-tagsinput.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        {{-- <div class="page-title-right">
                            <ol class="breadcrumb p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">batdongsan24h.vn</a></li>
                                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                <li class="breadcrumb-item active">General Elements</li>
                            </ol>
                        </div> --}}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @include('admin.boxes.notify')

            <!-- Form row -->
            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <form method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Detail Department</label>
                                    <input type="text" class="form-control" id="title" value="{{ $role->name }}"
                                        name="name" placeholder="Name permission">
                                </div>
                                <div class="form-group">
                                    <label>List Position</label>
                                    <div class="mb-0">
                                        <select multiple="" data-role="tagsinput">
                                            @foreach (json_decode($role->sponsor_slug) as $key => $value)
                                                <option value="{{ $value }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                        <thead>
                                            <th>{{ __('Module name') }}</th>
                                            <th>{{ __('View') }}</th>
                                            <th>{{ __('Get Add') }}</th>
                                            <th>{{ __('Post Add') }}</th>
                                            <th>{{ __('Get Edit') }}</th>
                                            <th>{{ __('Post Edit') }}</th>
                                            <th>{{ __('Delete') }}</th>
                                        </thead>
                                        <tbody>
                                            @foreach($modules as $key => $value)
                                            <tr>
                                                <td><i>{{ $value }}</i></td>
                                                <td>
                                                    <input type="checkbox" name="permissions[]" {{ in_array($value . '_access', $permissions) ? 'checked' : '' }} value="{{ $value . '_access' }}">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="permissions[]" {{ in_array($value . '_get_add', $permissions) ? 'checked' : '' }} value="{{ $value . '_get_add' }}">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="permissions[]" {{ in_array($value . '_post_add', $permissions) ? 'checked' : '' }} value="{{ $value . '_post_add' }}">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="permissions[]" {{ in_array($value . '_get_edit', $permissions) ? 'checked' : '' }} value="{{ $value . '_get_edit' }}">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="permissions[]" {{ in_array($value . '_post_edit', $permissions) ? 'checked' : '' }} value="{{ $value . '_post_edit' }}">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="permissions[]" {{ in_array($value . '_delete', $permissions) ? 'checked' : '' }} value="{{ $value . '_delete' }}">
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end col -->

            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>
@endsection
@push('js')
<script src="{{ asset('assets\libs\bootstrap-tagsinput\bootstrap-tagsinput.min.js') }}"></script>
@endpush