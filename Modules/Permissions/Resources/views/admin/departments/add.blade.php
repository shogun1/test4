@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Add Department</h4>
                        {{-- <div class="page-title-right">
                            <ol class="breadcrumb p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">batdongsan24h.vn</a></li>
                                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                <li class="breadcrumb-item active">General Elements</li>
                            </ol>
                        </div> --}}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @include('admin.boxes.notify')

            <!-- Form row -->
            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <form method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Right name</label>
                                    <input type="text" class="form-control" id="title" value="{{ old('name') }}"
                                        name="name" placeholder="Name department">
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                        <thead>
                                            <th></th>
                                            <th>{{ __('Name') }}</th>
                                        </thead>
                                        <tbody>
                                            @foreach($roles as $value)
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="roles[]" class="roles" value="{{ $value->id }}">
                                                </td>
                                                <td><i>{{ $value->name }}</i></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <button type="submit" class="btn btn-primary text-right">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end col -->

            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    // $(document).ready(function() {
    //     var list_department = [];

    //     $('.department').click(function() {
    //         if ($(this).prop('checked')==true){
    //             var checked = ($(this).val());
    //             var value_department = jQuery.parseJSON(checked)
    //             list_department.push(value_department);
    //         } else {
    //             list_department.splice($.inArray(checked, list_department),1);

    //         }

    //         var arrToConvert = list_department;
    //         var newArr = [];
    //         for(var i = 0; i < arrToConvert.length; i++) {
    //             newArr = newArr.concat(arrToConvert[i]);
    //         }

    //         $('.department').show()
    //         for (let i = 0; i < newArr.length; i++) {
    //             $('#'+newArr[i]+'').hide();
    //         }
    //     });
    // });
</script>
