@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Trash Permission</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @include('admin.boxes.notify')
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                    <thead>
                                        <tr>
                                            <th>{{ __('Name of management rights') }}</th>
                                            <th>{{ __('Time to update') }}</th>
                                            <th>{{ __('Restore') }}</th>
                                            <th>{{ __('Delete') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($roles) > 0)
                                        @foreach ($roles as $value)
                                            <tr>
                                                <td><a href="{{ route('admin.departments.edit', ['id' => $value->id]) }}"
                                                    class="text-primary">{{$value->name}}</a></td>
                                                <td>{{ $value->updated_at }}</td>
                                                <td style="white-space: nowrap; width: 1%;">
                                                    <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                                        <div class="btn-group btn-group-sm" style="float: none;"><a
                                                                href="{{ route('admin.departments.restore', ['id' => $value->id]) }}"
                                                                onclick="return confirm('Are you sure to restore the permission ?')"><button
                                                                    type="button"
                                                                    class="tabledit-edit-button btn btn-primary"
                                                                    style="float: none;"><span
                                                                        class="mdi mdi-restore"></span></button></a></div>
                                                    </div>
                                                </td>
                                                <td style="white-space: nowrap; width: 1%;">
                                                    <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                                        <div class="btn-group btn-group-sm" style="float: none;"><a
                                                                href="{{ route('admin.departments.deleteTrash', ['id' => $value->id]) }}"
                                                                onclick="return confirm('Are you sure to delete the permission ?')"><button
                                                                    type="button"
                                                                    class="tabledit-edit-button btn btn-danger"
                                                                    style="float: none;"><span
                                                                        class="mdi mdi-delete"></span></button></a></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3" class="text-center">{{ __('Items not found.') }}</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- end .table-responsive-->
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->
</div>
@endsection
