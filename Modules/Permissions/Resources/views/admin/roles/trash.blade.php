@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">All Roles Trash</h4>
                        <a href="{{ route('admin.roles') }}"
                            class="button-title float-right btn btn-outline-primary waves-effect width-md waves-light">All Roles</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @include('admin.boxes.notify')
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                    <thead>
                                        <tr>
                                            <th>Name Roles</th>
                                            <th>Time To Update</th>
                                            <th>Restore</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($roles) > 0)
                                        @foreach ($roles as $value)
                                            <tr>
                                                <td><a href="{{ route('admin.roles.edit', ['id' => $value->id]) }}"
                                                    class="text-primary">{{$value->name}}</a></td>
                                                <td>{{ $value->updated_at }}</td>
                                                <td style="white-space: nowrap; width: 1%;">
                                                    <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                                        <div class="btn-group btn-group-sm" style="float: none;"><a
                                                                href="{{ route('admin.roles.restore', ['id' => $value->id]) }}"
                                                                onclick="return confirm('Are you sure to restore the role ?')"><button
                                                                    type="button"
                                                                    class="tabledit-edit-button btn btn-primary"
                                                                    style="float: none;"><span
                                                                        class="mdi mdi-restore"></span></button></a></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3" class="text-center">{{ __('Items not found.') }}</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- end .table-responsive-->
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->
</div>
@endsection
