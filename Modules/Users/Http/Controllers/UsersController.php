<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use App\SocialUser;
use Modules\Permissions\Entities\Roles;
use Modules\Policy\Entities\Policy;
use App\Http\Controllers\OpenSource\OpenSource;
use Hash;
use Gate;
use Auth;

class UsersController extends Controller
{
    use ValidatesRequests;  

    public function getProfile()
    {
        $user = Auth::user();
        $data = [
            'social_user' => SocialUser::where('user_id', $user->id)->pluck('site_link', 'site_name')->toArray(),
            'user' => $user,
        ];
        
        return view('users::admin.profile', $data);
    }

    public function postProfile(Request $request) {
        $user = User::find(Auth::id());

        $this->validate($request, [
            'username' => 'required',
            'gender' => 'required',
            'birthday' => 'required|nullable|before:today',
            'address' => 'required',
            'image' => 'image',
        ]);
        if($request->password) {
            $this->validate($request,[
                'password' => 'required|min:6|confirmed',
            ]);
        }

        $gender = ['male', 'female'];
        if(!in_array($request->gender, $gender)) {
            return redirect()->back()->with('alert_error', 'Gender not found');
        }

        $image = $user->image;
        if($request->image) {
            $image = OpenSource::images($request->image);
        }
        
        $data = [
            'username' => $request->username,
            'password' => $request->password ? Hash::make($request->password) : $user->password,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'address' => $request->address,
            'site_facebook' => $request->site_facebook,
            'site_zalo' => $request->site_zalo,
            'site_linkedin' => $request->site_linkedin,
            'image' => $image,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        $user->update($data);

        if($request->site) {
            foreach($request->site as $key => $value) {
                if(!is_null($value)) {
                    $SocialUser = SocialUser::where('user_id', $user->id)->where('site_name', $key)->first();
                    if($SocialUser) {
                        $SocialUser->update([
                            'site_name' => $key,
                            'site_link' => $value
                        ]);
                    }else {
                        SocialUser::insert([
                            'user_id' => $user->id,
                            'site_name' => $key,
                            'site_link' => $value,
                        ]);
                    }
                }
            }
        }

        return redirect()->back()->with('alert_success', 'User updated successfully.');
    }

    public function index(Request $request)
    {
        Gate::allows('modules', 'users_access');

        // $userPermissions = OpenSource::usersPermissions(Auth::id());
        $users = User::orderBy('id', 'desc')->where('is_delete', 0)->paginate('10');
        $data = [
            'users' => $users,
            'roles' => Roles::where('is_delete', 0)->get(),
            'user_status' => user_status(),
            'user_type_status' => user_type_status(),
        ];
        return view('users::admin.index', $data);
    }

    public function getAdd()
    {
        Gate::allows('modules', 'users_get_add');

        $data = [
            'roles_basic' => config('roles'),
            'user_status' => user_status(),
            'roles' => Roles::get(),
            'policy' => Policy::get(),
        ];
        return view('users::admin.add', $data);
    }

    public function postAdd(Request $request)
    {
        Gate::allows('modules', 'users_post_add');
        
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|unique:users,phone|min:10|max:11',
            'gender' => 'required',
            'birthday' => 'required|nullable|before:today',
            'address' => 'required',
            'image' => 'required|image',
            'type' => 'required',
            'status' => 'required|boolean'
        ]);

        $gender = ['male', 'female'];
        if(!in_array($request->gender, $gender)) {
            return redirect()->back()->with('alert_error', 'Gender not found');
        }

        $type = ['member', 'system'];
        if(!in_array($request->type, $type)) {
            return redirect()->back()->with('alert_error', 'Type user not found');
        }

        if($request->type == 'member') {
            $policy = Policy::where('id', $request->policy)->first();
            if(!$policy) {
                return redirect()->back()->with('alert_error', 'Policy not found'); 
            }
            $type = 'member';
            $role = null;
            $policy = $request->policy;
        }else{
            $role = Roles::where('slug', $request->role)->first();
            if(!$role) {
                return redirect()->back()->with('alert_error', 'Role not found'); 
            }
            $type = 'system';
            $role = $role->id;
            $policy = null;
        }
        $image = OpenSource::images($request->image);
        
        $data = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'address' => $request->address,
            'site_facebook' => $request->site_facebook,
            'site_zalo' => $request->site_zalo,
            'site_linkedin' => $request->site_linkedin,
            'image' => $image,
            'type' => $type,
            'role' => $role,
            'policy_id' => $policy,
            'status' => $request->status,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        $user = User::create($data);

        if($request->site) {
            foreach($request->site as $key => $value) {
                if(!is_null($value)) {
                    SocialUser::insert([
                        'user_id' => $user->id,
                        'site_name' => $key,
                        'site_link' => $value
                    ]);
                }
            }
        }

        return redirect()->route('admin.users')->with('alert_success', 'User created successfully.');
    }

    public function getEdit($id)
    {
        Gate::allows('modules', 'users_get_edit');

        $user = User::findOrFail($id);
        $data = [
            'user' => $user,
            'roles_basic' => config('roles'),
            'user_status' => user_status(),
            'social_user' => SocialUser::where('user_id', $user->id)->pluck('site_link', 'site_name')->toArray(),
            'roles' => Roles::get(),
            'policy' => Policy::get(),
        ];
        
        return view('users::admin.edit', $data);
    }

    public function postEdit(Request $request, $id)
    {
        Gate::allows('modules', 'users_post_edit');

        $user = User::findorFail($id);

        $this->validate($request, [
            'username' => 'required',
            'gender' => 'required',
            'birthday' => 'before:today',
            'image' => 'image',
            'type' => 'required',
            'status' => 'required|boolean'
        ]);
        if($request->password) {
            $this->validate($request,[
                'password' => 'required|min:6|confirmed',
            ]);
        }

        $gender = ['male', 'female'];
        if(!in_array($request->gender, $gender)) {
            return redirect()->back()->with('alert_error', 'Gender not found');
        }

        $type = ['member', 'system'];
        if(!in_array($request->type, $type)) {
            return redirect()->back()->with('alert_error', 'Type user not found');
        }

        if($request->type == 'member') {
            $policy = Policy::where('id', $request->policy)->first();
            if(!$policy) {
                return redirect()->back()->with('alert_error', 'Policy not found'); 
            }
            $type = 'member';
            $role = null;
            $policy = $request->policy;
        }else{
            $role = Roles::where('slug', $request->role)->first();
            if(!$role) {
                return redirect()->back()->with('alert_error', 'Role not found'); 
            }
            $type = 'system';
            $role = $role->id;
            $policy = null;
        }

        $image = $user->image;
        if($request->image) {
            $image = OpenSource::images($request->image);
        }
        
        $data = [
            'username' => $request->username,
            'password' => $request->password ? Hash::make($request->password) : $user->password,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'address' => $request->address,
            'site_facebook' => $request->site_facebook,
            'site_zalo' => $request->site_zalo,
            'site_linkedin' => $request->site_linkedin,
            'image' => $image,
            'type' => $type,
            'role' => $role,
            'policy_id' => $policy,
            'status' => $request->status,
            'is_media_manager' => !is_null($request->is_media_manager) || false ? 1 : 0,
            'created_at' => now(),
            'updated_at' => now(),
        ];
        $user->update($data);

        if($request->site) {
            foreach($request->site as $key => $value) {
                if(!is_null($value)) {
                    $SocialUser = SocialUser::where('user_id', $user->id)->where('site_name', $key)->first();
                    if($SocialUser) {
                        $SocialUser->update([
                            'site_name' => $key,
                            'site_link' => $value
                        ]);
                    }else {
                        SocialUser::insert([
                            'user_id' => $user->id,
                            'site_name' => $key,
                            'site_link' => $value,
                        ]);
                    }
                }
            }
        }

        return redirect()->route('admin.users')->with('alert_success', 'User Update successfully.');
    }

    public function getBanned($id)
    {
        Gate::allows('modules', 'users_delete');

        $user = User::findOrFail($id);
        $user->update([
            'is_delete' => 1
        ]);
        return redirect('admin/users')->with('alert_success', 'User banned successfully.');
    }

    public function postSearch(Request $request)
    {
        $search_text = $request->search;
        $users = User::where(function ($query) use ($search_text) {
            $query->where('username', 'LIKE', '%' . $search_text . '%')
                ->orWhere('email', 'LIKE', '%' . $search_text . '%')
                ->orWhere('phone', 'LIKE', '%' . $search_text . '%')
                ->orWhere('created_at', 'LIKE', '%' . $search_text . '%');
        })->orderBY('id', 'desc')->paginate('10');

        $data = [
            'users' => $users,
            'roles' => Roles::where('is_delete', 0)->get(),
            'user_status' => user_status(),
            'user_type_status' => user_type_status(),
        ];

        return view('users::admin.index', $data);
    }

    public function getTrash() {
        Gate::allows('modules', 'users_delete');

        if (!isset($request->user_status) || $request->user_status == '') {
            $users = User::onlyTrashed()->orderBy('id', 'desc')->paginate('10');
        } else {
            $users = User::onlyTrashed()->where('status', intval($request->user_status))->orderBy('id', 'desc')->paginate('10');
        }
        $data = [
            'users' => $users,
            'roles' => config('roles'),
            'status' => Status::user_status(),
            'twofa_status' => Status::twofa_status(),
        ];

        return view('users::trash', $data);
    }

    public function getRestore($id) {
        Gate::allows('modules', 'users_delete');

        $users = User::withTrashed()->findOrFail($id)->restore();

        return redirect()->back()->with('alert_success', 'User Restore successful.');
    }

    public function deleteTrash($id) {
        Gate::allows('modules', 'users_delete');
        
        $users = User::withTrashed()->findOrFail($id)->forceDelete();

        return redirect()->back()->with('alert_success', 'User deleted successfully.');
    }

    public function getPartner() {
        $users = User::where('permission', 'partner')->get();
        $data = [
            'users' => $users,
            'roles' => config('roles'),
            'status' => user_status(),
            'twofa_status' => twofa_status(),
            'users' => $users
        ];

        return view('admin.partner.index', $data);
    }

    public function getEditPartner($id) {
        $user = User::findOrFail($id);
        $roles_basic = config('roles');
        $products = Products::where('author', $user->id)->get();
        $orders = Orders::where('id_shop', $user->id)->get();
        $data = [
            'orders' => $orders,
            'products' => $products,
            'roles_basic' => $roles_basic,
            'user' => $user,
            'user_status' => user_status(),
            'order_status' => Status::order_status()
        ];

        return view('admin.partner.edit', $data);
    }

    public function postFilter(Request $request) {
        $users = User::where('is_delete', 0);
        if(!is_null($request->roles)) {
            $role = Roles::where('id', $request->roles)->where('is_delete', 0)->first();
            if($role) {
                $users = $users->where('role', $role->id);
            }
        }

        if(!is_null($request->status)) {
            $status = ['0','1'];
            if(in_array($request->status, $status)) {
                $users = $users->where('status', $request->status);
            }
        }

        if(!is_null($request->type_user)) {
            $type_user = ['system','member'];
            if(in_array($request->type_user, $type_user)) {
                $users = $users->where('type', $request->type_user);
            }
        }

        $users = $users->where('is_delete', 0)->paginate('10');
        $data = [
            'users' => $users,
            'roles' => Roles::where('is_delete', 0)->get(),
            'user_status' => user_status(),
            'user_type_status' => user_type_status(),
            'filters' => $request->all(),
        ];

        return view('users::admin.index', $data);
    }
}
