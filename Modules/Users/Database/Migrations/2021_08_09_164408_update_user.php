<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->nullable(); 
            $table->string('image')->nullable(); 
            $table->string('birthday')->nullable(); 
            $table->string('phone')->nullable(); 
            $table->text('address')->nullable(); 
            $table->string('gender')->nullable(); 
            $table->integer('status')->nullable(); 
            $table->string('roles')->nullable(); 
            $table->string('type')->nullable(); 
            $table->string('policy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
