@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Danh sách thành viên</h4>
                        <a href="{{ route('admin.users.add') }}"
                            class="button-title float-right btn btn-outline-primary waves-effect width-md waves-light">Thêm thành viên
                            </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="table_top_actions">
                        <div class="table_top_actions_left">
                            <form action="{{ route('admin.users.filter') }}" method="post">
                                @csrf
                                <div class="d-flex justify-content-between" style="justify-content: left !important;">
                                    <div class="form-group pr-1">
                                        <label for="from"><strong>Roles</strong></label>
                                        <select class="form-control" name="roles">
                                            <option value="">All</option>
                                            @foreach($roles as $role)
                                            <option value="{{ $role->id }}" {{ (isset($filters) && $filters['roles'] == $role->id) ? 'selected' : '' }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group pr-1">
                                        <label for="from"><strong>Status</strong></label>
                                        <select class="form-control" name="status">
                                            <option value="all" {{ (isset($filters) && $filters['status'] == 'all') ? 'selected' : '' }}>All</option>
                                            <option value="1" {{ (isset($filters) && $filters['status'] == '1') ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ (isset($filters) && $filters['status'] == '0') ? 'selected' : '' }}>Inactive</option>
                                        </select>
                                    </div>
                                    <div class="form-group pr-1">
                                        <label for="from"><strong>Type user</strong></label>
                                        <select class="form-control" name="type_user">
                                            <option value="all" {{ (isset($filters) && $filters['type_user'] == 'all') ? 'selected' : '' }}>All</option>
                                            <option value="system" {{ (isset($filters) && $filters['type_user'] == 'system') ? 'selected' : '' }}>System</option>
                                            <option value="member" {{ (isset($filters) && $filters['type_user'] == 'member') ? 'selected' : '' }}>Member</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary waves-effect width-md waves-light">Filter</button>
                                </div>
                            </form>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-4">
                    <form class="app-search" action="{{ route('admin.users.search') }}" method="post">
                        @csrf
                        <div class="app-search-box">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn search" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @include('admin.boxes.notify')
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                    <thead>
                                        <tr>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Username</th>
                                            <th>Role</th>
                                            <th>Status</th>
                                            <th>Type user</th>
                                            <th>Banned</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $value)
                                        <tr>
                                            <td><a href="{{ route('admin.users.edit', ['id' => $value->id]) }}"
                                                    class="text-primary">{{$value->email}}</a></td>
                                            <td>{{ $value->phone}}</td>
                                            <td>{{ $value->username }}</td>
                                            <td>
                                                @if($value->Role())
                                                <span class="badge badge-primary"><a href="{{ route('admin.roles.edit', ['id' => $value->Role()->id ]) }}" class="text-white">{{ $value->Role()->name}}</a></span>
                                                @else
                                                ------
                                                @endif
                                            </td>
                                            <td>{!! $user_status[$value->status] !!}</td>
                                            <td>{!! $user_type_status[$value->type] !!}</td>
                                            <td style="white-space: nowrap; width: 1%;">
                                                <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                                    <div class="btn-group btn-group-sm" style="float: none;"><a
                                                            href="{{ route('admin.users.banned', ['id' => $value->id]) }}"
                                                            onclick="return confirm('Are you sure to banned the user ?')"><button
                                                                type="button"
                                                                class="tabledit-edit-button btn btn-danger"
                                                                style="float: none;"><span
                                                                    class="mdi mdi-delete"></span></button></a></div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- end .table-responsive-->
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12 mt-2 mt-md-4">
                    <ul class="pagination pagination_style1 justify-content-center">
                        {{ @$users->links('admin.pagination') }}
                    </ul>
                </div>
            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->
</div>
@endsection
