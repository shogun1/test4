@extends('admin.app')
@push('css')
<link href="{{ asset('assets\libs\bootstrap-datepicker\bootstrap-datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('assets\libs\switchery\switchery.min.css') }}" rel="stylesheet" type="text/css">
<style>
    
</style>
@endpush
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        {{-- <h4 class="page-title">Thêm thành viên</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Velonic</a></li>
                                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                <li class="breadcrumb-item active">Form Validation</li>
                            </ol>
                        </div> --}}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @include('admin.boxes.notify')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="pull-in">
                                <form id="basic-form" action="#" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div>
                                        <h3>Account</h3>
                                        <section>
                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="username">User name <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input class="form-control required" id="username" name="username" value="{{ $user->username }}" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="email">Email <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input class="form-control required" disabled id="email" name="email" value="{{ $user->email }}" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="password"> Password <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="password" name="password" type="password" class="required form-control">

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="confirm">Confirm Password <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="confirm" name="password_confirmation" type="password" class="required form-control">
                                                </div>
                                            </div>

                                        </section>
                                        <h3>Profile</h3>
                                        <section>
                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="phone"> Phone <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="phone" disabled name="phone" type="number" min="0" value="{{ $user->phone }}" class="required form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="gender"> Gender <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <select name="gender" class="form-control type_user">
                                                        <option {{ $user->gender == 'male' ? 'selected' : '' }} value="male">Male</option>
                                                        <option {{ $user->gender == 'female' ? 'selected' : '' }} value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="birthday"> Birthday <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="birthday" name="birthday" type="date" value="{{ $user->birthday }}"  data-date-format="DD MMMM YYYY" class="required form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="address"> Address <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="address" name="address" type="text" value="{{ $user->address }}" class="required form-control">
                                                </div>
                                            </div>
                                        </section>
                                        <h3>Social</h3>
                                        <section>
                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="site_facebook"> Facebook <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="site_facebook" name="site[site_facebook]" type="text" value="{{ isset($social_user['site_facebook']) ? $social_user['site_facebook'] : null }}" class="required form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="site_zalo"> Zalo <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="site_zalo" name="site[site_zalo]" type="text" value="{{ isset($social_user['site_zalo']) ? $social_user['site_zalo'] : null }}" class="required form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="site_linkedin"> LinkedIn <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <input id="site_linkedin" name="site[site_linkedin]" type="text" value="{{ isset($social_user['site_linkedin']) ? $social_user['site_linkedin'] : null }}" class="required form-control">
                                                </div>
                                            </div>
                                        </section>
                                        <h3>Permissions</h3>
                                        <section>
                                            <div class="form-group row">
                                                <div class="col-6 row">
                                                    <label class="col-lg-4 control-label" for="image"> Avatar <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <div class="container-avatar">
                                                            <div class="avatar-upload">
                                                                <div class="avatar-edit">
                                                                    <label title="Upload Image"><input type="file" name="image" id="imageUpload"></label>
                                                                </div>
                                                                <div class="avatar-preview">
                                                                    <div id="imagePreview" style="background-image: url({{ !is_null($user->image) ? $user->image : asset('assets/images/avatar.png') }});"></div>
                                                                </div>
                                                            </div>
                                                            <div class="imageFilename"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-6 row">
                                                    <label class="col-lg-3 control-label" for="image"> Status <span class="text-danger">*</span></label>
                                                    <div class="col-lg-9">
                                                        <select name="status" class="form-control">
                                                            <option {{ $user->status == 1 ? 'selected' : '' }} value="1">Active</option>
                                                            <option {{ $user->status == 0 ? 'selected' : '' }} value="0">Banned</option>
                                                        </select>
                                                    </div>
                                                    @if($user->type == 'system')
                                                    <label class="col-lg-3 control-label" for="image"> Media Folder <span class="text-danger">*</span></label>
                                                    <div class="col-lg-9">
                                                        <input type="checkbox" class="form-control" style="width: 16px;" name="is_media_manager" value="{{ $user->is_media_manager }}" {{ $user->is_media_manager == 1 ? 'checked' : '' }} id="">
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="type"> Type user <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <select name="type" class="form-control type_user">
                                                        <option value="" disabled selected>Select Type user</option>
                                                        <option {{ $user->type == 'member' ? 'selected' : '' }} value="member">Member</option>
                                                        <option {{ $user->type == 'system' ? 'selected' : '' }} value="system">System</option>
                                                    </select>
                                                </div>
                                                
                                            </div>

                                            <div class="form-group row" id="system" style="{{ $user->type == 'system' ? '' : 'display: none' }}">

                                                <label class="col-lg-2 control-label" for="name"> Roles <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <select name="role" class="form-control">
                                                        @foreach($roles as $value)
                                                        <option {{ $user->role == $value->slug ? 'selected' : '' }} value="{{ $value->slug }}">{{ $value->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row" id="member" style="{{ $user->type == 'member' ? '' : 'display: none' }}">

                                                <label class="col-lg-2 control-label" for="name"> Policy <span class="text-danger">*</span></label>
                                                <div class="col-lg-10">
                                                    <select name="policy" class="form-control">
                                                        @foreach($policy as $value)
                                                        <option {{ $user->policy_id == $value->id ? 'selected' : '' }} value="{{ $value->id }}">{{ $value->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2015 - 2020 &copy; Velonic theme by <a href="">Coderthemes</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .container-avatar {
    max-width: 115px;
    margin: 0px auto;
    }
    .avatar-upload {
    position: relative;
    max-width: 200px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 3px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit label{
    display: flex;
    justify-content: center;
    align-items: center;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input{
        position: absolute;
    opacity: 0;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    }
    .avatar-upload .avatar-edit label:hover {
    background: #fff;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit label:after{
    content: "\f040";
    font-family: 'FontAwesome';
    color: #007BFF;
    margin-left: 10px;
    }
    .avatar-upload .avatar-preview {
    width: 100px;
    height: 100px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview>div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }
    .imageFilename{
        text-align: center;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
</style>
@endpush
@push('js')
<!--Form Wizard-->
<script src="{{ asset('assets\libs\jquery-steps\jquery.steps.min.js') }}"></script>
<!-- Init js-->
<script src="{{ asset('assets\js\pages\form-wizard.init.js') }}"></script>
<script src="{{ asset('assets\libs\bootstrap-datepicker\bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets\libs\switchery\switchery.min.js') }}"></script>
<script src="{{ asset('assets\js\pages\form-advanced.init.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.type_user').change(function(){ 
            var value = $(this).val();
            if(value == 'member') {
                $('#member').show();
                $('#system').hide();
            }else {
                $('#system').show();
                $('#member').hide();
            }
        });

        $("#wizard-picture").change(function(){
            readURL(this);
        });
    });
    function readURL(input) {
 	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide().fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
        $('.imageFilename').html(input.files[0].name).attr('title', input.files[0].name);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });
</script>
@endpush
