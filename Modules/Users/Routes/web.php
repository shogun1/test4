<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::group(['prefix' => '/users'], function () {
        Route::get('/profile', 'UsersController@getProfile')->name('admin.profile');
        Route::post('/profile', 'UsersController@postProfile');

        Route::get('/', 'UsersController@index')->name('admin.users');
        Route::group(['prefix' => 'add'], function () {
            Route::get('/', 'UsersController@getAdd')->name('admin.users.add');
            Route::post('/', 'UsersController@postAdd');
        });

        Route::group(['prefix' => 'edit'], function () {
            Route::get('/{id}', 'UsersController@getEdit')->name('admin.users.edit');
            Route::post('/{id}', 'UsersController@postEdit');
        });

        Route::get('/banned/{id}', 'UsersController@getBanned')->name('admin.users.banned');
        Route::post('/search', 'UsersController@postSearch')->name('admin.users.search');
        Route::post('/filter', 'UsersController@postFilter')->name('admin.users.filter');
    });
});
