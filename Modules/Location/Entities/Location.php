<?php

namespace Modules\Location\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Location extends Model
{
    use HasFactory;

    protected $table = 'location';
    protected $fillable = ['id', 'type', 'slug', 'name', 'name_with_type', 'path', 'path_with_type', 'code', 'parent', 'status'];
    protected $hidden = ['created_at', 'updated_at'];
    protected static function newFactory()
    {
        return \Modules\Location\Database\factories\LocationFactory::new();
    }
}
