<?php

namespace Modules\Location\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Exception;
use Modules\Location\Entities\Location;


class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $offer = $request->offer ?? 10;
        $datas = Location::where('status', '<>', 'trash')->paginate($offer);
        return view('theme::admin.location.location', compact("datas"));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        DB::beginTransaction();
        try {
            $input = Request::all();
            Location::insert($input);
            DB::commit();
            return view('theme::admin.location.location')->with('message', "Create Success");
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            Location::insert([
                'type' => $request->type,
                'slug' => $request->slug,
                'name' => $request->name,
                'name_with_type' => $request->name_with_type,
                'path' => $request->path,
                'path_with_type' => $request->path_with_type,
                'code' => $request->code,
                'parent' => 0,
                'status' => "text"
            ]);
            DB::commit();
            $response = array(
                "status" => 'success',
                'message' => array(
                    'display' => false,
                    'content' => 'Created new location successfully',
                )
            );
            return $response;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $location = Location::findOrFail($id);
        return $location;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $location = Location::findOrFail($id);
        return view('theme::admin.location.location', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $location = Location::findOrFail($id);
            $location->update([
                'type' => $request->type,
                'slug' => $request->slug,
                'name' => $request->name,
                'name_with_type' => $request->name_with_type,
                'path' => $request->path,
                'path_with_type' => $request->path_with_type,
                'code' => $request->code,
                'parent' => 0,
                'status' => "text"
            ]);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            Location::find($id)->update(['status' => 'trash']);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
