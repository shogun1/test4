<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationRelationshipTable extends Migration
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'location_relationship';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_relationship', function (Blueprint $table) {
            $table->integer('location_id');
            $table->integer('object_id');
            $table->integer('term_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_relationship');
    }
}
