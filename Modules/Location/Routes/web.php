<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::prefix('location')->group(function () {
        Route::get('/', 'LocationController@index')->name('location-index');
        Route::get('/all', 'LocationController@index');
        Route::get('/add', 'LocationController@create');
        Route::post('/store', 'LocationController@store');
        Route::get('/{id}', 'LocationController@show');
        Route::get('/edit/{id}', 'LocationController@edit');
        Route::post('/update/{id}', 'LocationController@update');
        Route::post('/trash/{id}', 'LocationController@destroy');
    });
});
