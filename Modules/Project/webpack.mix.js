const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath( 'public' ).mergeManifest();

mix.styles(
    [
        __dirname + '/Resources/assets/css/admin/project.css'
    ],
    'public/assets/css/admin/project.css'
);

mix.scripts(
    [
        __dirname + '/Resources/assets/js/admin/project-sections.js',
        __dirname + '/Resources/assets/js/admin/project-post.js',
    ],
    'public/assets/js/admin/project.js'
);

if (mix.inProduction()) {
    mix.version();
}
