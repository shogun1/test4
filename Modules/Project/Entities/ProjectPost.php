<?php

namespace Modules\Project\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProjectPost extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'project_post';
    protected $fillable = ['title', 'slug'];

    protected static function newFactory()
    {
        return \Modules\Project\Database\factories\ProjectPostFactory::new();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     * @author Trac Lam
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
