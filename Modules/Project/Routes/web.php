<?php
use Modules\Theme\Http\Controllers\ThemeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    Route::prefix( 'project' )->group( function() {
        Route::get( '/', 'ProjectController@index' );
        Route::get( '/all', 'ProjectController@index' );
        Route::get( '/add', 'ProjectController@create' );
        Route::post( '/store', 'ProjectController@store' );
    });

});

Route::get( '/du-an/demo-1', [ThemeController::class, 'index'] );
