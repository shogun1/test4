let ProjectPost = {};

ProjectPost.savePost = function() {

    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxUrl = thisForm.attr( 'action' );
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();

        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: ajaxUrl,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: ProjectPost.savePost()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    console.log( response );
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }


};

$( document ).on( 'click', 'button#save-post', function() {
    ProjectPost.savePost();
});
