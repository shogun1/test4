@extends('admin.app')
@section('content')
    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Thêm dự án</h4>
                            <a href="{{ route('admin.users.add') }}"
                               class="button-title float-right btn btn-outline-primary waves-effect width-md waves-light">Thêm dự án
                            </a>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>

                @include('admin.boxes.notify')
                <!-- end page title -->

            </div>
            <!-- end container-fluid -->

        </div>
        <!-- end content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        2021 © batdongsan24h.vn
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
@endsection
