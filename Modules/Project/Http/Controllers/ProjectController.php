<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Project\Entities\ProjectPost;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('theme::admin.project.all' );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('theme::admin.project.create' );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store( Request $request ) {

        $data = $request->all();

        // Validate requested inputs
        $validation = 1;
        $validation -= empty( $data['title'] ) ? 1 : 0;
        $validation -= empty( $data['slug'] ) ? 1 : 0;

        if( $validation ) {

            $new_post = ProjectPost::firstOrNew(['title' => $data['title'], 'slug' => $data['slug']]);

            $new_post->save();

            $response = array(
                'status' => 'success',
                'debug' => $new_post
            );
        } else {
            $response = array(
                'status' => 'error',
                'debug' => $validation
            );
        }


        echo json_encode( $response );
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('project::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('project::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
