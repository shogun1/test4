<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\http\Controllers\OpenSource\OpenSource;

/** Admin routes **/

Route::group(['prefix' => 'admin', 'middleware' => ['auth'], 'namespace' => 'Admin'], function () {

    Route::get('/', function () {
        //redirect url permission user setup
        if(Auth::check()) {
            $pathAdmin = OpenSource::redirectAdmin(Auth::id());
            return redirect($pathAdmin);
        }
        return redirect('/');
    });

    Route::group(['prefix' => '/users'], function () {
        Route::get('/profile', 'UsersController@getProfile')->name('admin.profile');
        Route::post('/profile', 'UsersController@postProfile');

        // Route::post('/2fa','UsersController@enable2fa')->name('enable2fa');

        Route::get('/', 'UsersController@index')->name('admin.users');
        Route::group(['prefix' => 'add'], function () {
            Route::get('/', 'UsersController@getAdd')->name('admin.users.add');
            Route::post('/', 'UsersController@postAdd');
        });
        Route::group(['prefix' => 'edit'], function () {
            Route::get('/{id}', 'UsersController@getEdit')->name('admin.users.edit');
            Route::post('/{id}', 'UsersController@postEdit');
        });
        Route::get('/delete/{id}', 'UsersController@getDelete')->name('admin.users.delete');
        Route::get('/search', 'UsersController@getSearch')->name('admin.users.search');
    });

});

/** Web routes **/

Route::get('/', 'ThemeController@index')->name( 'home' );
Route::get('/{slug}', 'ThemeController@getSlug');
Route::get('/sitemap.xml', 'SiteMapController@index');
Route::get('/404', 'HomeController@Error404')->name('404');
