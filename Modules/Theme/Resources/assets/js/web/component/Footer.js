import React, {Component} from "react";
import {NavLink} from "react-router-dom";

class Footer extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            appData: window.appData
        };
    }

    updateLinks() {
        const { appData } = this.state;
        const { footer } = appData;
        const { menu1, menu2 } = footer;

        let links = [];
        menu1.menuList.map(( item, index ) => {
            links.push({
                link: item.link,
                screen: item.screen
            });
        });
        menu2.menuList.map(( item, index ) => {
            links.push({
                link: item.link,
                screen: item.screen
            });
        });

        this.props.updateLinks( links );
    }

    componentDidMount() {
        this.updateLinks();
    }

    render() {

        const { appData } = this.state;
        const { footer } = appData;
        const { topSection, footerAddress, menu1, menu2, footerFollow, copyRight } = footer;

        return(
            <footer id="footer">

                <section className="footer-top">
                    <div className="container">
                        <div className="row">
                            <div className="column-1 col-3 col-md-4" dangerouslySetInnerHTML={{__html: topSection.footer_logo}}></div>
                            <div className="column-2 col-5 col-md-2" dangerouslySetInnerHTML={{__html: topSection.footer_hotline}}></div>
                            <div className="column-3 col-4 col-md-6" dangerouslySetInnerHTML={{__html: topSection.footer_tbbct}}></div>
                        </div>
                    </div>
                </section>

                <section className="footer-mid">
                    <div className="container">
                        <div className="row">

                            <div className="column-1 col-12 col-lg-4">
                                <div className="wrap" dangerouslySetInnerHTML={{__html: footerAddress}}></div>
                            </div>

                            <div className="column-2 col-12 col-md-3 col-lg-2">
                                <div className="wrap">
                                    <h3 className="footer-menu-title">{menu1.menuTitle}</h3>
                                    <ul className="footer-menu">
                                        {menu1.menuList.map((item, index) => {
                                            return(
                                                <li className="menu-item" key={index}>
                                                    <NavLink to={item.link} activeClassName="active">{item.title}</NavLink>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>

                            <div className="column-3 col-12 col-md-3 col-lg-3">
                                <div className="wrap">
                                    <h3 className="footer-menu-title">{menu2.menuTitle}</h3>
                                    <ul className="footer-menu">
                                        {menu2.menuList.map((item, index) => {
                                            return(
                                                <li className="menu-item" key={index}>
                                                    <NavLink to={item.link} activeClassName="active">{item.title}</NavLink>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>

                            <div className="column-4 col-12 col-md-6 col-lg-3">
                                <div className="wrap" dangerouslySetInnerHTML={{__html: footerFollow}}></div>
                            </div>

                        </div>
                    </div>
                </section>

                <section className="footer-bottom">
                    <div className="container" dangerouslySetInnerHTML={{__html: copyRight}}></div>
                </section>

            </footer>
        )
    }
}

export default Footer;
