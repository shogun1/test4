import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import AppHelpers from "../helpers/AppHelpers";

class Header extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            appData: window.appData
        };
    }

    /**
     * Update all links in this component to App Route
     * */
    updateLinks() {
        const { appData } = this.state;
        const { header } = appData;
        const { headerMenu, headerButtons } = header;

        let links = [];
        headerMenu.map(( item, index ) => {

            links.push({
                link: item.link,
                screen: item.screen
            });

            if( typeof( item.sub_menu ) !== "undefined" ) {
                item.sub_menu.map(( sub_item, sub_index ) => {
                    links.push({
                        link: sub_item.link,
                        screen: sub_item.screen
                    });
                });
            }
        });
        for( const key in headerButtons ) {
            links.push({
                link: headerButtons[key].link,
                screen: headerButtons[key].screen
            });
        }

        this.props.updateLinks( links );
    }

    /**
     * Render sub-menu
     * */
    renderMenuItem( menuItem, index ) {
        if( typeof( menuItem.sub_menu ) !== "undefined" ) {
            return(
                <li className="menu-item has-children" key={index}>
                    <NavLink to={menuItem.link} activeClassName="active">{menuItem.title}</NavLink>
                    <button className="button toggle-sub-menu-button">
                        <i className="icon fal fa-angle-down"></i>
                    </button>
                    <ul className="sub-menu">
                        {menuItem.sub_menu.map((subItem, subIndex) => {
                            return(
                                <li className="menu-item" key={subIndex}>
                                    <NavLink to={subItem.link} activeClassName="active">{subItem.title}</NavLink>
                                </li>
                            )
                        })}
                    </ul>
                </li>
            )
        } else {
            return(
                <li className="menu-item" key={index}>
                    <NavLink to={menuItem.link} activeClassName="active">{menuItem.title}</NavLink>
                </li>
            )
        }

    }

    componentDidMount() {
        AppHelpers.toggleMobileMenu();
        AppHelpers.toggleSubMenuOnMobile();
        AppHelpers.hideMenuWhenTapOutside();
        this.updateLinks();
    }

    render() {

        const { appData } = this.state;
        const { header } = appData;
        const { appTitle, headerLogo, headerMenu, headerButtons, headerSearchForm, headerBanner } = header;
        const { submitPropertyButton, notificationButton, whistListButton, loginButton } = headerButtons;
        const { typeRadio } = headerSearchForm;

        return(
            <header id="masthead">

                <section className="header-top">
                    <div className="container">

                        <div className="logo">
                            <NavLink to="/">
                                <img className="color-logo" src={headerLogo.src.color} alt={appTitle} />
                                <img className="white-logo" src={headerLogo.src.white} alt={appTitle} />
                            </NavLink>
                        </div>

                        <div className="header-nav-bar">
                            <button className="button mobile-button d-md-none">
                                <i className="icon hide-on-open fal fa-bars fa-fw"></i>
                                <i className="icon hide-on-close fal fa-times fa-fw"></i>
                            </button>
                            <div className="nav-bar-container">
                                <ul className="main-menu">
                                    {headerMenu.map((item, index) => {
                                        return this.renderMenuItem( item, index );
                                    })}
                                </ul>
                            </div>
                        </div>

                        <div className="buttons-left d-none d-md-inline-block">
                            <NavLink to={submitPropertyButton.link} className="button button__has-icon button__submit-property">
                            <span>
                                <i className="icon fal fa-edit"></i>
                                <span className="text d-none d-md-inline-block">{submitPropertyButton.title}</span>
                            </span>
                            </NavLink>
                        </div>

                        <div className="buttons-right">
                            <NavLink to={notificationButton.link} className="button button__has-icon button__notification d-none d-md-inline-block">
                            <span>
                                <i className="icon fal fa-bell"></i>
                                <span className="text d-none">{notificationButton.title}</span>
                            </span>
                            </NavLink>
                            <NavLink to={whistListButton.link} className="button button__has-icon button__whist-list">
                            <span>
                                <i className="icon fal fa-heart"></i>
                                <span className="text d-none">{whistListButton.title}</span>
                            </span>
                            </NavLink>
                            <NavLink to={loginButton.link} className="button button__has-icon button__login d-none d-md-inline-block">
                            <span>
                                <i className="icon fal fa-user"></i>
                                <span className="text d-none d-lg-inline-block">{loginButton.title}</span>
                            </span>
                            </NavLink>
                        </div>

                    </div>
                </section>

                <section className="header-bottom">

                    <div className="header-search">
                        <form id="main-search" className="search-form">
                            <h3 className="title">{headerSearchForm.title}</h3>
                            <fieldset className="type-search d-none d-md-inline-block">
                                {typeRadio.map(( item, index ) => {
                                    return(
                                        <label key={index}>
                                            <input type="radio" name="type" value={item.value} />
                                            <span className="text">{item.text}</span>
                                        </label>
                                    )
                                })}
                            </fieldset>
                            <fieldset className="keyword-search">
                                <input type="text" name="keyword" className="form-input text-input" placeholder={headerSearchForm.placeholder} />
                                <button type="button" className="button advanced-search-button d-none d-md-inline-block">
                                    <i className="icon fal fa-sliders-h"></i>
                                </button>
                                <button type="button" className="button submit-button">
                                    <i className="icon fal fa-search"></i>
                                    <span className="text d-none d-md-inline-block">{headerSearchForm.submitButtonText}</span>
                                </button>
                            </fieldset>
                        </form>
                    </div>

                    <img className="header-banner" src={headerBanner.src} />

                </section>

            </header>
        )
    }
}

export default Header;
