import React, {Component} from "react";
import {Switch, Route} from "react-router-dom";

import AgentCategory from "./screen/AgentCategory/AgentCategory";
import Home from "./screen/Home/Home";
import Page from "./screen/Page/Page";
import MyAccountLogin from "./screen/MyAccountLogin/MyAccountLogin";
import MyAccountNotification from "./screen/MyAccountNotification/MyAccountNotification";
import MyAccountWhistList from "./screen/MyAccountWhistList/MyAccountWhistList";
import NewsCategory from "./screen/NewsCategory/NewsCategory";
import ProjectCategory from "./screen/ProjectCategory/ProjectCategory";
import ProjectSingle from "./screen/ProjectSingle/ProjectSingle";
import PropertyCategory from "./screen/PropertyCategory/PropertyCategory";
import SubmitPropertyPage from "./screen/SubmitPropertyPage/SubmitPropertyPage";
import VideoCategory from "./screen/VideoCategory/VideoCategory";


class AppRoute  extends Component {
    constructor( props ) {
        super( props );

        this.state = {
            appLink: window.appLink
        };
    }

    componentDidUpdate( prevProps ) {
        if ( this.props.links !== prevProps.links ) {
            this.setState({
                appLink: this.props.links
            });
        }
    }

    render() {

        const { appLink } = this.state;

        let links = [];
        if( typeof appLink === 'object' ) {
            links = appLink;
        }

        return(
            <Switch>

                <Route path="/" exact>
                    <Home pageClass="home" />
                </Route>

                {links.map((item, index) => {

                    let screen;
                    switch( item.screen ) {
                        case 'Page':
                            screen = <Page pageClass="page" />; break;
                        case 'AgentCategory':
                            screen = <AgentCategory pageClass="agent-category" />; break;
                        case 'MyAccountLogin':
                            screen = <MyAccountLogin pageClass="login-page" />; break;
                        case 'MyAccountNotification':
                            screen = <MyAccountNotification pageClass="notification-page" />; break;
                        case 'MyAccountWhistList':
                            screen = <MyAccountWhistList pageClass="whist-list-page" />; break;
                        case 'NewsCategory':
                            screen = <NewsCategory pageClass="news-category" />; break;
                        case 'ProjectCategory':
                            screen = <ProjectCategory pageClass="project-category" />; break;
                        case 'ProjectSingle':
                            screen = <ProjectSingle pageClass="project-single" />; break;
                        case 'PropertyCategory':
                            screen = <PropertyCategory pageClass="property-category" />; break;
                        case 'SubmitPropertyPage':
                            screen = <SubmitPropertyPage pageClass="submit-property-page" />; break;
                        case 'VideoCategory':
                            screen = <VideoCategory pageClass="video-category" />; break;
                        default:
                            screen = <Home pageClass="home" />;
                    }

                    return(
                        <Route path={item.link} exact key={index}>
                            <main id="primary">
                                {screen}
                            </main>
                        </Route>
                    )
                })}
            </Switch>
        )
    }
}

export default AppRoute;
