import AppConfig from "../AppConfig";

const AppHelpers = {
    addAppLink: ( links ) => {
        links.map(( linkItem, index ) => {
            if( window.appLink.findIndex( item => item.link === linkItem.link ) == -1 ) {
                window.appLink.push({
                    link: linkItem.link,
                    screen: linkItem.screen
                });
            }
        });
    },
    hideMainMenu: ( menu ) => {
        /** Header Menu - Hide menu on mobile */
        $( menu ).removeClass( 'active' );
        $( 'body' ).removeClass( 'open-menu' );
    },
    hideMenuWhenTapOutside: () => {
        /** Header Menu - Hide menu when tap outside the menu area */
        if( $( window ).width() < 1200 ) {
            $(document).on('mouseup', function (e) {
                let menuWrap = $(AppConfig.MAIN_MENU_WRAP);
                let notTarget = !menuWrap.is(e.target);
                let notChild = menuWrap.has(e.target).length === 0;
                if (notTarget && notChild) {
                    menuWrap.removeClass('active');
                    $( 'body' ).removeClass( 'open-menu' );
                    $( '.sub-menu', menuWrap ).slideUp();
                }
            });
        }
    },
    toggleMobileMenu: () => {
        /** Header Menu - Toggle on Click to burger button */
        $( document ).on( 'click', AppConfig.MOBILE_MENU_BUTTON, function(){
            $( this ).parent().toggleClass( 'active' );
            $( 'body' ).toggleClass( 'open-menu' );
        });
    },
    toggleSubMenuOnMobile: () => {
        /** Header Menu - Toggle sub-menu on mobile */
        if( $( window ).width() < 1200 ) {
            $( document ).on( 'click', AppConfig.MOBILE_TOGGLE_SUBMENU_BUTTON, function(){
                $( this ).parent().find( '.sub-menu' ).slideToggle();
            });
        }
    },
    addClassToBody: ( pageClass ) => {
        $( 'body' ).attr( 'class', pageClass );
    },
    scrollToTop: () => {
        window.scrollTo(0, 0);
    }
};

export default AppHelpers;
