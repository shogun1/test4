import React, {Component} from "react";
import AppConfig from "../../AppConfig";
import AppHelpers from "../../helpers/AppHelpers";

class ProjectSingle extends Component {

    constructor( props ) {
        super( props );
        AppHelpers.addClassToBody( this.props.pageClass );
        AppHelpers.hideMainMenu( AppConfig.MAIN_MENU_WRAP );
    }

    componentDidMount() {
        AppHelpers.scrollToTop();
    }

    componentDidUpdate() {
        AppHelpers.scrollToTop();
    }

    render() {
        return(
            <div className="project-container">
                <section id="project-gallery">
                    <div className="container-fluid">
                        <div className="owl-carousel owl-theme">
                            <div className="item">
                                <img src="/assets/images/demo/project-demo-1.jpeg" />
                            </div>
                            <div className="item">
                                <img src="/assets/images/demo/project-demo-1.jpeg" />
                            </div>
                            <div className="item">
                                <img src="/assets/images/demo/project-demo-1.jpeg" />
                            </div>
                        </div>
                    </div>
                </section>

                <section id="project-overview">
                    <div className="container-fluid">
                        <div className="text">
                            <p>ABC def....</p>
                            <p>ABC def....</p>
                            <p>ABC def....</p>
                            <p>ABC def....</p>
                            <p>ABC def....</p>
                        </div>
                        <div className="image">
                            <img src="/assets/images/demo/project-demo-1.jpeg" />
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default ProjectSingle;
