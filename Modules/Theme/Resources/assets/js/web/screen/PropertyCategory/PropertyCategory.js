import React, {Component} from "react";
import AppHelpers from "../../helpers/AppHelpers";
import AppConfig from "../../AppConfig";

class PropertyCategory extends Component {

    constructor( props ) {
        super( props );
        AppHelpers.addClassToBody( this.props.pageClass );
        AppHelpers.hideMainMenu( AppConfig.MAIN_MENU_WRAP );
    }

    componentDidMount() {
        AppHelpers.scrollToTop();
    }

    componentDidUpdate() {
        AppHelpers.scrollToTop();
    }

    render() {
        return(
            <p>Property Category content</p>
        )
    }
}

export default PropertyCategory;
