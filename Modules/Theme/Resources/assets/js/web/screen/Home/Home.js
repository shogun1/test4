import React, {Component} from "react";
import AppConfig from "../../AppConfig";
import AppHelpers from "../../helpers/AppHelpers";

class Home extends Component {

    constructor( props ) {
        super( props );
        AppHelpers.addClassToBody( this.props.pageClass );
        AppHelpers.hideMainMenu( AppConfig.MAIN_MENU_WRAP );
    }

    componentDidMount() {
        AppHelpers.scrollToTop();
    }

    componentDidUpdate() {
        AppHelpers.scrollToTop();
    }

    render() {
        return(
            <p>Home content</p>
        )
    }
}

export default Home;
