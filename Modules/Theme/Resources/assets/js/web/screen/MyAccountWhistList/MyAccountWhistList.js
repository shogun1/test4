import React, {Component} from "react";
import AppHelpers from "../../helpers/AppHelpers";
import AppConfig from "../../AppConfig";

class MyAccountWhistList extends Component {

    constructor( props ) {
        super( props );
        AppHelpers.addClassToBody( this.props.pageClass );
        AppHelpers.hideMainMenu( AppConfig.MAIN_MENU_WRAP );
    }

    componentDidMount() {
        AppHelpers.scrollToTop();
    }

    componentDidUpdate() {
        AppHelpers.scrollToTop();
    }

    render() {
        return(
            <p>My Account WhistList content</p>
        )
    }
}

export default MyAccountWhistList;
