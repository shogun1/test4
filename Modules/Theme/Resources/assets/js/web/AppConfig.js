const AppConfig = {
    MAIN_MENU_WRAP: '.header-nav-bar',
    MOBILE_MENU_BUTTON: '.header-nav-bar .mobile-button',
    MOBILE_TOGGLE_SUBMENU_BUTTON: '.header-nav-bar .toggle-sub-menu-button'
};

export default AppConfig;
