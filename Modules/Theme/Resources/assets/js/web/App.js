import React, {Component} from "react";
import {BrowserRouter as Router} from "react-router-dom";

import Header from "./component/Header";
import Footer from "./component/Footer";
import AppRoute from "./AppRoute";
import AppHelpers from "./helpers/AppHelpers";

class App extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            appData: window.appData,
            appLink: []
        };

        window.appLink = [];

        this.updateLinks = this.updateLinks.bind( this );
    }

    updateLinks( links ) {
        AppHelpers.addAppLink( links );
        this.setState({ appLink: window.appLink });
    }

    componentDidMount() {
    }

    render() {
        return(
            <Router>
                <Header updateLinks={this.updateLinks} />
                <AppRoute links={ this.state.appLink } />
                <Footer updateLinks={this.updateLinks} />
            </Router>
        )
    }

};

export default App;
