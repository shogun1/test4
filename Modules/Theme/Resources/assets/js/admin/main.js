/**
 * Global Tiny MCE Options
 * */
window.tinymceOptions = {
    plugins: [
        "media",
        "autolink link lists advlist",
        "table"
    ],
    toolbar: "formatselect | fontselect | bold italic strikethrough forecolor backcolor formatpainter | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | link image | removeformat | code | addcomment",
    height: 250,
    menubar: 'table insert tools',
    autosave_interval: '30s'
};
if( typeof( tinymce ) !== 'undefined' ) {
    tinymce.baseURL = window.homeURL + '/assets/js/admin/tinymce';
    tinymce.suffix = '.min';
}
