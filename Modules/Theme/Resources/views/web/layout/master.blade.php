<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', config('app.name', '@Master Layout'))</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset( 'assets/images/admin/favicon.png' ) }}">
        <link rel="apple-touch-icon" href="{{ asset( 'assets/images/admin/favicon.png' ) }}">
        <link rel="shortcut icon" href="{{ asset( 'assets/images/admin/favicon.png' ) }}">

        <link href="{{ mix('assets/vendor/css/vendor.css') }}" rel="stylesheet">

    </head>
    <body class="@yield('bodyClass')">

        @include('theme::web.layout.header')

        <div id="main">
            @yield('content')
        </div>

        @yield('sidebar')

        @include('theme::web.layout.footer')

        @yield('custom-script')

        <script src="{{ mix('assets/vendor/js/vendor.js') }}"></script>
        <script src="{{ mix('assets/js/web/render.js') }}"></script>
    </body>
</html>
