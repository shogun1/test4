@extends('theme::web.layout.master')

@section('custom-script')
    <script id="custom-script">
        {!! $customScript !!}
    </script>
@endsection
