@extends('theme::admin.layout.master')

@section('customStyle')
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/css/admin/project.css' ) }}">
@endsection

@section('customScript')
    <script src="{{ mix('assets/js/admin/project.js') }}"></script>
@endsection

@section('bodyClass', 'admin-page module-project create-project')

@section('content')

<div class="app-content content ">

    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>

    <div class="content-wrapper container-xxl p-0">

        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Create New Project</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin">Admin</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/admin/project">Project</a>
                                </li>
                                <li class="breadcrumb-item active">Add Project
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">

            <form id="project-content-form" class="mt-2" action="/admin/project/store" method="post">

                @csrf

                <div class="row">

                    <div class="col-12 col-md-9">
                        <div class="card">
                            <div class="card-body">

                                <fieldset class="mb-2 check-valid">
                                    <label class="form-label" for="post-title">Title</label>
                                    <input type="text" name="title" id="post-title" class="form-control" value="" required>
                                    <div class="invalid-feedback">The title cannot be empty.</div>
                                </fieldset>

                                <fieldset class="mb-2 check-valid">
                                    <label class="form-label" for="post-slug">Slug</label>
                                    <input type="text" name="slug" id="post-slug" class="form-control" value="" required>
                                    <div class="invalid-feedback">The slug cannot be empty.</div>
                                </fieldset>

                                <div id="project-sections" class="mt-3"></div>

                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <button type="button" id="save-post" class="btn btn-primary me-1 waves-effect waves-float waves-light">Publish</button>
                            </div>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>

</div>

<div class="modal-area">
    <div class="modal fade" id="element_module" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body element-group">
                    <div class="row">
                        <div class="item element-editor col-12 col-md-6 col-lg-3">
                            <div class="wrap" data-element="editor">
                                <span class="icon"><i class="fal fa-pen-square"></i></span>
                                <label>Editor</label>
                            </div>
                        </div>
                        <div class="item element-slider col-12 col-md-6 col-lg-3">
                            <div class="wrap" data-element="slider">
                                <span class="icon"><i class="fal fa-presentation"></i></span>
                                <label>Slider</label>
                            </div>
                        </div>
                        <div class="item element-tab col-12 col-md-6 col-lg-3">
                            <div class="wrap" data-element="tab">
                                <span class="icon"><i class="fal fa-columns"></i></span>
                                <label>Tab</label>
                            </div>
                        </div>
                        <div class="item element-icon-list col-12 col-md-6 col-lg-3">
                            <div class="wrap" data-element="icon-list">
                                <span class="icon"><i class="fal fa-list"></i></span>
                                <label>Icon List</label>
                            </div>
                        </div>
                        <div class="item element-button col-12 col-md-6 col-lg-3">
                            <div class="wrap" data-element="button">
                                <span class="icon"><i class="fal fa-rectangle-wide"></i></span>
                                <label>Button</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="js-template" style="display: none">

    <!-- Editor extra buttons -->
    <div class="editor-extra-buttons">
            <span class="editor-action-item">
                <a href="#" class="media-button">
                    <i class="far fa-image"></i> Add Media
                </a>
            </span>
    </div>

    <!-- Element block action buttons -->
    <div class="element-block-action-buttons">
        <button type="button" class="add-element-button" data-toggle="modal" data-target="#element_module" title="Add Button">
            <i class="fal fa-plus-circle"></i>
            Add Button
        </button>
        <button type="button" class="remove-element-button" title="Remove Button">
            <i class="far fa-trash-alt"></i>
        </button>
    </div>

    <!-- Init section -->
    <section id="" data-id="" class="project-section toggle-section toggle-open">
        <div class="wrap">

            <header class="section-header">
                <h3 class="section-title">
                    <input type="text" class="form-input" placeholder="Section title">
                </h3>
                <button type="button" class="right-button clone-button" title="Clone">
                    <i class="far fa-copy"></i>
                </button>
                <button type="button" class="right-button toggle-button" data-toggle-open-icon="far fa-chevron-up" data-toggle-close-icon="far fa-chevron-down" title="Show/Hide">
                    <i class="far fa-chevron-up"></i>
                </button>
                <button type="button" class="left-button move-button" title="Remove">
                    <i class="far fa-expand-arrows-alt"></i>
                </button>
            </header>

            <div class="section-content">

                <ul class="section-setting">
                    <li class="item setting-hide-section">
                        <label>
                            <input type="checkbox" class="form-checkbox">
                            Hide this section in Front-end
                        </label>
                    </li>
                    <li class="item setting-section-columns has-icon">
                        <i class="far fa-line-columns"></i>
                        <select class="form-select" data-name="section-columns-number">
                            <option value="1">Layout 1 column</option>
                            <option value="2">Layout 2 columns</option>
                            <option value="3">Layout 3 columns</option>
                            <option value="4">Layout 4 columns</option>
                        </select>
                    </li>
                    <li class="item setting-section-ratio has-icon" style="display: none">
                        <i class="far fa-percentage"></i>
                        <select class="form-select hide" data-name="section-columns-ratio" data-column="2" style="display: none">
                            <option value="6-6">Columns 6 / 6</option>
                            <option value="5-7">Columns 5 / 7</option>
                            <option value="7-5">Columns 7 / 5</option>
                            <option value="4-8">Columns 4 / 8</option>
                            <option value="8-4">Columns 8 / 4</option>
                            <option value="3-9">Columns 3 / 9</option>
                            <option value="9-3">Columns 9 / 3</option>
                        </select>
                        <select class="form-select hide" data-name="section-columns-ratio" data-column="3" style="display: none">
                            <option value="4-4-4">Columns 4 / 4 / 4</option>
                            <option value="3-6-3">Columns 3 / 6 / 3</option>
                            <option value="3-5-4">Columns 3 / 5 / 4</option>
                            <option value="4-5-3">Columns 4 / 5 / 3</option>
                            <option value="3-4-5">Columns 3 / 4 / 5</option>
                            <option value="5-4-3">Columns 5 / 4 / 3</option>
                        </select>
                        <select class="form-select hide" data-name="section-columns-ratio" data-column="4" style="display: none">
                            <option value="3-3-3-3">Columns 3 / 3 / 3 / 3</option>
                            <option value="2-4-4-2">Columns 2 / 4 / 4 / 2</option>
                            <option value="2-2-4-4">Columns 2 / 2 / 4 / 4</option>
                            <option value="4-4-2-2">Columns 4 / 4 / 2 / 2</option>
                            <option value="2-3-3-4">Columns 2 / 3 / 3 / 4</option>
                            <option value="4-3-3-2">Columns 4 / 3 / 3 / 2</option>
                        </select>
                    </li>
                    <li class="item setting-button-group">
                        <button type="button" class="toggle-advanced-setting" title="Settings">
                            <i class="fas fa-cog"></i>
                            Settings
                        </button>
                    </li>
                </ul>

                <ul class="section-advanced-setting" style="display: none">
                    <li class="item setting-section-id">
                        <label>
                            <span class="label-text">Section ID</span>
                            <input type="text" class="form-input" data-name="section-id">
                        </label>
                    </li>
                    <li class="item setting-section-class">
                        <label>
                            <span class="label-text">Section class</span>
                            <input type="text" class="form-input" data-name="section-class">
                        </label>
                    </li>
                    <li class="item setting-section-attr">
                        <label>
                            <span class="label-text">Section attributes</span>
                            <input type="text" class="form-input" data-name="section-attr">
                        </label>
                    </li>
                    <li class="footer-button-group">
                        <button type="button" class="delete-section" title="Delete section">
                            <i class="fal fa-trash-alt"></i>
                        </button>
                    </li>
                </ul>

                <div class="section-column-container">
                    <div class="column-tab">
                        <div class="row mx-0">
                            <div class="item px-0 col-12 col-lg-12 active" data-column="1">
                                <span class="column-text">Column 1</span>
                            </div>
                        </div>
                    </div>
                    <div class="column-content">
                        <div class="item available active" data-column="1">
                            <div class="wrap"></div>
                            <footer class="button-group">
                                <button type="button" class="add-element-button" data-bs-toggle="modal" data-bs-target="#element_module">
                                    <i class="fal fa-plus-circle"></i>
                                    Add elements
                                </button>
                            </footer>
                        </div>
                        <div class="item" data-column="2">
                            <div class="wrap"></div>
                            <footer class="button-group">
                                <button type="button" class="add-element-button" data-bs-toggle="modal" data-bs-target="#element_module">
                                    <i class="fal fa-plus-circle"></i>
                                    Add elements
                                </button>
                            </footer>
                        </div>
                        <div class="item" data-column="3">
                            <div class="wrap"></div>
                            <footer class="button-group">
                                <button type="button" class="add-element-button" data-bs-toggle="modal" data-bs-target="#element_module">
                                    <i class="fal fa-plus-circle"></i>
                                    Add elements
                                </button>
                            </footer>
                        </div>
                        <div class="item" data-column="4">
                            <div class="wrap"></div>
                            <footer class="button-group">
                                <button type="button" class="add-element-button" data-bs-toggle="modal" data-bs-target="#element_module">
                                    <i class="fal fa-plus-circle"></i>
                                    Add elements
                                </button>
                            </footer>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>

</div>

@endsection
