@extends('theme::admin.layout.master')

@section('customStyle')
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/css/admin/property.css' ) }}">
@endsection

@section('customScript')
<script src="{{ mix('assets/js/admin/property.js') }}"></script>
@endsection

@section('bodyClass', 'admin-page module-property create-property')

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Property Type</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                                <li class="breadcrumb-item"><a href="/admin/property">Property</a></li>
                                <li class="breadcrumb-item"><a href="/admin/property-type">Type</a></li>
                                <li class="breadcrumb-item active">Add Property Type</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <div class="row">
                <div class="col-12 col-md-4">
                    <form id="project-content-form" action="/admin/property-type/store" method="post">
                        @csrf
                        <div class="card">
                            <div class="card-body">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <input type="hidden" id="post-id" />
                                <fieldset class="mb-2 check-valid">
                                    <label class="form-label" for="post-title">Title</label><span class="text-danger"> *</span>
                                    <input type="text" name="title" id="post-title" class="form-control" value="" required>
                                    <div class="invalid-feedback">The title cannot be empty.</div>
                                </fieldset>

                                <fieldset class="mb-2 check-valid">
                                    <label class="form-label" for="post-slug">Slug</label><span class="text-danger"> *</span>
                                    <input type="text" name="slug" id="post-slug" class="form-control" value="" required>
                                    <div class="invalid-feedback">The slug cannot be empty.</div>
                                </fieldset>

                                <fieldset class="mb-2 check-valid">
                                    <label class="form-label" for="post-author">Author</label><span class="text-danger"> *</span>
                                    <select class="form-select" aria-label="" required id="post-author">
                                        <option value="0">- Select user -</option>
                                    </select>
                                    <div class="invalid-feedback">The author cannot be empty.</div>
                                </fieldset>

                                <fieldset class="mb-2 check-valid">
                                    <label class="form-label" for="post-status">Status</label><span class="text-danger"> *</span>
                                    <select class="form-select" aria-label="" id="post-status" required>
                                        <option value="publish" selected>publish</option>
                                        <option value="draft">draft</option>
                                        <option value="pending">pending</option>
                                        <option value="private">private</option>
                                        <option value="trash">trash</option>
                                    </select>
                                    <div class="invalid-feedback">The Status cannot be empty.</div>
                                </fieldset>

                                <fieldset class="mb-2">
                                    <label class="form-label" for="post-description">Description</label>
                                    <textarea type="text" name="description" id="post-description" class="form-control" value=""></textarea>
                                </fieldset>

                                <button type="button" id="save-post" class="btn btn-primary me-1 waves-effect waves-float waves-light save-post-type">Add new property type</button>
                                <button type="button" id="save-edit" class="btn btn-primary me-1 waves-effect waves-float waves-light" style="display : none">Save Edit</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-12 col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center mx-0 row">
                                <form action="{{ route('property-type-index') }}" method="GET" style="display:inline-flex">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="dataTables_length" id="DataTables_Table_0_length">
                                            <label style="margin-top: 0.5rem;margin-bottom: 1rem;font-weight: normal;text-align:left;white-space: nowrap;">Show
                                                <select class="form-select" style="width: auto;display: inline-block;width: 5rem;margin: 0 0.5rem;" id="select-record" name="record" onchange="this.form.submit()">
                                                    <option value="10" {{ (isset($record) && $record == '10') ? 'selected' : '' }}>10</option>
                                                    <option value="25" {{ (isset($record) && $record == '25') ? 'selected' : '' }}>25</option>
                                                    <option value="50" {{ (isset($record) && $record == '50') ? 'selected' : '' }}>50</option>
                                                    <option value="100" {{ (isset($record) && $record == '100') ? 'selected' : '' }}>100</option>
                                                </select> entries</label>

                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                            <label style="margin-top: 0.5rem;margin-bottom: 1rem;font-weight: normal;white-space: nowrap;text-align: left;">Search:
                                                <input name="search" type="search" class="form-control" placeholder="" aria-controls="DataTables_Table_0" style="margin-left: 0.75rem;display: inline-block;width: auto;" onblur="this.form.submit()" value="{{ isset($search) ? $search : '' }}">
                                            </label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th hidden>Id</th>
                                        <th>Title</th>
                                        <th>Slug</th>
                                        <th>Author</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                @foreach ($datas as $data)
                                <tbody>
                                    <td hidden> {{ $data->id }} </td>
                                    <td class="col-md-4" style="padding: 5px 10px  !important"> {{ $data->title }} </td>
                                    <td class="col-md-3" style="padding: 5px 10px  !important"> {{ $data->slug }} </td>
                                    <td class="col-md-3" style="padding: 5px 10px  !important"> {{ $data->author }} </td>
                                    <td style="text-align: center; padding: 5px 10px  !important" class="col-md-2">
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <button type="button" id="delete-post" class="btn btn-danger" style="float: none; padding : 6px" data-id="{{ $data->id }}"><i class="far fa-trash-alt"></i></button>
                                        <button type="button" id="edit-post" class="btn btn-primary" style="float: none; padding : 6px" data-id="{{ $data->id }}"><i class="far fa-edit"></i></button>
                                    </td>
                                </tbody>
                                @endforeach
                                <tfoot>
                                    <tr>
                                        <th hidden>Id</th>
                                        <th>Title</th>
                                        <th>Slug</th>
                                        <th>Author</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="row">
                                <div class="col-12 mt-2 mt-md-2">
                                    <ul class="pagination pagination_style1 justify-content-center">
                                        {{ @$datas->links('admin.pagination') }}
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection