<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">

    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto">
                <a class="navbar-brand" href="/admin">
                    <span class="brand-logo">
                        <img src="{{ asset( 'assets/images/admin/favicon.png' ) }}" alt="Bất động sản 24h">
                    </span>
                    <h2 class="brand-text">Admin</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse">
                    <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i>
                    <i class="d-none d-xl-block collapse-toggle-icon font-medium-4 text-primary" data-feather="disc" data-ticon="disc"></i>
                </a>
            </li>
        </ul>
    </div>

    <div class="shadow-bottom"></div>

    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            @foreach( $admin_menu as $key => $value )

                @if( $role_sidebar == 'super-admin' || in_array($value['name'], $permissions_sidebar) )

                    <li class="nav-item {{ request()->is( substr( $value['url'], 1 ) ) ? 'active' : '' }}">
                        <a class="d-flex align-items-center" href="{{ isset( $value['sub'] ) ? '#' : $value['url'] }}" title="{{ $value['title'] }}">
                            <i data-feather="{{ $value['icon'] }}"></i>
                            <span class="menu-title text-truncate" data-i18n="{{ $value['title'] }}">{{ $value['title'] }}</span>
                        </a>

                        @if( isset( $value['sub'] ) )

                            <ul class="menu-content">

                                @foreach( $value['sub'] as $value_sub )

                                    <li class="{{ request()->is( substr( $value_sub['url'], 1 ) ) ? 'active' : '' }}">
                                        <a class="d-flex align-items-center" href="{{ $value_sub['url'] }}" title="{{ $value_sub['title'] }}">
                                            <i data-feather="circle"></i>
                                            <span class="menu-item text-truncate" data-i18n="{{ $value_sub['title'] }}">{{ $value_sub['title'] }}</span>
                                        </a>
                                    </li>

                                @endforeach

                            </ul>

                        @endif

                    </li>

                @endif

            @endforeach

        </ul>
    </div>
</div>
<!-- END: Main Menu-->
