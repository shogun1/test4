<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', config('app.name', '@Master Layout'))</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset( 'assets/images/admin/favicon.png' ) }}">
        <link rel="apple-touch-icon" href="{{ asset( 'assets/images/admin/favicon.png' ) }}">
        <link rel="shortcut icon" href="{{ asset( 'assets/images/admin/favicon.png' ) }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/fonts/font-awesome/css/font-awesome.min.css' ) }}">

        <!-- Main CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/css/admin/main.css' ) }}">

        <!-- Custom CSS -->
        @yield('customStyle')

        <script>
            window.homeURL = "{{ url('/') }}";
        </script>

    </head>
    <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static @yield('bodyClass')" data-open="click" data-menu="vertical-menu-modern" data-col="">

        @include('theme::admin.layout.header')

        @include('theme::admin.layout.menu')

        <div id="main">
            @yield('content')
        </div>

        @include('theme::admin.layout.footer')

        <!-- Main JS -->
        <script src="{{ mix('assets/js/admin/main.js') }}"></script>

        <!-- Custom JS -->
        @yield('customScript')

        <script>
            $( window ).on( 'load', function() {
                if( feather ) {
                    feather.replace({
                        width: 14,
                        height: 14
                    });
                }
            })
        </script>

    </body>
</html>
