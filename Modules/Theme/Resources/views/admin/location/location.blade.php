@extends('theme::admin.layout.master')

@section('customStyle')
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/css/admin/project.css' ) }}">
@endsection

@section('customScript')
<script src="{{ mix('assets/js/admin/location.js') }}"></script>
@endsection

@section('bodyClass', 'admin-page module-project')

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <section id="dashboard-ecommerce">
                <div class="row match-height">
                    <!-- Medal Card -->
                    <div class="col-xl-4 col-md-6 col-12">
                        <form id="location-content-form" action="/admin/location/store" method="post">
                            @csrf
                            <div class="card">
                                <div class="card-body">
                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                    <input type="hidden" id="post-id" />
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-name">Name</label>
                                        <input type="text" name="name" id="post-name" class="form-control" value="" maxlength="200" required>
                                    </fieldset>
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-slug">Slug</label>
                                        <input type="text" name="slug" id="post-slug" class="form-control" value="" maxlength="200" maxlength="200" required>
                                    </fieldset>
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-type">Type</label>
                                        <input type="text" name="type" id="post-type" class="form-control" value="" maxlength="200" required>
                                    </fieldset>
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-name-with-type">Name With Type</label>
                                        <input type="text" name="post-name-with-type" id="post-name-with-type" class="form-control" value="" maxlength="200" required>
                                    </fieldset>
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-path">Path</label>
                                        <input type="text" name="path" id="post-path" class="form-control" value="" maxlength="200" required>
                                    </fieldset>
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-path-type">Path With Type</label>
                                        <input type="text" name="post-path-type" id="post-path-type" class="form-control" value="" maxlength="200" required>
                                    </fieldset>
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-code">Code</label>
                                        <input type="text" name="code" id="post-code" class="form-control" value="" required>
                                    </fieldset>
                                    <fieldset class="mb-2 check-valid">
                                        <label class="form-label" for="post-parent">Parent </label>
                                        <select class="form-select" aria-label="Default select example">
                                            <option selectedCity>Open this select menu</option>
                                            <option value="1">001</option>
                                            <option value="2">002</option>
                                        </select>
                                    </fieldset>
                                    <button type="button" id="save-post-type" class="btn btn-primary me-1 waves-effect waves-float waves-light">Add new location</button>
                                    <button type="button" id="save-edit-type" class="btn btn-primary me-1 waves-effect waves-float waves-light" style="display : none">Save Edit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--/ Medal Card -->

                    <!-- Table location -->
                    <div class="col-12 col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-datatable">
                                    <table class="dt-responsive table">
                                        <thead>
                                            <tr>
                                                <th hidden>Id</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Slug</th>
                                                <th scope="col">Name With Type</th>
                                                <th scope="col">Code</th>
                                                <th scope="col">Parent</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        @foreach ($datas as $item)
                                        <tbody>
                                            <tr>
                                                <td hidden> {{ $item->id }} </td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->name}}</td>
                                                <td style="text-align: center;" class="col-md-2">
                                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                                    <button type="button" id="delete-post-type" class="btn btn-danger" style="float: none; padding : 6px" data-id="{{ $item->id }}"><i class="far fa-trash-alt"></i></button>
                                                    <button type="button" id="edit-post-type" class="btn btn-primary" style="float: none; padding : 6px" data-id="{{ $item->id }}"><i class="far fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                        <tfoot>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Slug</th>
                                                <th scope="col">Name With Type</th>
                                                <th scope="col">Code</th>
                                                <th scope="col">Parent</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    {{ @$datas->links('admin.pagination') }}
                                </div>
                            </div>
                        </div>
                    </div>

            </section>
            <!-- Dashboard Ecommerce ends -->

        </div>
    </div>
</div>
<!-- END: Content-->
@endsection