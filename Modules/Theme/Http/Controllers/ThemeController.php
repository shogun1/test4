<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ThemeController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = $this->create_data();
        $customScript = "window.appData = " . json_encode( $data ) . ";\nwindow.appLink=[];";
        return view('theme::web.page.index', ['customScript' => $customScript] );
    }

    public function getSlug( $slug ) {
        $data = $this->create_data();
        $customScript = "window.appData = " . json_encode( $data ) . ";\nwindow.appLink=[];";
        return view('theme::web.page.index', ['customScript' => $customScript] );
    }

    private function create_data() {
        return array(
            'header' => $this->get_header(),
            'content' => array(),
            'footer' => $this->get_footer()
        );
    }

    private function get_header() {
        return array(
            'appTitle' => 'Bất Động Sản 24h',
            'headerLogo' => array(
                'src' => array(
                    'color' => asset('assets/images/demo/logo.png'),
                    'white' => asset('assets/images/demo/logo-white.png'),
                )
            ),
            'headerMenu' => array(
                array(
                    'title' => 'Nhà đất bán',
                    'link' => '/nha-dat-ban',
                    'current' => true,
                    'screen' => 'PropertyCategory'
                ),
                array(
                    'title' => 'Nhà đất cho thuê',
                    'link' => '/nha-dat-cho-thue',
                    'current' => false,
                    'screen' => 'PropertyCategory'
                ),
                array(
                    'title' => 'Dự án',
                    'link' => '/du-an',
                    'current' => false,
                    'screen' => 'ProjectCategory',
                    'sub_menu' => array(
                        array(
                            'title' => 'Demo 1',
                            'link' => '/du-an/demo-1',
                            'current' => false,
                            'screen' => 'ProjectSingle',
                        ),
                        array(
                            'title' => 'Demo 2',
                            'link' => '/du-an/demo-2',
                            'current' => false,
                            'screen' => 'ProjectSingle',
                        )
                    )
                ),
                array(
                    'title' => 'Video',
                    'link' => '/video',
                    'current' => false,
                    'screen' => 'VideoCategory'
                ),
                array(
                    'title' => 'Đại lý',
                    'link' => '/dai-ly',
                    'current' => false,
                    'screen' => 'AgentCategory'
                ),
                array(
                    'title' => 'Tin tức',
                    'link' => '/tin-tuc',
                    'current' => false,
                    'screen' => 'NewsCategory'
                )
            ),
            'headerButtons' => array(
                'submitPropertyButton' => array(
                    'title' => 'Đăng tin',
                    'link' => '/dang-tin',
                    'type' => 'link',
                    'screen' => 'SubmitPropertyPage'
                ),
                'notificationButton' => array(
                    'title' => 'Thông báo mới',
                    'link' => '/tai-khoan/thong-bao-moi',
                    'type' => 'link',
                    'screen' => 'MyAccountNotification'
                ),
                'whistListButton' => array(
                    'title' => 'Trang yêu thích',
                    'link' => '/tai-khoan/trang-yeu-thich',
                    'type' => 'link',
                    'screen' => 'MyAccountWhistList'
                ),
                'loginButton' => array(
                    'title' => 'Đăng nhập',
                    'link' => '/tai-khoan/dang-nhap',
                    'type' => 'link',
                    'screen' => 'MyAccountLogin'
                )
            ),
            'headerSearchForm' => array(
                'title' => 'KHỞI ĐẦU THAY ĐỔI',
                'placeholder' => 'Nhập địa điểm, khu dân cư, tòa nhà...',
                'typeRadio' => array(
                    array(
                        'value' => 'buy',
                        'text' => 'Mua bán'
                    ),
                    array(
                        'value' => 'rent',
                        'text' => 'Cho thuê'
                    ),
                    array(
                        'value' => 'project',
                        'text' => 'Dự án'
                    )
                ),
                'submitButtonText' => 'Tìm kiếm'
            ),
            'headerBanner' => array(
                'src' => '/assets/images/demo/golden-gate.png'
            )
        );
    }

    private function get_footer() {

        $footer_logo = '<figure class="footer-logo"><img src="/assets/images/demo/logo-white.png" alt="Bất Động Sản 24h"></figure>';
        $footer_hotline = '<div class="footer-hotline"><i class="icon fal fa-phone-volume"></i><span class="text">Hotline<a href="tel:18006868">1800.6868</a></span></div>';
        $footer_tbbct = '<a class="footer-tbbct" href="#"><img src="/assets/images/demo/tbbct.png" alt="Bất Động Sản 24h"></a>';

        $footer_address = '<div class="footer-contact">';
        $footer_address .= '<h3>CÔNG TY WAYLAND COOPERATION</h3>';
        $footer_address .= '<div class="item"><i class="icon fal fa-map-marker-alt fa-fw"></i>231 Bùi Điền, P.5, Q.8, TP. HCM</div>';
        $footer_address .= '<div class="item"><i class="icon fal fa-mobile-android-alt fa-fw"></i>0393.222.333</div>';
        $footer_address .= '<div class="item"><i class="icon fal fa-envelope fa-fw"></i>hotro@batdongsan24h.vn</div>';
        $footer_address .= '</div>';

        $menu_1 = array(
            'menuTitle' => 'HƯỚNG DẪN',
            'menuList' => array(
                array(
                    'title' => 'Báo giá & Hỗ trợ',
                    'link' => '/bao-gia-ho-tro',
                    'current' => false,
                    'screen' => 'Page'
                ),
                array(
                    'title' => 'Sitemap',
                    'link' => '/sitemap',
                    'current' => false,
                    'screen' => 'Page'
                ),
                array(
                    'title' => 'Liên hệ',
                    'link' => '/lien-he',
                    'current' => false,
                    'screen' => 'Page'
                ),
            )
        );

        $menu_2 = array(
            'menuTitle' => 'QUY ĐỊNH',
            'menuList' => array(
                array(
                    'title' => 'Quy định đăng tin',
                    'link' => '/quy-dinh-dang-tin',
                    'current' => false,
                    'screen' => 'Page'
                ),
                array(
                    'title' => 'Điều khoản thỏa thuận',
                    'link' => '/dieu-khoan-thoa-thuan',
                    'current' => false,
                    'screen' => 'Page'
                ),
                array(
                    'title' => 'Giải quyết khiếu nại',
                    'link' => '/giai-quyet-khieu-nai',
                    'current' => false,
                    'screen' => 'Page'
                ),
            )
        );

        $footer_follow = '<div class="footer-follow">';
        $footer_follow .= '<h3>ĐĂNG KÝ NHẬN TIN</h3>';
        $footer_follow .= '<form class="subscribe-form"><fieldset><input type="email" name="email" placeholder="Nhập Email của bạn"><button type="button" class="button submit-button"><i class="icon fal fa-paper-plane"></i></button></fieldset></form>';
        $footer_follow .= '<div class="social-links">Theo dõi chúng tôi trên';
        $footer_follow .= '<a class="item youtube" href="#"><i class="icon fab fa-youtube"></i></a>';
        $footer_follow .= '<a class="item facebook" href="#"><i class="icon fab fa-facebook-f"></i></a>';
        $footer_follow .= '<a class="item linkedin" href="#"><i class="icon fab fa-linkedin-in"></i></a>';
        $footer_follow .= '</div>';
        $footer_follow .= '</div>';

        $copyright = '<p class="copyright">© 2021 | Bản quyền thuộc về Wayland Cooperation | Hotline 0393.222.333</p>';

        return array(
            'topSection' => array(
                'footer_logo' => $footer_logo,
                'footer_hotline' => $footer_hotline,
                'footer_tbbct' => $footer_tbbct,
            ),
            'footerAddress' => $footer_address,
            'menu1' => $menu_1,
            'menu2' => $menu_2,
            'footerFollow' => $footer_follow,
            'copyRight' => $copyright
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('theme::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('theme::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('theme::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
