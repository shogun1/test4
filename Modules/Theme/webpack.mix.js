const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath( 'public' ).mergeManifest();

/** Vendors */
mix.copyDirectory( __dirname + '/Resources/assets/vendor/font-awesome-pro/webfonts', 'public/assets/vendor/webfonts' )
    .styles(
        [
            __dirname + '/Resources/assets/vendor/bootstrap/css/bootstrap.css', // 5.1.3
            __dirname + '/Resources/assets/vendor/font-awesome-pro/css/all.css', // 5.15.3
            __dirname + '/Resources/assets/vendor/owl-carousel-2/assets/owl.carousel.css', // 2.3.4
        ],
        'public/assets/vendor/css/vendor.css'
    )
    .scripts(
        [
            __dirname + '/Resources/assets/vendor/jquery/jquery.js', // 3.6.0
            __dirname + '/Resources/assets/vendor/bootstrap/js/bootstrap.bundle.js', // 5.1.3
            __dirname + '/Resources/assets/vendor/owl-carousel-2/owl.carousel.js', // 2.3.4
        ],
        'public/assets/vendor/js/vendor.js'
    );

/** Admin */
mix.copyDirectory( __dirname + '/Resources/assets/data', 'public/assets/data' )
    .copyDirectory( __dirname + '/Resources/assets/fonts', 'public/assets/fonts' )
    .copyDirectory( __dirname + '/Resources/assets/js/admin/tinymce', 'public/assets/js/admin/tinymce' )
    .styles(
        [
            __dirname + '/Resources/assets/css/admin/vendors.min.css',
            __dirname + '/Resources/assets/css/admin/apexcharts.css',
            __dirname + '/Resources/assets/css/admin/toastr.min.css',
            __dirname + '/Resources/assets/css/admin/bootstrap.css',
            __dirname + '/Resources/assets/css/admin/bootstrap-extended.css',
            __dirname + '/Resources/assets/css/admin/dataTables.bootstrap5.min.css',
            __dirname + '/Resources/assets/css/admin/responsive.bootstrap5.min.css',
            __dirname + '/Resources/assets/css/admin/flatpickr.min.css',
            __dirname + '/Resources/assets/css/admin/form-flat-pickr.css',
            __dirname + '/Resources/assets/css/admin/colors.css',
            __dirname + '/Resources/assets/css/admin/components.css',
            __dirname + '/Resources/assets/css/admin/vertical-menu.css',
            __dirname + '/Resources/assets/css/admin/main.css'
        ],
        'public/assets/css/admin/main.css'
    )
    .scripts(
        [
            __dirname + '/Resources/assets/js/admin/vendors.min.js',
            __dirname + '/Resources/assets/js/admin/apexcharts.js',
            __dirname + '/Resources/assets/js/admin/toastr.min.js',
            __dirname + '/Resources/assets/js/admin/jquery.dataTables.min.js',
            __dirname + '/Resources/assets/js/admin/dataTables.bootstrap5.min.js',
            __dirname + '/Resources/assets/js/admin/dataTables.responsive.min.js',
            __dirname + '/Resources/assets/js/admin/responsive.bootstrap5.js',
            __dirname + '/Resources/assets/js/admin/flatpickr.min.js',
            __dirname + '/Resources/assets/js/admin/jquery.stringtoslug.js',
            __dirname + '/Resources/assets/js/admin/tinymce/tinymce.min.js',
            __dirname + '/Resources/assets/js/admin/app-menu.js',
            __dirname + '/Resources/assets/js/admin/app.js',
            __dirname + '/Resources/assets/js/admin/table-datatables-advanced.js',
            __dirname + '/Resources/assets/js/admin/main.js',
        ],
        'public/assets/js/admin/main.js'
    );

/** Web */
mix.react( __dirname + '/Resources/assets/js/web/render.js', 'assets/js/web/render.js' );

/** Other assets */
mix.copyDirectory( __dirname + '/Resources/assets/images', 'public/assets/images' );

if (mix.inProduction()) {
    mix.version();
}
