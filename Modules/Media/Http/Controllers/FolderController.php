<?php

namespace Modules\Media\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Permissions\Entities\Roles;
use Modules\Media\Entities\Media;
use Modules\Permissions\Entities\Departments;
use Modules\Permissions\Entities\RolesGroup;
use Session;
use App\User;
use Auth;
use Modules\Media\Entities\Folder;
use DB;
use Gate;

class FolderController extends Controller
{
    use ValidatesRequests;  

    public function index() {
        $folder = Folder::where('user_id', Auth::id())->where('is_delete', 0)->with('children')->where('parent_id', 0)->orderBy('id', 'desc')->get();
        $data = [];
        foreach($folder as $child_lv1) {
            $child_lv1->child_level = 1;
            $data[] = $child_lv1;
            if($child_lv1->children) {
                foreach($child_lv1->children as $child_lv2) {
                    $child_lv2->child_level = 2;
                    $data[] = $child_lv2;
                    if($child_lv2->children) {
                        foreach($child_lv2->children as $child_lv3) {
                            $child_lv3->child_level = 3;
                            $data[] = $child_lv3;
                        }
                    }
                }
            }
        }

        return view('media::admin.folder.index', compact('data'));
    }

    public function getFolderAdd() {
        $folder = Folder::where('user_id', Auth::id())->with('children')->where('parent_id', 0)->where('is_delete', 0)->get();

        return view('media::admin.folder.add', compact('folder'));
    }

    public function postFolderAdd(Request $request) {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $parent = 0;
        $level = 0;
        if($request->parent != '') {
            $parent_folder = Folder::find($request->parent);
            if(!$parent_folder) {
                return redirect()->back()->with('alert_error', 'Parent not found');
            }
            if($parent_folder->level == 2) {
                $parent = $parent_folder->parent_id;
                $level = 2;
            }else {
                $parent = $parent_folder->id;
                $level = $parent_folder->level + 1;
            }
        }

        Folder::create([
            'parent_id' => $parent,
            'level' => $level,
            'user_id' => Auth::id(),
            'name' => $request->name,
            'slug' => checkSlug_exists('folder', $request->name),
            'user_folder' => null,
            'is_delete' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->route('admin.media.folder')->with('alert_success', 'Create folder successful');
    }

    public function getFolderEdit($id) {
        $folder = Folder::findOrFail($id);
        $folder_child_lv1 = Folder::where('user_id', Auth::id())->where('id', '!=', $folder->id)->where('parent_id', 0)->where('is_delete', 0)->get();
        $user = Auth::user();
        $role = Roles::where('id', $user->role)->where('is_delete', 0)->first();
        if(!$role) {
            return redirect()->back()->with('alert_error', 'Role does not exist');
        }
        $department_check = RolesGroup::where('role_id', $role->id)->first();
        if($department_check) {
            $roles = RolesGroup::where('department_id', $department_check->department_id)->pluck('role_id')->toArray();
            $users = User::whereIn('role', $roles)->where('id', '!=', $user->id)->get();
        }else {
            $users = User::where('role', $role->id)->where('id', '!=', $user->id)->get();
        }
        
        $data = [
            'folder' => $folder,
            'folder_child_lv1' => $folder_child_lv1,
            'users' => $users,
        ];

        return view('media::admin.folder.edit', $data);
    }

    public function postFolderEdit($id, Request $request) {
        $folder = Folder::where('user_id', Auth::id())->where('id', $id)->where('is_delete', 0)->firstOrFail();
        $users = null;

        if($request->users != '') {
            $users = implode(',', $request->users);
        }

        if($request->parent == '') {
            $folder->update([
                'name' => $request->name,
                'parent_id' => 0,
                'level' => 0,
                'user_folder' => $users,
            ]);
        }else {
            $parent = Folder::where('id', $request->parent)->where('is_delete', 0)->firstOrFail();
            if($parent && $folder->level != $parent->level) {
                $folder->update([
                    'name' => $request->name,
                    'parent_id' => $parent->id,
                    'level' => $parent->level + 1,
                    'user_folder' => $users,
                ]);
            }
        }
        
        return redirect()->route('admin.media.folder')->with('alert_success', 'Update media folder successfully');
    }

    public function getFolderDelete($id) {
        $folder = Folder::findOrFail($id);
        $folder->update([
            'is_delete' => 1
        ]);

        return redirect()->back()->with('alert_success', 'Delete folder successfully');
    }

    public function postFilter(Request $request) {
        $user = Auth::user();
        if($request->folder_id == 'user') {
            Session::put('folder_id', 'user');
            $role = Roles::where('id', $user->role)->where('is_delete', 0)->first();
            if($role) {
                if($role->slug == 'super-admin') {
                    $users = User::where('is_delete', 0)->select('id', 'username')->get();
                }else {
                    $users = [$user];
                }
                $data = [
                    'users' => $users,
                ];
    
                return response()->json($data);
            }
        }else {
            $folder = Folder::where('id', $request->folder_id)->where('is_delete', 0)->first();
            if($folder) {
                Session::put('folder_id', $folder->id);
                $users = User::where('id', explode(',', $folder->user_folder))->where('is_delete', 0)->get();
                $data = [
                    'users' => $users
                ];
                
                return response()->json($data);
            }
        }
    }

}
