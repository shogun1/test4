<?php

namespace Modules\Media\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Modules\Media\Entities\Folder;
use Modules\Media\Entities\Media;
use Modules\Media\Entities\SettingMedia;
use Modules\Permissions\Entities\Roles;
use App\Http\Controllers\OpenSource\OpenSource;
use App\Services\UploadFileService;
use App\User;
use Session;
use Storage;
use DB;
use Auth;
use Gate;
use Artisan;

class MediaController extends Controller
{
    use ValidatesRequests;

    public function __construct(UploadFileService $uploadFileSer)
    {
        $this->uploadFileSer = $uploadFileSer;
    }

    public function getIndex() {
        Session::put('folder_id', 'user');
        $user = Auth::user();
        $role = Roles::where('id', $user->role)->where('is_delete', 0)->first();
        if($role) {
            if($role->slug == 'super-admin') {
                $users = User::where('is_delete', 0)->select('id', 'username')->get();
                $users_folder = $users->pluck('id')->toArray();
            }else {
                $users = User::where('id', $user->id)->get();
                $users_folder = $users->pluck('id')->toArray();
            }
            
            $media = DB::table('media')->whereIn('media_author', $users_folder)->whereNull('media_folder')->where('is_delete', 0)
                ->orderBy('media_id', 'DESC')->offset(0)->limit(100)->get();

            if($user->is_media_manager) {
                $folder_ = Folder::where('is_delete', 0)->where('user_id', $user->id)->where('level', 0)->get();
                $folder_tabs = Folder::whereRaw("find_in_set('".$user->id."',user_folder)")->where('user_id', '!=', $user->id)->where('is_delete', 0)->get();
                $folder = $folder_->merge($folder_tabs);
            }else {
                $folder = folder::whereRaw("find_in_set('".$user->id."',user_folder)")->where('is_delete', 0)->get();
            }

            $data = [
                'media' => $media,
                'users' => ($users) ? $users : [],
                'folder' => $folder,
            ];

            return view('media::admin.system', $data);
        }
    }

    function postMedia(Request $request){
        $this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',]);
        $user = Auth::user();

        $file = $request->file('file');
        $file_size = $file->getSize();
        $user = Auth::user();
        $path = 'uploads/media';
        $extension = $file->getClientOriginalExtension();
        $mime_type = $file->getClientMimeType();
        $file_type = $this->get_media_type($extension);
        $slug_name = str_replace('.' . $extension, '', trim($file->getClientOriginalName()));
        $file_name = Str::slug($slug_name);
        $filename_origin = $file_name;

        $width = 0;
        $height = 0;
        $media_name = $file_name;
        if($file_type == 'image' || $file_type == 'icon'){
            list($width, $height) = getimagesize($file);
            if($width < $height){
                $media_style = 'portrait';
            }else{
                $media_style = 'landscape';
            }
            $file_name = $file_name . '_size_' . $width . 'x' . $height;
        }
        $get_file_name = DB::table('media')->where('media_name', $file_name)->count();
        $file_index = 2;
        while($get_file_name > 0){
            $file_name = $file_name . '-' . $file_index;
            $get_file_name = DB::table('media')->where('media_name', $file_name)->where('media_extension', $extension)->count();
            $file_index++;
        }

        $file_path = $this->uploadFileSer->uploadImageThumb($path, $file, $file_name . '.' . $extension);
        $media_source = $file_path['main'];
        $media_large = $file_path['large'];
        $media_thumb = $file_path['thumbnail'];

        $media_style = 'landscape';
        if($file_type == 'image' || $file_type == 'icon'){
            $media_url = $media_source;
            if($file_type == 'image'){
            }
        }else{
            $media_url = url('modules/layout/contents/images/media_thumbs/' . $file_type . '.png');
        }

        $folder_id = Session::get('folder_id');
        if($folder_id == 'user') {
            $folder = null;
        }else {
            $check_folder = Folder::where('id', $folder_id)->where('is_delete', 0)->select('id')->first();
            if(!$check_folder) $folder =null;
            $folder = $check_folder->id;
        }

        $media_author = !Auth::guest() ? Auth::user()->id : 0;
        $media = [
            'media_name' => $file_name,
            'media_extension' => $extension,
            'media_width' => $width,
            'media_height' => $height,
            'media_style' => $media_style,
            'media_size' => $file_size,
            'mime_type' => $mime_type,
            'media_type' => $file_type,
            'media_source' => $media_source,
            'media_large' => $media_large,
            'media_thumb' => $media_thumb,
            'media_url' => $media_url,
            'media_title' => $media_name,
            'media_alt' => $media_name,
            'media_description' => '',
            'media_folder' => $folder,
            'media_author' => $media_author,
            'created_at' => date('Y-m-d H:i:s')
        ];
        $media_id = DB::table('media')->insertGetId($media);
        $media['media_id'] = $media_id;
        return $media;
    }

    function getMediaLazy(Request $request){
        $offset = $request->offset;
        $limit = $request->limit;
        
        $media_type = $request->media_type;
        $media_users = isset($request->media_users) ? (array)$request->media_users : [];
        $media_search = $request->media_search;
        $media_folder = $request->media_folder;

        $media = [];
        $user = Auth::user();
        if($media_folder == 'user') {
            // $role = Roles::where('id', $user->role)->where('is_delete', 0)->first();
            // if($role) {
                // if($role->slug == 'super-admin') {
                //     $users = User::where('is_delete', 0)->pluck('id')->toArray();
                //     if(count($media_users) > 0) {
                //         $media = Media::whereIn('media_author', $media_users)->whereNull('media_folder')->where('is_delete', 0);
                //     }else {
                //         $media = Media::whereIn('media_author', $users)->whereNull('media_folder')->where('is_delete', 0);
                //     }
                // }else {
                    $media = Media::where('media_author', $user->id)->whereNull('media_folder')->where('is_delete', 0);
                // }

                if($media_search != ''){
                    $media = $media->where('media_name', 'LIKE', '%'.$media_search.'%');
                }

                $media = $media->orderBy('media_id', 'DESC')->offset(0)->limit(50)->get();
            // }
        }else {
            $folder = Folder::find($media_folder);
            if($folder) {
                if($folder->user_folder) {
                    $media = Media::where('media_folder', $folder->id)->where('is_delete', 0);
                    if(count($media_users) > 0) {
                        $media = $media->whereIn('media_author', $media_users);
                    }

                    if($media_search != ''){
                        $media = $media->where('media_name', 'LIKE', '%'.$media_search.'%');
                    }

                    $media = $media->orderBy('media_id', 'DESC')->offset($offset)->limit($limit)->get();
                }
            }
        }

        return response()->json($media);
    }

    function getMediaFilter(Request $request){
        $media_type = $request->media_type;
        $media_users = isset($request->media_users) ? (array)$request->media_users : [];
        $media_search = $request->media_search;
        $media_folder = $request->media_folder;

        $media = [];
        $user = Auth::user();
        if($media_folder == 'user') {
            Session::put('folder_id', 'user');
            $role = Roles::where('id', $user->role)->where('is_delete', 0)->first();
            if($role) {
                if($role->slug == 'super-admin') {
                    $users = User::where('is_delete', 0)->pluck('id')->toArray();
                    if(count($media_users) > 0) {
                        $media = Media::whereIn('media_author', $media_users)->whereNull('media_folder')->where('is_delete', 0);
                    }else {
                        $media = Media::whereIn('media_author', $users)->whereNull('media_folder')->where('is_delete', 0);
                    }
                }else {
                    $media = Media::where('media_author', $user->id)->whereNull('media_folder')->where('is_delete', 0);
                }

                if($media_search != ''){
                    $media = $media->where('media_name', 'LIKE', '%'.$media_search.'%');
                }

                $media = $media->orderBy('media_id', 'DESC')->offset(0)->limit(50)->get();
            }
        }else {
            $folder = Folder::find($media_folder);
            if($folder) {
                Session::put('folder_id', $folder->id);
                if($folder->user_folder) {
                    $media = Media::where('media_folder', $folder->id)->where('is_delete', 0);
                    if(count($media_users) > 0) {
                        $media = $media->whereIn('media_author', $media_users);
                    }

                    if($media_search != ''){
                        $media = $media->where('media_name', 'LIKE', '%'.$media_search.'%');
                    }

                    $media = $media->orderBy('media_id', 'DESC')->offset(0)->limit(50)->get();
                }
            }
        }

        return response()->json($media);
    }

    function getMediaAlone(Request $request){
        $media_id = $request->media_id;
        $data = DB::table('media')->LeftJoin('users', 'media.media_author', '=', 'users.id')->where('media.media_id', $media_id)->select('media.*', 'users.username')->first();

        return response()->json($data);
    }

    function postSaveMedia(Request $request){
        $media_id = $request->media_id;
        $media_title = $request->media_title;
        $media_alt = $request->media_alt;
        $media_description = $request->media_description;
        $media = ['media_title' => $media_title, 'media_alt' => $media_alt, 'media_description' => $media_description];
        DB::table('media')->where('media_id', $media_id)->update($media);
        return 'true';
    }

    function postDeleteMedia(Request $request){
        $media_id = $request->media_id;
        $media = DB::table('media')->where('media_id', $media_id)->first();
        if(is_null($media)) {
            return false;
        }
        // Storage::disk('local')->delete(env('FTP_CDN_PATH').$media->media_location . '/' . $media->media_name . '.' . $media->media_extension);
        DB::table('media')->where('media_id', $media_id)->update(['is_delete' => 1]);
        return 'true';
    }

    function postDeleteMultiMedia(Request $request){
        $media_ids = $request->media_ids;
        if(count($media_ids) > 0){
            foreach($media_ids as $media_id){
                $media = DB::table('media')->where('media_id', $media_id)->first();
                if(is_null($media)) {
                    return false;
                }
                // Storage::disk('local')->delete(env('FTP_CDN_PATH').$media->media_location . '/' . $media->media_name . '.' . $media->media_extension);
                DB::table('media')->where('media_id', $media_id)->delete();
            }
            return 'true';
        }else{
            return 'false';
        }
    }

    function get_media_type($type){
        $image = ['JPE','JPEG','JPG','PNG', 'GIF', 'SVG', 'ICO'];
        $icon = ['ICO'];
        $res = 'other';
        if(in_array(strtoupper($type), $icon)){
            $res = 'icon';
        }
        if(in_array(strtoupper($type), $image)){
            $res = 'image';
        }
        return $res;
    }

    public function getSetting() {
        Gate::allows('modules', 'media_setting_get_edit');

        $users_media = User::where('is_media_manager', 1)->where('is_delete', 0)->get();
        $settings_media = SettingMedia::pluck('setting_value', 'setting_name')->toArray();

        $data = [
            'users_media' => $users_media,
            'settings_media' => $settings_media,
            'setting_permission_delete' => explode(',',$settings_media['media_user_permission_delete']),
        ];
        return view('media::admin.setting.index', $data);
    }

    public function postSetting(Request $request) {
        Gate::allows('modules', 'media_setting_post_edit');

        $this->validate($request, [
            'media_large_auto_resize_width' => 'numeric',
            'media_large_auto_resize_height' => 'numeric',
            'media_thumbnail_auto_resize_width' => 'numeric',
            'media_thumbnail_auto_resize_height' => 'numeric',
            'media_auto_watermark' => 'mimes:jpeg,jpg,png,gif|max:10000'
        ]);

        if($request->media_large_auto_resize_width) {
            $data['media_large_auto_resize_width'] = $request->media_large_auto_resize_width;
        }

        if($request->media_large_auto_resize_height) {
            $data['media_large_auto_resize_height'] = $request->media_large_auto_resize_height;
        }

        if($request->media_thumbnail_auto_resize_width) {
            $data['media_thumbnail_auto_resize_width'] = $request->media_thumbnail_auto_resize_width;
        }

        if($request->media_thumbnail_auto_resize_height) {
            $data['media_thumbnail_auto_resize_height'] = $request->media_thumbnail_auto_resize_height;
        }
        
        if($request->hasFile('media_auto_watermark')) {
            $data['media_auto_watermark'] = OpenSource::images($request->media_auto_watermark, 'public/media_setting/');
        }

        if(isset($request->media_user_permission_delete)) {
            $users = User::whereIn('id', $request->media_user_permission_delete)->where('is_media_manager', 1)->where('is_delete', 0)->pluck('id')->toArray();
            $data['media_user_permission_delete'] = implode(',', $users);
        }

        foreach($data as $key => $value) {
            SettingMedia::where('setting_name', $key)->update([
                'setting_value' => $value,
            ]);
        }

        return redirect()->route('admin.media.settings')->with('alert_success', 'Update media settings successfully');
    }

    public function getEditor() {
        Session::put('folder_id', 'user');
        $user = Auth::user();
        $folder = Folder::query();
        if($user->is_media_manager == 1) {
                $folder = $folder->where('user_id', $user->id);
        }
        $folder = $folder->OrWhereRaw("find_in_set('".$user->id."',user_folder)")
                ->where('is_delete', 0)->orderBy('created_at', 'desc')->get();
        
        $media = DB::table('media')->whereIn('media_author', [$user->id])->whereNull('media_folder')->where('is_delete', 0)
                ->orderBy('media_id', 'DESC')->offset(0)->limit(100)->get();


        $data = [
            'folders' => $folder,
            'media' => $media,
        ];

        return view('media::admin.demo_editor', $data);
    }
}
