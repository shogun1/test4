<?php

namespace Modules\Media\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Media extends Model
{
    use HasFactory;

    protected $table = 'media';
    protected $fillable = ['media_id', 'media_name', 'media_extension', 'media_width', 'media_height', 'media_style', 'media_size', 'mime_type', 'media_type', 'media_source', 'media_thumb', 'media_url', 'media_alt', 'media_description', 'media_folder', 'media_author', 'is_delete'];
    
    protected static function newFactory()
    {
        return \Modules\Media\Database\factories\MediaFactory::new();
    }
}
