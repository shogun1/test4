<?php

namespace Modules\Media\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SettingMedia extends Model
{
    use HasFactory;

    protected $table = 'setting_media';
    protected $fillable = ['setting_name', 'setting_value'];
    
    protected static function newFactory()
    {
        return \Modules\Media\Database\factories\SettingMediaFactory::new();
    }
}
