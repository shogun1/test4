<?php

namespace Modules\Media\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\User;
use Modules\Permissions\Entities\Roles;
use Auth;
use DB;

class Folder extends Model
{
    use HasFactory;

    protected $table = 'folder';
    protected $fillable = ['parent_id', 'level', 'user_id', 'name', 'slug', 'user_folder', 'is_delete'];
    
    protected static function newFactory()
    {
        return \Modules\Media\Database\factories\FolderFactory::new();
    }

    public function children() { 
        return $this->hasMany('Modules\Media\Entities\Folder', 'parent_id', 'id'); 
    }

    public function children_hidden($parent_id, $folder_id) {
        return Folder::where('id', '!=', $folder_id)->where('parent_id', $parent_id)->get();
    }

    public function UserFolder($user_folder) {
        if($user_folder == null) {
            return [];
        }
        $users = explode(',', $user_folder);
        $users = User::whereIn('id', explode(',', $user_folder))->where('is_delete', 0)->get();
        $user = Auth::user();
        if($user->permission == 'super-admin') {
            return $users;
        }else {
            $role = Roles::where('slug', $user->permission)->where('type', 'department')->where('is_delete', 0)->first();
            if($role) {
                $departments = json_decode($role->sponsor_slug);
                $departments_users = [];
                foreach($users as $value) {
                    if(in_array($value->permission, $departments)) {
                        $departments_users[] = $value;
                    }
                }
                return $departments_users;
            }
        }
        return [];
    }
}
