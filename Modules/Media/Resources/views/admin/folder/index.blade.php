@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Folder</h4>
                        <a href="{{ route('admin.media.folder.add') }}"
                            class="button-title float-right btn btn-outline-primary waves-effect width-md waves-light">Create folder</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @include('admin.boxes.notify')
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                    <thead>
                                        <tr>
                                            <th>Folder project</th>
                                            <th>User folder</th>
                                            <th>Ngày khởi tạo</th>
                                            <th>Xóa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $value)
                                        <tr>
                                            <td><a href="{{ route('admin.media.folder.edit', ['id' => $value->id]) }}"
                                                    class="text-primary">
                                                   @if ($value->child_level == 2)
                                                        ─ 
                                                   @elseif($value->child_level == 3)
                                                        ── 
                                                   @endif {{$value->name}}</a></td>
                                            <td>
                                                @foreach ($value->UserFolder($value->user_folder) as $user)
                                                    <span class="badge badge-success">{{ $user->username }}</span>
                                                @endforeach
                                            </td>
                                            <td>{{ $value->created_at }}</td>
                                            <td style="white-space: nowrap; width: 1%;">
                                                <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                                    <div class="btn-group btn-group-sm" style="float: none;"><a
                                                            href="{{ route('admin.media.folder.delete', ['id' => $value->id]) }}"
                                                            onclick="return confirm('Bạn muốn xóa thư mục này?')"><button
                                                                type="button"
                                                                class="tabledit-edit-button btn btn-danger"
                                                                style="float: none;"><span
                                                                    class="mdi mdi-delete"></span></button></a></div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- end .table-responsive-->
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2017 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->
</div>
@endsection
