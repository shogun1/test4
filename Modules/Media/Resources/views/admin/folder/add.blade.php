@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Create folder</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">batdongsan24h.vn</a></li>
                                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                <li class="breadcrumb-item active">General Elements</li>
                            </ol>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- Form row -->
            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <form method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Name folder</label>
                                    <input type="text" class="form-control" id="title" value="{{ old('name') }}"
                                        name="name">
                                </div>
                                <div class="form-group">
                                    <label for="">Parent folder</label>
                                    <select class="form-control" name="parent" id="">
                                        <option value="">None</option>
                                        @foreach($folder as $child_lv1)
                                            <option value="{{ $child_lv1->id }}">{{ $child_lv1->name }}</option>
                                            @if($child_lv1->children)
                                                @foreach ($child_lv1->children as $child_lv2)
                                                <option value="{{ $child_lv2->id }}">--{{ $child_lv2->name }}</option>
                                                @if($child_lv2->children)
                                                    @foreach ($child_lv2->children as $child_lv3)
                                                    <option value="{{ $child_lv3->id }}">----{{ $child_lv3->name }}</option>
                                                    @endforeach
                                                @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end col -->

            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2017 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>
@endsection
