<div class="x_content">
    <div class="choose_img_lib post_single_image">
        <div class="form-group">
            <div class="img_wrapper">
                <div class="img_show">
                    <div class="img_thumbnail">
                        <div class="img_centered">
                            <img class="show_img_lib"
                                src="{{ asset('image/no-image.png') }}"
                                alt="Featured Image">
                        </div>
                    </div>
                    <div class="remove_featured_image">
                        <button><i class="dashicons dashicons-no-alt"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group bottom_five">
            <a href="javascript:void(0);" class="open_img_lib post_image_choose_from_library" gallery="false">{{ __('Set featured image') }}</a>
        </div>
        <input type="hidden" class="fill_img_lib" name="post_img"
            value="{{ asset('image/no-image.png') }}">
    </div>
</div>
@include('media::admin.media_modal')
@push('css')
<link rel="stylesheet" href="{{ asset('modules/layout/contents/admin/css/media.css') }}">
<link rel="stylesheet" href="{{ asset('modules/layout/contents/admin/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('modules/layout/contents/admin/css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('modules/layout/contents/admin/css/app.css') }}">
<script src="{{ asset('modules/layout/contents/admin/js/vendor.bundle.base.js') }}"></script>
@endpush
@push('js')
<script src="{{ asset('modules/layout/contents/admin/js/app.js') }}"></script>
<script src="{{ asset('modules/layout/contents/admin/js/custom.js') }}"></script>
@endpush