@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Media setting</h4>
                        {{-- <div class="page-title-right">
                            <ol class="breadcrumb p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Minh Hoàng container</a></li>
                                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                <li class="breadcrumb-item active">General Elements</li>
                            </ol>
                        </div> --}}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @include('admin.boxes.notify')

            <!-- Form row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            {{-- <h4 class="header-title mb-4">Form row</h4> --}}

                            <form method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="col-form-label">Auto resize large</label>
                                        <div class="input-group mt-2">
                                            <span class="input-group-prepend">
                                                <label class="input-group-text width-xs" for="dataWidth">Width</label>
                                            </span>
                                            <input type="text" class="form-control" name="media_large_auto_resize_width" id="dataWidth" value="{{ $settings_media['media_large_auto_resize_width'] }}" placeholder="width">
                                            <span class="input-group-append">
                                                <span class="input-group-text">px</span>
                                            </span>
                                        </div>
                                        <div class="input-group mt-2">
                                            <span class="input-group-prepend">
                                                <label class="input-group-text width-xs" for="dataHeight">Height</label>
                                            </span>
                                            <input type="text" class="form-control" name="media_large_auto_resize_height" id="dataHeight" value="{{ $settings_media['media_large_auto_resize_height'] }}" placeholder="height">
                                            <span class="input-group-append">
                                                <span class="input-group-text">px</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="col-form-label">Auto resize thumbnail</label>
                                        <div class="input-group mt-2">
                                            <span class="input-group-prepend">
                                                <label class="input-group-text width-xs" for="dataWidth">Width</label>
                                            </span>
                                            <input type="text" class="form-control" name="media_thumbnail_auto_resize_width" id="dataWidth" value="{{ $settings_media['media_thumbnail_auto_resize_width'] }}" placeholder="width">
                                            <span class="input-group-append">
                                                <span class="input-group-text">px</span>
                                            </span>
                                        </div>
                                        <div class="input-group mt-2">
                                            <span class="input-group-prepend">
                                                <label class="input-group-text width-xs" for="dataHeight">Height</label>
                                            </span>
                                            <input type="text" class="form-control" name="media_thumbnail_auto_resize_height" id="dataHeight" value="{{ $settings_media['media_thumbnail_auto_resize_height'] }}" placeholder="height">
                                            <span class="input-group-append">
                                                <span class="input-group-text">px</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputPassword4" class="col-form-label">Auto watermark</label>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="file" class="form-control" name="media_auto_watermark" id="example-fileinput" onchange="onFileSelected(event)">
                                            </div>
                                            <img id="myimage" src="{{ isset($settings_media['media_auto_watermark']) ? $settings_media['media_auto_watermark'] : '' }}" alt height="200" class="img-thumbnail">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inputAddress" class="col-form-label">Users permission delete media</label>
                                        <select class="form-control select2-multiple" name="media_user_permission_delete[]" data-toggle="select2" multiple="multiple" data-placeholder="User ...">
                                            <optgroup label="User folder">
                                                @foreach ($users_media as $user)
                                                    <option {{ in_array($user->id, $setting_permission_delete) ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->username }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>
@endsection
@push('css')
    <link href="{{ asset('assets\libs\select2\select2.min.css') }}" rel="stylesheet" type="text/css">
    <style>
        img#myimage {
            margin-top: 10px;
        }
        button.btn.btn-primary {
            margin-top: 10px;
        }
    </style>
@endpush
@push('js')
    <script src="{{ asset('assets\libs\select2\select2.min.js') }}"></script>
    <script src="{{ asset('assets\js\pages\form-advanced.init.js') }}"></script>
    <script type="text/javascript">
    function onFileSelected(event) {
        var selectedFile = event.target.files[0];
        var reader = new FileReader();

        var imgtag = document.getElementById("myimage");
        imgtag.title = selectedFile.name;

        reader.onload = function(event) {
            imgtag.src = event.target.result;
        };

        reader.readAsDataURL(selectedFile);
    }
    </script>
@endpush