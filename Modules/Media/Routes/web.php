<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::group(['prefix' => '/media'], function () {
        Route::get('/', 'MediaController@getIndex')->name('media');
        Route::post('/', 'MediaController@postMedia');
        Route::post('lazyload', 'MediaController@getMediaLazy');
        Route::post('media-filter', 'MediaController@getMediaFilter');
        Route::post('get-media', 'MediaController@getMediaAlone');
        Route::post('save-media', 'MediaController@postSaveMedia');
        Route::post('delete-media', 'MediaController@postDeleteMedia');
        Route::post('delete-multi-media', 'MediaController@postDeleteMultiMedia');

        Route::group(['prefix' => 'folder', 'middleware' => 'media_manager'], function() {
            Route::get('/', 'FolderController@index')->name('admin.media.folder');

            Route::group(['prefix' => 'add'], function() {
                Route::get('/', 'FolderController@getFolderAdd')->name('admin.media.folder.add');
                Route::post('/', 'FolderController@postFolderAdd');
            });
            
            Route::group(['prefix' => 'edit'], function() {
                Route::get('/{id}', 'FolderController@getFolderEdit')->name('admin.media.folder.edit');
                Route::post('/{id}', 'FolderController@postFolderEdit');
            });
            
            Route::get('/delete/{id}', 'FolderController@getFolderDelete')->name('admin.media.folder.delete');
            Route::post('/filter', 'FolderController@postFilter')->name('admin.media.folder.filter')->withoutMiddleware(['media_manager']);;
        });

        Route::group(['prefix' => 'setting'], function() {
            Route::get('/', 'MediaController@getSetting')->name('admin.media.settings');
            Route::post('/', 'MediaController@postSetting');
        });
    });

    Route::get('editor', 'MediaController@getEditor');
});