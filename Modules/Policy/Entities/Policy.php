<?php

namespace Modules\Policy\Entities;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $table = 'policy';
    protected $fillable = [];
}
