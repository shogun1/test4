const mix = require('laravel-mix');
const glob = require('glob');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
    fileLoaderDirs: {
        images: 'assets/images',
        fonts: 'assets/fonts'
    }
});

// Run all webpack.mix.js in Modules
//glob.sync('./Modules/**/webpack.mix.js').forEach(item => require(item));

require( './Modules/Project/webpack.mix.js' );
require( './Modules/Property/webpack.mix.js' );
require( './Modules/Theme/webpack.mix.js' );
require( './Modules/Location/webpack.mix.js' );
