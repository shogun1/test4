<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Modules\Permissions\Entities\Permissions;
use Modules\Permissions\Entities\Roles;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('modules', function ($user, $module) {
            if($user->status == 1) {
                if(!is_null($user->role)) {
                    $role = Roles::where('id', $user->role)->first();
                    if($role) {
                        if($role->slug == 'super-admin') {
                            return true;
                        }

                        $modules = Permissions::where('role_id', $role->id)->pluck('module')->toArray();
                        if($modules && in_array($module, $modules)) {
                            return true;
                        }
                    }
                }
            }
            abort(404);
        });
    }
}
