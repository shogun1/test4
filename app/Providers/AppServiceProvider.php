<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Category;
use Modules\Permissions\Entities\Permissions;
use Modules\Permissions\Entities\Roles;
use App\sortPages;
use App\Posts;
use App\ImageFooter;
use Auth;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view)
        {
            if(\Request::is('admin/*')) {

                $admin_menu = config('admin_menu');
                $permissions = [];

                if(Auth::check()) {
                    $user = Auth::user();
                    $role = Roles::find($user->role);
                    if($role) {
                        if($role->slug != 'super-admin') {
                            $permissions = Permissions::where('role_id', $role->id)->pluck('module')->toArray();
                            $permissions[] = 'media_access';
                            if($user->is_media_manager == 1) {
                                $permissions[] = 'media_folder_access';
                            }
                        }
                    }
                }
                $settings = get_settings(['
                        seo_separator', 'title_website', 'site_email', 'site_phone', 'phone_branch_1', 'phone_branch_2', 'site_logo', 'favicon', 'is_website_notice',
                        'website_notice', 'tawk_to_id', 'google_analytics', 'maintenance_content', 'maintenance_expired', 'site_telegram', 'site_zalo',
                        'site_youtube', 'site_twitter', 'site_facebook', 'site_linkedin', 'site_description', 'site_address', 'site_keywords', 'site_default_thumbnail', 'address_zalo', 'address_message']);

                View::share( 'admin_menu', $admin_menu );
                View::share( 'role_sidebar', $role->slug );
                View::share( 'permissions_sidebar', $permissions );
                View::share( 'settings', $settings);
            }
        });
    }
}
