<?php
use Carbon\Carbon;

function post_status() {
    return [
        'publish' => '<span class="badge badge-success">Publish</span>',
        'draft' => '<span class="badge badge-secondary">Draft</span>',
        'pending' => '<span class="badge badge-danger">Pending Review</span>',
    ];
}

function contact_status() {
    return [
        '<span class="badge badge-warning">Chờ xử lý</span>',
        '<span class="badge badge-success">Hoàn thành</span>',
    ];
}

function user_status() {
    return [
        '<span class="badge badge-warning">'.__('Inactive').'<span>',
        '<span class="badge badge-primary">'.__('Active').'<span>',
        '<span class="badge badge-danger">'.__('Banned').'<span>',
    ];
}

function user_type_status() {
    return [
        'member' => '<span class="badge badge-secondary">'.__('Member').'<span>',
        'system' => '<span class="badge badge-primary">'.__('System').'<span>',
    ];
}

function twofa_status() {
    return [
        '<i class="fa fa-times text-danger"><i>',
        '<i class="fa fa-check text-success"><i>',
    ];
}

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
 
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
 }
