<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class SiteMapController extends Controller
{
    public function index() {
        $posts= Posts::latest()->get();

        return response()->view('sitemap.index', [
            'posts' => $posts,
        ])->header('Content-Type', 'text/xml');
    }
}
