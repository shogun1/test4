<?php
namespace App\Http\Controllers\OpenSource;
use Illuminate\Support\Facades\DB;
use App\User;
use Modules\Permissions\Entities\Roles;
use Modules\Permissions\Entities\Permissions;
use Storage;
use Str;
class OpenSource
{
	public static function convert_time($date_format) {
		$month = substr($date_format,4,3);
		if($month == 'Jan') $month = '01';
		else if($month == 'Feb') $month = '02';
		else if($month == 'Mar') $month = '03';
		else if($month == 'Apr') $month = '04';
		else if($month == 'May') $month = '05';
		else if($month == 'Jun') $month = '06';
		else if($month == 'Jul') $month = '07';
		else if($month == 'Aug') $month = '08';
		else if($month == 'Sep') $month = '09';
		else if($month == 'Oct') $month = '10';
		else if($month == 'Nov') $month = '11';
		else if($month == 'Dec') $month = '12';

		$date = substr($date_format,8,2);
		$year = substr($date_format,11,4);
		$hour = substr($date_format,15, 3);
		$minute = substr($date_format,19, 2);
		$seconds = substr($date_format,22, 2);
		$finaldt = $year.'-'.$month.'-'.$date.''.$hour.':'.$minute.':'.$seconds;
		return $finaldt;
	}

	public static function media($media_source, $thumb = false){
		$media = DB::table('media')->where('media_source', $media_source)->first();
		$data = $media->media_url;
		if(!is_null($media)){
			if($media->media_type == 'image'){
				if($thumb == true){
					$data = str_replace('_size_' . $media->media_width . 'x' . $media->media_height . '.' . $media->media_extension, '_thumb.' . $media->media_extension, $media->media_url);
				}else{
					$data = $media->media_url;
				}
			}
		}
		return $data;
	}

	public static function redirectAdmin($user_id) {
		$user = User::find($user_id);
		$role = Roles::where('id', $user->role)->where('is_delete', 0)->first();
		if($role) {
			if($role->slug == 'super-admin') {
				return config('admin_menu')[0]['url'];
			}
				$permissions = Permissions::where('role_id', $role->id)->pluck('module')->toArray();
	
				if(count($permissions) > 0) {
					$modules = config('admin_menu');
					foreach($modules as $key => $value) {
						if(in_array($value['name'], $permissions)) {
							return $value['url'];
						}
						
						if(isset($value['sub'])) {
							foreach($value['sub'] as $sub) {
								if(in_array($sub['name'], $permissions)) {
									return $sub['url'];
								}
							}
						}
					}
				}
		}
		
		return '/';
	}

	public static function usersPermissions($user_id) {
		$user = User::find($user_id);
		if($user) {
			if($user->permission == 'super-admin') {
				return User::where('is_delete', 0)->pluck('id')->toArray();
			}else {
				$role = Roles::where('slug', $user->permission)->where('is_delete', 0)->first();
				if($role) {
					$users = [];
					foreach(json_decode($role->sponsor_slug) as $key => $value) {
						$users[] = User::where('permission', $value)->where('is_delete', 0)->pluck('id')->toArray();
					}
					$users[] = $user->id;
					dd($users);
					$users = array_column($users);
					return $users;
				}
			}
		}
	}

	public static function images($images, $folder = 'public/avatar/') {
		$new_name_image = rand(1,999999) . '-' . $images->getClientOriginalName();
		$dir = $folder.date('Y-m-d');
		$path = Storage::disk('local')->putFileAs($dir, $images, $new_name_image);
		$image = Storage::disk('local')->url($path);
		return env('APP_URL').$image;
	}

	// fix 29/02/2020
	public static function validate_username($username) {
		// bỏ khoảng trống. Username ko được có khoản trống
		$username = str_replace(array(chr(32)), '', $username);

		// return
		return $username;
	}
	// fix 29/02/2020
    public static function get_settings($args) {
		$data = [];
		if(!is_null($args)){
			foreach($args as $value){
				$data[$value] = '';
			}
			$settings = DB::table('settings')->whereIn('setting_name', $args)->select('setting_name', 'setting_value')->get();
			if(!is_null($settings)){
				foreach($settings as $setting){
					$data[$setting->setting_name] = $setting->setting_value;
				}
			}
		}
		return $data;
	}

	public static function time_to_date($time, $type = ""){
        $strreturn = "";
        switch($type) {
            case "H:i, d/m/Y":
            case "d/m/Y":
                $strreturn = date($type,$time);
                break;
            case "abbr":
                $strreturn = '<abbr class="DateTime" data-time="'.$time.'" data-diff="'.(time()-$time).'" data-datestring="'.date("d/m/Y",$time).'" data-timestring="'.date("H:i",$time).'"></abbr>';
                break;
            default:
                if (($remain=time()-$time)>= 86400 * 30)
                    $strreturn = date("H:i, d-m-Y",$time);
                else
                {
                    if($remain >= 86400)
                        $strreturn = intval($remain/86400)." ngày trước";
                    elseif ($remain>=3600)
                        $strreturn = intval($remain/3600)." giờ trước";
                    elseif ($remain>=60)
                        $strreturn = intval($remain/60)." phút trước";
                    else
                        $strreturn = $remain." giây trước";
                }
                break;
        }
        return $strreturn;
	}


	public static function checkSlug_exists($table, $title, $id = 0) {
        $slug =	Str::slug(trim($title));
        $index = 2;
        $new_slug = $slug;
        $check = DB::table($table)->where('slug', $new_slug);
        if($id > 0) {
            $check = $check->where('id', '<>', $id);
        }
        $check = $check->exists();
        while($check){
            $new_slug = $slug . '-' . $index;
            $index++;
            $check = DB::table($table)->where('slug', $new_slug);
            if($id > 0) {
                $check = $check->where('id', '<>', $id);
            }
            $check = $check->exists();
        }
        return $new_slug;
	}

	public static function _substr($str, $length, $minword = 3){
		$sub = '';
		$len = 0;
		foreach (explode(' ', $str) as $word){
			$part = (($sub != '') ? ' ' : '') . $word;
			$sub .= $part;
			$len += strlen($part);
			if (strlen($word) > $minword && strlen($sub) >= $length){
				break;
			}
		}
		return $sub . (($len < strlen($str)) ? ' ...' : '');
	}

	public static function estimate($userid) {
		$user = User::find($userid);
		$currencies = Currencies::where('actived', 1)->get();
		$balance = $user->UserBalance();
		$estimate = 0;
		foreach($currencies as $key => $value) {
			$good_price = DB::table('offers')->where('action', 'SELL')->where('symbol', $value->symbol)->orderBy('price', 'asc')->where('status', 0)->value('price_has_fee');
			$estimate += $balance->{$value->symbol} * $good_price;
		}
		return number_format($estimate + $balance->VND);
	}
}