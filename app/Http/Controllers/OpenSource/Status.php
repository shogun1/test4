<?php
namespace App\Http\Controllers\OpenSource;

trait Status {

	public static function user_status() {
		return [
			'<span class="badge badge-warning">'.__('Inactive').'<span>',
			'<span class="badge badge-success">'.__('Actived').'<span>',
			'<span class="badge badge-danger">'.__('Banned').'<span>',
		];
    }

    public static function PMO_status() {
        return [
            '<span style="font-size: 10px;" class="badge badge-success">'.__('Actived').'<span>',
			'<span style="font-size: 10px;" class="badge badge-warning">'.__('Inactive').'<span>',
		];
    }

    public static function twofa_status() {
		return [
            '<i class="fa fa-times text-danger"><i>',
            '<i class="fa fa-check text-success"><i>',
		];
    }

	public static function appointments_status() {
		return [
			'<span class="badge-pill badge-warning">After Appointment</span>',
			'<span class="badge-pill badge-success">Befor Appointment</span>',
		];
    }

    public static function HRM_status() {
		return [
			'<span class="badge-pill badge-danger">'.__('Banned').'</span>',
			'<span class="badge-pill badge-success">'.__('Actived').'</span>',
		];
	}

	public static function contact_status() {
		return [
			'<span class="badge-pill badge-warning">Đang chờ</span>',
			'<span class="badge-pill badge-secondary">Đã xem</span>',
			'<span class="badge-pill badge-success">Đã phản hồi</span>',
			'<span class="badge-pill badge-danger">Chưa phản hồi</span>',
		];
	}

	public static function commission_level_status() {
		return [
			'<span class="badge badge-warning">'.__('Inactive').'<span>',
			'<span class="badge badge-success">'.__('Actived').'<span>',
		];
	}

	public static function commission_sale_status() {
		return [
			'<span class="badge badge-warning">'.__('Inactive').'<span>',
			'<span class="badge badge-success">'.__('Actived').'<span>',
		];
	}

	public static function deposit_status() {
		return [
			'<span class="badge badge-warning">'.__('Pending').'<span>',
			'<span class="badge badge-success">'.__('Completed').'<span>',
			'<span class="badge badge-danger">'.__('Cancelled').'<span>',
		];
	}

	public static function transfers_status() {
		return [
			'<span class="badge badge-warning">'.__('Pending').'<span>',
			'<span class="badge badge-success">'.__('Completed').'<span>',
			'<span class="badge badge-danger">'.__('Cancelled').'<span>',
		];
	}

	public static function withdraw_status() {
		return [
			'<span class="badge badge-warning">'.__('Pending').'<span>',
			'<span class="badge badge-success">'.__('Completed').'<span>',
			'<span class="badge badge-danger">'.__('Cancelled').'<span>',
		];
	}

	public static function admincp_verify_status() {
		return [
			'',
			'<span class="badge badge-warning">'.__('Pending').'<span>',
			'<span class="badge badge-success">'.__('Completed').'<span>',
			'<span class="badge badge-danger">'.__('Từ chối').'<span>',
		];
	}

	public static function airdrop_status() {
		return [
			'<span class="badge badge-warning">'.__('Pending').'<span>',
			'<span class="badge badge-success">'.__('Completed').'<span>',
			'<span class="badge badge-danger">'.__('Error').'<span>',
		];
	}

	public static function action_type() {
		return [
			'DEPOSIT' => 'Nạp',
			'BUY' => 'Mua',
			'SELL' => 'Bán',
			'WITHDRAW' => 'Rút'
		];
	}

	public static function order_status() {
		return [
			'<span class="badge badge-warning">'.__('Chờ xử lý').'<span>',
			'<span class="badge badge-success">'.__('Hoàn thành').'<span>',
			'<span class="badge badge-danger">'.__('Đã hủy').'<span>',
		];
	}

	public static function robot_status() {
		return [
			'<span class="badge badge-danger">Stopped</span>',
			'<span class="badge badge-success">Running</span>',
		];
	}
	public static function admin_order_status() {
		return [
			'<span class="badge badge-warning">Pending...</span>',
			'<span class="badge badge-primary">Processing...</span>',
			'<span class="badge badge-success">Completed</span>',
			'<span class="badge badge-danger">Cancelled</span>',
			'<span class="badge badge-danger">Error</span>'
		];
	}

	public static function account_order_status() {
		return [
			'<span class="badge badge-warning">Chưa thanh toán</span>',
			'<span class="badge badge-primary">Đã thanh toán</span>',
			'<span class="badge badge-success">Completed</span>',
			'<span class="badge badge-danger">Cancelled</span>',
		];
	}

	public static function post_status() {
		return [
			'publish' => '<span class="badge badge-success">Publish</span>',
			'draft' => '<span class="badge badge-secondary">Draft</span>',
			'pending' => '<span class="badge badge-danger">Pending Review</span>',
		];
	}

	public static function commission_status() {
		return [
			'<span class="badge badge-warning">Pending</span>',
			'<span class="badge badge-success">Paid</span>',
		];
	}

	public static function ticket_status() {
		return [
			'<span class="badge badge-success">OPEN</span>',
			'<span class="badge badge-warning">WAITING</span>',
			'<span class="badge badge-primary">RESPONSE</span>',
			'<span class="badge badge-danger">CLOSE</span>',
		];
	}

	public static function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	 
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	 }
}
