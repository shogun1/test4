<?php
namespace App\Http\Controllers\OpenSource;

use App\Image;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
//File
use Illuminate\Support\Facades\File;
//Rise Image
use \Intervention\Image\ImageManagerStatic;

class UploadFileService
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function savePathAFile($request, $folder = 'document_uploads')
    {
        if ($request->hasFile('file')) {
            $images = $request->file('file');
            $fileName = mt_rand(100000, 999999).'_'.$request->file->getClientOriginalName();
            $filePath = $images->storeAs('uploads/'.$folder, $fileName, 'public');

            return [
                'name' => $fileName,
                'path' => $filePath,
                'url' => url('/storage/'.$filePath)
            ];
        }

        return false;
    }

    /**
     * Upload file have thumbnail
     *
     * @var Request $request
     * @var $folder
     *
     * @return mixed
     */
    public function uploadImageThumb($file, $folder = 'document_uploads')
    {
        if (!empty($file)) {
            $strTime = time().mt_rand(1, 1000);
            $fileType = $file->getClientOriginalExtension();
            $fileName = $strTime.'.'.$fileType;
            $destinationPath = public_path($folder);// Example for $folder: 'upload/Project'
            $file->move($destinationPath, $fileName);

            if (!in_array($fileType, ['mp4','ogx','oga','ogv','ogg','webm'])) {
                $fileNameThumbnail = $strTime.'_thumb.'.$file->getClientOriginalExtension();
                $image_resize = ImageManagerStatic::make($destinationPath.'/'.$fileName);
                $image_resize->resize(150, 150);
                $image_resize->save($destinationPath.'/'.$fileNameThumbnail);

                return [
                    'main' => url($folder.'/'.$fileName),
                    'thumbnail' => url($folder.'/'.$fileNameThumbnail),
                ];

            }

            return [
                'main' => url($folder.'/'.$fileName),
                'thumbnail' => url($folder.'/'.$fileName),
            ];
        }

        return false;
    }

    /**
     * Upload file have thumbnail
     *
     * @var Request $request
     * @var $folder
     *
     * @return mixed
     */
    public function uploadImage($file, $folder = 'document_uploads')
    {
        if (!empty($file)) {
            $strTime = time().mt_rand(1, 1000);
            $fileType = $file->getClientOriginalExtension();
            $fileName = $strTime.'.'.$fileType;
            $destinationPath = public_path($folder);// Example for $folder: 'upload/Project'
            $file->move($destinationPath, $fileName);

            return url($folder.'/'.$fileName);
        }

        return null;
    }

    /**
     * Delete file
     *
     * @var Request $request
     *
     * @return
     */
    public function deleteFile($imageUrl)
    {
        $domain = $this->request->root();
        $filePath = str_replace($domain.'/','',$imageUrl);
        if (File::exists($filePath)) File::delete($filePath);
    }

    /**
     * Upload Image and store to images table
     *
     * @var Request $request
     *
     * @return
     */
    public function storeImage($request, $folder)
    {

        $fileArr = $request->file('file');
        if (!empty($fileArr)) {
            $j = 0;
            $resultArr = array();
            foreach ($fileArr as $file) {

                //Upload Image
                $imagePathArr = $this->uploadImageThumb($file, $folder);

                //Save Database
                $image = new Image();
                $image['main'] = $imagePathArr['main'];
                $image['thumbnail'] = $imagePathArr['thumbnail'];
                $image->save();
                $resultArr[$j] = $image;
                $j++;
            }

            return $this->success( "Create data success", $resultArr,  201);
        }

        return $this->error( "File not found",  404);
    }

    /**
     * Delete 1 Image
     *
     * @var $id
     *
     * @return
     */
    public function deleteImage($id)
    {

        $image = Image::find($id);
        if (!empty($image)) {
            $this->deleteFile($image['main']);
            $this->deleteFile($image['thumbnail']);
            $image->delete();

            return $this->success("The data deleted", $image);
        }

        return $this->error( "File not found",  404);
    }
}
