<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialUser extends Model
{
    protected $table = "social_users";
    protected $fillable = ['userid', 'site_name', 'site_link'];
}
