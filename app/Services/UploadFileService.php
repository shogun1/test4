<?php
namespace App\Services;

use App\Models\Image;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Http\Request;
//File
use Illuminate\Support\Facades\File;
//Rise Image
use \Intervention\Image\ImageManagerStatic;
use Storage;
use Modules\Media\Entities\SettingMedia;

class UploadFileService
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function savePathAFile($folder = 'document_uploads', $request, $fileName)
    {
        if ($request->hasFile('file')) {
            $images = $request->file('file');
            $fileName = mt_rand(100000, 999999).'_'.$request->file->getClientOriginalName();
            $filePath = $images->storeAs('uploads/'.$folder, $fileName, 'public');

            return [
                'name' => $fileName,
                'path' => $filePath,
                'url' => url('/storage/'.$filePath)
            ];
        }

        return false;
    }

    /**
     * Upload file have thumbnail
     *
     * @var Request $request
     * @var $folder
     *
     * @return mixed
     */
    public function uploadImageThumb($folder = 'document_uploads', $file, $fileName)
    {
        if (!empty($file)) {
            $fileType = $file->getClientOriginalExtension();
            $destinationPath = public_path($folder);// Example for $folder: 'upload/Project'
            $file->move($destinationPath, $fileName);

            if (!in_array($fileType, ['mp4','ogx','oga','ogv','ogg','webm'])) {
                $settings_media = SettingMedia::pluck('setting_value', 'setting_name')->toArray();
                $domain = env('APP_URL').'/';

                $fileNameFullSize = strstr($fileName, '_size_', true);
                $full_size = ImageManagerStatic::make($destinationPath.'/'.$fileName);
                $full_size->save($destinationPath.'/'.$fileNameFullSize); 

                $fileNameLargeSize = 'large_'.$fileName;
                $watermark = ImageManagerStatic::make(str_replace($domain, '', $settings_media['media_auto_watermark']))->resize(50, 50);
                $large_size = ImageManagerStatic::make($destinationPath.'/'.$fileName)->resize($settings_media['media_large_auto_resize_width'], $settings_media['media_large_auto_resize_height']);
                $large_size->insert($watermark, 'bottom-right', 10, 10);
                $watermark = $large_size->save($destinationPath.'/'.$fileNameLargeSize);

                $fileNameThumbnailSize = 'thumbnail_'.$fileName;
                $thumbnail_size = ImageManagerStatic::make($destinationPath.'/'.$fileName)->resize($settings_media['media_thumbnail_auto_resize_width'], $settings_media['media_thumbnail_auto_resize_height']);
                $thumbnail_size->save($destinationPath.'/'.$fileNameThumbnailSize); 

                return [
                    'main' => url($folder.'/'.$fileNameFullSize),
                    'large' => url($folder.'/'.$fileNameLargeSize),
                    'thumbnail' => url($folder.'/'.$fileNameThumbnailSize),
                ];

            }

            return [
                'main' => url($folder.'/'.$fileName),
                'thumbnail' => url($folder.'/'.$fileName),
            ];
        }

        return false;
    }

    /**
     * Upload file have thumbnail
     *
     * @var Request $request
     * @var $folder
     *
     * @return mixed
     */
    public function uploadImage($file, $folder = 'document_uploads')
    {
        if (!empty($file)) {
            $strTime = time().mt_rand(1, 1000);
            $fileType = $file->getClientOriginalExtension();
            $fileName = $strTime.'.'.$fileType;
            $destinationPath = public_path($folder);// Example for $folder: 'upload/Project'
            $file->move($destinationPath, $fileName);

            return url($folder.'/'.$fileName);
        }

        return null;
    }

    /**
     * Delete file
     *
     * @var Request $request
     *
     * @return
     */
    public function deleteFile($imageUrl)
    {
        $domain = $this->request->root();
        $filePath = str_replace($domain.'/','',$imageUrl);
        if (File::exists($filePath)) File::delete($filePath);
    }

    /**
     * Upload Image and store to images table
     *
     * @var Request $request
     *
     * @return
     */
    public function storeImage($request, $folder)
    {

        $fileArr = $request->file('file');
        if (!empty($fileArr)) {
            $j = 0;
            $resultArr = array();
            foreach ($fileArr as $file) {

                //Upload Image
                $imagePathArr = $this->uploadImageThumb($file, $folder);

                //Save Database
                $image = new Image();
                $image['main'] = $imagePathArr['main'];
                $image['thumbnail'] = $imagePathArr['thumbnail'];
                $image->save();
                $resultArr[$j] = $image;
                $j++;
            }

            return "Create data success";
        }

        return "File not found";
    }

    /**
     * Delete 1 Image
     *
     * @var $id
     *
     * @return
     */
    public function deleteImage($id)
    {

        $image = Image::find($id);
        if (!empty($image)) {
            $this->deleteFile($image['main']);
            $this->deleteFile($image['thumbnail']);
            $image->delete();

            return "The data deleted";
        }

        return "File not found";
    }
}
