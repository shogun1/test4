<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Permissions\Entities\Roles;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'image', 'email', 'birthday', 'phone', 'address', 'gender', 'status', 'role', 'type', 'permission', 'policy_id', 'email_verified_at', 'password', 'is_media_manager', 'is_delete', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function Post() {
        return $this->hasMany('App\Posts', 'post_author', 'id'); 
    }

    public function Role() {
        return $role = Roles::where('id', $this->role)->first();
    }
}
