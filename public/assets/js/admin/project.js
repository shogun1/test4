// projectSection AbstractClass
let projectSection = {
    process: function() {
        this.initSections();
        this.toggleSection();
        this.toggleColumnRatio();
        this.toggleAdvancedSetting();
        this.renderSectionColumn();
        this.changeSectionColumn();
        this.toggleColumnContent();
        this.passButtonDataToElementModal();
        this.addElement();
        this.removeElement();
        return true;
    }
};

function inherit(proto) {
    let F = function() { };
    F.prototype = proto;
    return new F();
}

// Log Helper
let log = (function() {
    let log = "";
    return {
        add: function(msg) { log += msg + "\n"; },
        show: function() {
            if( log !== '' ) console.log(log);
            log = "";
        }
    }
})();

// Main Functions
(function($) {

    'use strict';

    let ProjectSections = $( '#project-sections' );
    let ElementModuleModal = $( '#element_module' );
    let JSTemplate = $( '.js-template' );

    let section = inherit( projectSection );

    section.initSections = function() {
        if( ProjectSections.length > 0 ) {
            let projectSection = $( '> .project-section', JSTemplate ).clone();
            projectSection.attr( 'id', 'project-section-0' );
            projectSection.attr( 'data-id', '0' );

            ProjectSections.empty().append( projectSection );
        }
    };

    section.toggleSection = function() {
        if( $( '.toggle-section' ).length > 0 ) {
            $( document ).on( 'click', '.toggle-section .toggle-button', function(){
                let thisButton = $( this );
                let buttonIcon = $( 'i', thisButton );
                let parentSection = thisButton.closest( '.toggle-section' );

                $( '.section-content', parentSection ).slideToggle();

                if( parentSection.hasClass( 'toggle-open' ) ) {
                    parentSection.removeClass( 'toggle-open' ).addClass( 'toggle-close' );
                    buttonIcon.attr( 'class', thisButton.data( 'toggle-close-icon' ) );
                } else {
                    parentSection.removeClass( 'toggle-close' ).addClass( 'toggle-open' );
                    buttonIcon.attr( 'class', thisButton.data( 'toggle-open-icon' ) );
                }
            });
        }
    };

    section.toggleColumnRatio = function() {
        if( $( 'select[data-name="section-columns-number"]' ).length > 0 ) {
            $( document ).on( 'change', 'select[data-name="section-columns-number"]', function() {
                let columnNumber = $( this ).val();
                $( '.setting-section-ratio select.form-select.hide' ).removeClass( 'active' ).hide();
                if( columnNumber > 1 ) {
                    $( '.setting-section-ratio select.form-select.hide[data-column="' + columnNumber + '"]' ).addClass( 'active' ).show();
                    $( '.setting-section-ratio' ).show();
                } else {
                    $( '.setting-section-ratio' ).hide();
                }
            });
        }
    };

    section.toggleAdvancedSetting = function() {
        if( $( '.toggle-advanced-setting' ).length > 0 ) {
            $( document ).on( 'click', '.toggle-advanced-setting', function(){
                $( '.section-advanced-setting' ).slideToggle();
            });
        }
    };

    section.renderSectionColumn = function( columnNumber = null, columnRatio = null ) {
        if( columnNumber ) {
            let tabs_html = '';

            // Hide all content
            $( '.section-column-container .column-content > .item.available' ).removeClass( 'active available' );

            if( columnNumber > 1 ) {
                let tabActive = '';
                let columnRatioArr = columnRatio.split('-');
                $.each( columnRatioArr, function( index, value ){

                    let columnIndex = index + 1;

                    // Active the 1st tab
                    if( index === 0 ) {
                        tabActive = ' active';
                    } else {
                        tabActive = '';
                    }

                    // Create tab HTML
                    tabs_html += '<div class="item px-0 col-12 col-lg-' + value + tabActive + '" data-column="' + columnIndex + '"><span class="column-text">Column ' + columnIndex + '</span></div>';

                    // Show the content
                    $( '.section-column-container .column-content > .item[data-column="' + columnIndex + '"]' ).addClass( 'available' + tabActive );
                });
            } else {
                // Create tab HTML
                tabs_html = '<div class="item px-0 col-12 col-lg-12 active" data-column="1"><span class="column-text">Column 1</span></div>';

                // Show the content
                $( '.section-column-container .column-content > .item[data-column="1"]' ).addClass( 'available active' );
            }

            $( '.section-column-container .column-tab > .row' ).empty().append( tabs_html );
        }
    };

    section.changeSectionColumn = function() {
        $( document ).on( 'change', 'select[data-name="section-columns-number"], select[data-name="section-columns-ratio"]', function() {
            let columnNumber = $( 'select[data-name="section-columns-number"]' ).val();
            let columnRatio = $( 'select[data-name="section-columns-ratio"].active' ).val();
            section.renderSectionColumn( columnNumber, columnRatio );
        });
    };

    section.toggleColumnContent = function() {
        if( $( '.section-column-container .column-tab .item' ).length > 0 ) {
            $( document ).on( 'click', '.section-column-container .column-tab .item', function(){

                $( '.section-column-container .column-tab .item.active' ).removeClass( 'active' );
                $( '.section-column-container .column-content .item.active' ).removeClass( 'active' );

                let columnNumber = $( this ).data( 'column' );
                $( '.section-column-container .column-content .item[data-column="' + columnNumber + '"]' ).addClass( 'active' );

                $( this ).addClass( 'active' );
            });
        }
    };

    section.passButtonDataToElementModal = function() {
        $( '#element_module' ).on( 'show.bs.modal', function (event) {
            let button = $( event.relatedTarget );
            let thisModalElementItem = $( '.element-group .item > .wrap', this );

            let parentColumnContent = button.closest( '.item.available.active' ).data( 'column' );
            thisModalElementItem.attr( 'data-column', parentColumnContent );

            let parentElementBlock = button.closest( '.element-block' );
            if( parentElementBlock.length > 0 ) {
                let parentElementBlockIndex = parentElementBlock.data( 'index' );
                thisModalElementItem.attr( 'data-element-block', parentElementBlockIndex );
            } else {
                thisModalElementItem.removeAttr( 'data-element-block' );
            }
        })
    };

    section.countingElementBlockNumberOrder = function( content ) {
        $( '.element-block', content ).each( function( index, element ) {
            $( this ).attr( 'data-index', index );
        });
    };

    section.addElement = function() {
        $( document ).on( 'click', '#element_module .element-group .item > .wrap', function(){
            let column = $( this ).attr( 'data-column' );
            let elementBlockIndex = $( this ).attr( 'data-element-block' );
            let content = $( '.column-content .item.available.active[data-column="' + column + '"]' );
            let elementType = $( this ).data( 'element' );
            switch( elementType ) {
                case 'editor':
                    section.renderEditor( content, elementBlockIndex ); break;
                case 'slider':
                    section.renderSlider( content, elementBlockIndex ); break;
                case 'tab':
                    section.renderTab( content, elementBlockIndex ); break;
                case 'icon-list':
                    section.renderIconList( content, elementBlockIndex ); break;
                case 'button':
                    section.renderButton( content, elementBlockIndex ); break;
            }
            $( '#element_module' ).modal( 'hide' );

            section.countingElementBlockNumberOrder( content );
        });
    };

    section.removeElement = function() {

    };

    section.renderElementBlock = function( elementHTML ) {
        let openBlock = '<div class="element-block">';
        let actionButtons = $( '.js-template .element-block-action-buttons' ).clone().prop( 'outerHTML' );
        let closeBlock = '</div>';
        return openBlock + elementHTML + actionButtons + closeBlock;
    };

    section.renderEditor = function( content, elementBlockIndex = null ) {
        let columnNumber = content.data( 'column' );
        let totalEditor = $( '.editor-tinymce', content ).length;
        let thisEditorID = 'tinymce_content_column_' + columnNumber + '_' + (totalEditor + 1);
        let editorHTML = '<textarea name="' + thisEditorID + '" id="' + thisEditorID + '" class="editor-tinymce" rows="3"></textarea>';

        let editorExtraButtons = $( '.js-template .editor-extra-buttons' ).clone();
        $( '.media-button', editorExtraButtons ).attr( 'data-result', thisEditorID );

        let newElementBlock = section.renderElementBlock( editorExtraButtons.prop( 'outerHTML' ) + editorHTML );

        if( elementBlockIndex != null ) {
            $( '.element-block[data-index="' + elementBlockIndex + '"]', content ).after( newElementBlock );
        } else {
            $( '> .wrap', content ).append( newElementBlock );
        }

        if( typeof( window.tinymceOptions ) !== 'undefined' ) {
            let newOptions = $.extend( true, window.tinymceOptions, { selector: '#' + thisEditorID } );
            tinymce.init( newOptions );
        } else {
            log.add( 'Global TinyMCE Option is undefined.' );
        }
    };

    section.renderSlider = function( content, elementBlockIndex = null ) {
        let newElementBlock = section.renderElementBlock( '<p>Slider</p>' );

        if( elementBlockIndex != null ) {
            $( '.element-block[data-index="' + elementBlockIndex + '"]', content ).after( newElementBlock );
        } else {
            $( '> .wrap', content ).append( newElementBlock );
        }
    };

    section.renderTab = function( content, elementBlockIndex = null ) {
        let newElementBlock = section.renderElementBlock( '<p>Tab</p>' );

        if( elementBlockIndex != null ) {
            $( '.element-block[data-index="' + elementBlockIndex + '"]', content ).after( newElementBlock );
        } else {
            $( '> .wrap', content ).append( newElementBlock );
        }
    };

    section.renderIconList = function( content, elementBlockIndex = null ) {
        let newElementBlock = section.renderElementBlock( '<p>Icon List</p>' );

        if( elementBlockIndex != null ) {
            $( '.element-block[data-index="' + elementBlockIndex + '"]', content ).after( newElementBlock );
        } else {
            $( '> .wrap', content ).append( newElementBlock );
        }
    };

    section.renderButton = function( content, elementBlockIndex = null ) {
        let newElementBlock = section.renderElementBlock( '<p>Button</p>' );

        if( elementBlockIndex != null ) {
            $( '.element-block[data-index="' + elementBlockIndex + '"]', content ).after( newElementBlock );
        } else {
            $( '> .wrap', content ).append( newElementBlock );
        }
    };

    section.process();

    log.show();

}(jQuery));

let ProjectPost = {};

ProjectPost.savePost = function() {

    let thisForm = $( 'form#project-content-form' );

    if( thisForm.length > 0 ) {

        let formCSRF = $( '[name="_token"]', thisForm ).val();
        let ajaxUrl = thisForm.attr( 'action' );
        let ajaxMethod = thisForm.attr( 'method' );

        /** Form data */
        let formData = {};

        formData.title = $( '#post-title', thisForm ).val();
        formData.slug = $( '#post-slug', thisForm ).val();

        if( thisForm[0].checkValidity() ) {

            /** Setup CSRF Token in header */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': formCSRF
                }
            });

            $.ajax({
                url: ajaxUrl,
                type: ajaxMethod,
                dataType: 'json',
                data: formData,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( '### TASK: ProjectPost.savePost()' );
                    console.log( 'textStatus: ' + textStatus );
                    console.log( 'errorThrown: ' + errorThrown );
                    console.log( jqXHR.getAllResponseHeaders() );
                },
                success: function( response ) {
                    console.log( response );
                }
            });
        } else {
            $( '.check-valid', thisForm ).addClass( 'was-validated' );
        }
    }


};

$( document ).on( 'click', 'button#save-post', function() {
    ProjectPost.savePost();
});
