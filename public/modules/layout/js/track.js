// Add prototype inView to Jquery
$.fn.inView = function() {
    if (!this.length) return false;
    var rect = this.get(0).getBoundingClientRect();

    if (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <=
            (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <=
            (window.innerWidth || document.documentElement.clientWidth)
    ) {
        return this.get(0);
    } else {
        return false;
    }
};

// Defined track obj
var TrackData = TrackData || {};
// Init default value
TrackData.debug = false;
TrackData.data = {};
TrackData.data.client = {};
TrackData.data.data = {};
TrackData.isAdmin = false;
TrackData.pageViewInterval;
TrackData.dataName = 'track';
TrackData.action = [];
TrackData.Scroll = {"25":false, "50":false, "75":false};
TrackData.itemTrack = ['.track-item'];
TrackData.selectTextEventFired = false;
TrackData.LastSelectedText = '';
TrackData.stopFlag = false;
// Init track obj
TrackData.init =  function(){
    var start = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    if(TrackData.getDataStorage() == null){
        TrackData.initPage();
        TrackData.initClient();
    } else{
        TrackData.data = TrackData.getDataStorage();
        TrackData.initPage();
    }
    if(start){
        TrackData.startTrack('liveTime');
    }
}

// Init track obj
TrackData.initClient =  async function(){
    $.ajax({
        type: "GET",
        url: window.location.origin + "/track/init",
        success: function(result) {
            TrackData.log('Init Client Data:');
            TrackData.log(result);
            TrackData.data.client = result;
            TrackData.setDataStorage(TrackData.data);
        }
    });
}

// Get data track in cookie
TrackData.getDataStorage = function() {
    var data = localStorage.getItem(TrackData.dataName);

    if(data != null){
        TrackData.log('Data:');
        TrackData.log(JSON.parse(data));
        return JSON.parse(data);
    }
    TrackData.log('Data: Not found');
    return null;
}

/**
 * Function Set Cookie Track
 * @param Object data
 * @param Number exdays
 */
TrackData.setDataStorage = function (data) {
    value = JSON.stringify(data);
    localStorage.setItem(TrackData.dataName,value);
    return true;
}

/**
 * Function TrackData log
 */
TrackData.log = function(str){
    if(TrackData.debug){
        console.log(str)
    }
}

/**
 * Function init page infor
 */
TrackData.initPage = function(){
    if (
        TrackData.data.data[window.location.href] == undefined
    ) {
        TrackData.data.data[window.location.href] = {
            keyword: $("meta[name='keywords']").attr("content"),
            description:$("meta[name='description']").attr("content"),
            total_time: 1,
            first_visit: Date('Y-m-d H:i:s'),
            last_visit: Date('Y-m-d H:i:s'),
            live_time: 0,
            action: {
                view: [],
                click: []
            }
        };
    } else {
        TrackData.data.data[window.location.href].total_time++;
        TrackData.data.data[window.location.href].last_visit = Date();
    }
    TrackData.setDataStorage(TrackData.data);
}

/**
 * Function track page live time
 */
TrackData.liveTime = function(){
    TrackData.pageViewInterval = setInterval(function() {
        if (TrackData.data.data[window.location.href] == undefined || 
            TrackData.data.data[window.location.href].live_time === undefined) {
            TrackData.data.data[window.location.href].live_time = 0;
        } else {
            TrackData.data.data[window.location.href].live_time += 0.5;
        }
    }, 500);
}

/**
 * Function start track
 * @param String actions
 */
TrackData.startTrack = function(actions){
    TrackData.stopFlag = false;
    if(actions == null){
        TrackData.liveTime();
        return;
    }

    var listTrack = actions.split(',');
    if(listTrack.length == 0){
        TrackData.liveTime();
    } else{
        listTrack.forEach(action => {
            TrackData[action]();
        });
    }

    TrackData.saveData();
}

/**
 * Function stop track
 */
TrackData.stopTrack = function(){
    TrackData.stopFlag = true;
    clearInterval(TrackData.pageViewInterval);
    TrackData.pageViewInterval = null;
}

/**
 * Function clear TrackData
 */
TrackData.clearData = function(){
    localStorage.removeItem(TrackData.dataName);
}

/**
 * Function destroy TrackData
 */
TrackData.destroy = function(){
    TrackData.stopTrack();
    TrackData.clearData();
}

/**
 * Funtion Add data action track
 * @param Object action
 * */ 
TrackData.addAction = function(action){
    if(TrackData.stopFlag == false){
        var actionData = {
            item: action.item,
            name: action.name,
            value: action.value
        };
        if(TrackData.data.data[window.location.href].action[action.name] == undefined){
            TrackData.data.data[window.location.href].action[action.name] = [];
        } else{
            TrackData.data.data[window.location.href].action[action.name].push(actionData);
        }
        TrackData.setDataStorage(TrackData.data);
    }
}

/**
 * Function track read percent of page
 */
TrackData.readStatus = function(){
    var scrollTop = $(document).height() - $(window).height();
    $( window ).scroll(function() {
        if( $(window).scrollTop() > 25 ){
            if( $(window).scrollTop() > 3*(scrollTop/4) && TrackData.Scroll['75'] == false ) {
                TrackData.Scroll['75'] = true;
                TrackData.addAction({item:"Window", name:"scroll", value:"75"});
            } else if( $(window).scrollTop() > 2*(scrollTop/4) && TrackData.Scroll['50'] == false ){
                TrackData.Scroll['50'] = true;
                TrackData.addAction({item:"Window", name:"scroll", value:"50"});
            } else if( $(window).scrollTop() > (scrollTop/4) && TrackData.Scroll['25'] == false ){
                TrackData.Scroll['25'] = true;
                TrackData.addAction({item:"Window", name:"scroll",value:"25"});
            }
        }
    });
}

/**
 * Function track reading custom content
 */
TrackData.readContent = function(){
    var viewInterval;
    var visitedFlag = false;
    var lastVisit;
    var stage = 0;
    $(window).on("scroll", function() {
        var listItem = TrackData.itemTrack;
        if(TrackData.itemTrack.length > 1){
            listItem = TrackData.itemTrack.join(',');
        }
        $(listItem).each(function(key,el){
            if ($(el).inView()) {
                TrackData.log('Track item active');
                visitedFlag = true;
                var start = new Date().getTime();
                lastVisit = $(el).inView();
                if (!viewInterval) {
                    viewInterval = setInterval(function() {
                        stage = Math.round((new Date().getTime() - start) / 1000);
                    }, 500);
                }
            } else {
                clearInterval(viewInterval);
                viewInterval = null;
                if (visitedFlag) {
                    if($(el).prop('track') == ''){
                        TrackData.addAction({item:"Content", name:"view",value:stage});
                    } else{
                        TrackData.addAction({item:$(el).prop('track'), name:"view",value:stage});
                    }
                    stage = 0;
                    visitedFlag = false;
                }
            }
        })
    });
}

/**
 * Function track select text
 */
TrackData.selectText = function(){
    TrackData.selectTextEvent(TrackData.selectTextCallBack);
}

/**
 * Function select text event handler
 */
TrackData.selectTextEvent = function(callback){
    $(document).on("mouseup touchend onselectstart onselectend onselectionchange", function(e) {
        //e.preventDefault();
        if( !TrackData.selectTextEventFired ){
            var text = TrackData.getSelectedText();
            if (text!='' && text != TrackData.LastSelectedText){
                TrackData.LastSelectedText = text;
                callback( text );
            }
            TrackData.SelectTextEventFired = true;
            setTimeout(function(){ TrackData.SelectTextEventFired = false; }, 50);
        }
    });
}

/**
 * Function get text selected
 */
TrackData.getSelectedText = function(){
    var ret = '';
    if (window.getSelection) {
        ret = window.getSelection().toString();
    } else if (document.selection) {
        ret = document.selection.createRange().text;
    }
    return ret;
}

/**
 * Function save data select text action
 */
TrackData.selectTextCallBack = function( text ){
    TrackData.addAction({item:'document', name:"selection",value:text});
}

/**
 * Function track method get/post
 */
TrackData.trackMethod = function(){
    $('form').on('submit',function(){
        var method = $(this).prop('method');
        var action = $(this).prop('action');
        TrackData.addAction({item:method, name:"method",value:action});
    });
}

/**
 * Function save data
 */
TrackData.saveData = function(){
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        url: window.location.origin + "/track",
        success: function(result) {
            console.log(result);
        },
        dataType: "json",
        data: {
            data: JSON.stringify(TrackData.data),
            is_admin: TrackData.isAdmin
        }
    });
}
