$(document).on("keyup", '[name="price"]', function () {
    var rate = $('.crypto_rate').val();
    var calculator = parseFloat($(this).val()) / rate;
    var amount = $('[name="amount"]').val(calculator.toFixed(8));
    $('#amount_to_sell').html(number_format(calculator, 8));
    $('#crypto_rate').html(number_format(rate, 8));
    $('#pay_total').html(number_format($(this).val(), 2));
    var fee = parseFloat($('[name="payment_method"]').find("option:selected").attr("data-fee"));
    var fee_min = parseFloat($('[name="payment_method"]').find("option:selected").attr("data-fee-min"));
    var fee_max = parseFloat($('[name="payment_method"]').find("option:selected").attr("data-fee-max"));
    if (fee_min != fee_max) { // nếu fee bằng %
        fee = parseFloat($(this).val()) * fee / 100;
        if (fee < fee_min) {
            fee = fee_min;
        }
        if (fee > fee_max) {
            fee = fee_max;
        }
    }
    var total_has_fee = parseFloat($(this).val()) - fee;
    $('#total_has_cost').html(number_format(total_has_fee, 2));
    $("#cost").html(number_format(fee, 2));
});