const chart_symb = [];
const chart_obj = [];
var chartjs;
const chart_option = {
    tooltips: {
        mode: null
    },
    legend: {
        display: false,
    },
    spanGaps: false,
    elements: {
        line: {
            tension: 0.000001
        }
    },
    plugins: {
        filler: {
            propagate: false
        }
    }
}

$(document).ready(() => {
    chartjs = document.querySelectorAll('[chart-render]');
    // console.log(chartjs)
    // return;
    if (chartjs.length) {
        for (let item of chartjs) {
            let target = getValueElm(item)
            // console.log(target)
            let obj = {};
            obj[target.symbol] = new Chart(target.id, {
                type: 'line',
                data: {
                    labels: [],
                    datasets: [{
                        fill: false,
                    }]
                }
            });
            chart_option['scales'] = {
                xAxes: [{
                    display: target.xAxes,
                }],
                yAxes: [{
                    display: target.yAxes,
                }]
            };
            for (let i = 0; i < target.resultNumber; i++){
                obj[target.symbol].data.labels.push(" ");
                obj[target.symbol].data.datasets[0].data.push(0);
            }
            obj[target.symbol].data.datasets[0].borderWidth = 1;
            obj[target.symbol].data.datasets[0].borderColor = target.color;
            obj[target.symbol].data.datasets[0].pointColor = target.pointColor;
            obj[target.symbol].data.datasets[0].pointHoverBorderColor = target.hoverPointColor;
            obj[target.symbol].options = chart_option;
            obj[target.symbol].update();
            obj['symbol'] = target.symbol;
            obj['resultNumber'] = target.resultNumber;
            obj['result'] = [];
            chart_obj.push(obj);
            const sk_url = 'wss://stream.binance.com:9443/ws/!ticker@arr';
            const conn = new WebSocket(sk_url);
            // console.log(conn)
            conn.addEventListener('open', (e) => {
            })
            conn.addEventListener('message', (e) => {
                let data = JSON.parse(e.data);
                for (let chart of chart_obj) {
                    data.map((target, index, data) => {
                        if ((target.s).toLowerCase() === chart.symbol) {
                            // console.log(target)
                            changeDataChart(chart, (target.s).toLowerCase(), target)
                            updateChartDOM(target)
                        }
                    })
                }
            })
        }
    }

});

function getValueElm(data) {
    return {
        id: data.dataset.target,
        color: data.dataset.lineColor || '#5a9216',
        pointColor: data.dataset.pointColor || '#5a9216',
        hoverPointColor: data.dataset.hoverPointColor || 'yellow',
        symbol: (data.attributes['chart-render'].value).toLowerCase(),
        xAxes: data.dataset.xline || false,
        yAxes: data.dataset.yline || false,
        resultNumber: data.dataset.resultNumber || 10
    }
}

function changeDataChart(chart, target, data) {
    // console.log(chart[target])
    // if (o.length === chart.resultNumber) {
    //     o.shift();
    // }
    // o.push(data.q);
    // chart[target].update()
    if (chart.result.length === chart.resultNumber) {
        chart[target].data.datasets[0].data = chart.result;
        chart.result = [];
        chart[target].update()
    } else {
        chart.result.push(data.q)
    }
    // console.log(chart.result.length)
}
function updateChartDOM(target) {
    // console.log(chartjs.length)
    // return;
    for (let id = 0; id < chartjs.length; id++) {
        // console.log([chartjs[id].attributes[''], target.s])
        // return;
        if (chartjs[id].attributes['chart-render'].value === target.s) {
            let domAttr = ['volumn', 'percent', 'percent-content', 'percent-icon', 'price'];
            for (let dom of domAttr) {
                let i = chartjs[id].querySelector(`[data-${dom}]`);
                if (i) {
                    if (dom === 'percent') {
                        if (i.classList.contains('text-danger')) i.classList.remove('text-danger');
                        if (i.classList.contains('text-success')) i.classList.remove('text-success');
                        if (parseFloat(target.p) > 0) i.classList.add('text-success');
                        if (parseFloat(target.p) < 0) i.classList.add('text-danger');
                    }
                    else if (dom === 'percent-icon') {
                        if (i.classList.contains('fa-long-arrow-up')) i.classList.remove('fa-long-arrow-up');
                        if (i.classList.contains('fa-long-arrow-down')) i.classList.remove('fa-long-arrow-down');
                        if (parseFloat(target.p) > 0) i.classList.add('fa-long-arrow-up');
                        if (parseFloat(target.p) < 0) i.classList.add('fa-long-arrow-down');
                    } else {
                        if (dom == 'volumn') i.innerText = parseFloat(target.v).toFixed(0);
                        if (dom == 'percent-content') i.innerText = parseFloat(target.p).toFixed(2);
                        if (dom == 'price') i.innerText = parseFloat(target.h).toFixed(2);
                    }
                }
            }
            break;
        }
    }
}