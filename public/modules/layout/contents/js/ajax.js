$(document).on('submit', '.ajax-form', function(e){
	elem = $(this);
	var submit_text = elem.find('[type=submit]').html();
	$('.error_response').attr("style", "display:none");
	$(elem).find('[type=submit]').attr('disabled', 1).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ');
	$("#waiting").show();
	var _url = $(this).attr('action') != '' ? $(this).attr('action') : '';
	var _data = $(this).serialize();
	$.ajax({
		url: _url,
		type: 'POST',
		dataType: 'json',
		data: _data,
	}).done(function(res) {
		$(elem).find('[type=submit]').attr('disabled', false).html(submit_text);
		/*
			0: Success and print message
			1: Success and redirect
			2: Error and print message
			3: Success and show result
		*/
		switch(res.error) {
			case 0:
				toastr.success(res.message);
			break;
			case 1:
				// toastr.success(res.message);
				if (res.redirect != undefined && res.redirect != null) {
					window.location = res.redirect; 
				}
			break;
			case 2:
				toastr.error(res.message);
				
			break;
			case 3:
				$(elem).html(res.result);
				if (res.message != undefined && res.message != null) {
					toastr.success(res.message);
				}
			break;
			case 4:
				v_modal(res.id, res.title);
				v_modal_insert(res.id, res.title, res.result);
			break;
		}
	}).fail(function(err) {
		var error_text = JSON.parse(err.responseText);
		$.each(error_text.errors, function(key, value) {
			toastr.error(value);
		});
		$(elem).find('[type=submit]').attr('disabled', false).html(submit_text);
	});
	return false;
});
