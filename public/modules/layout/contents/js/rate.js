$(document).ready(function() {
	socket.on('rate:message', function(data){
		data = JSON.parse(data);
		$.each(data, function(key, value) {
			$('#'+key+'_rate_sell').text(number_format(value.bid)+ ' Price');
			$('#'+key+'_rate_buy').text(number_format(value.ask)+ ' Price');
		});
	});
	
});