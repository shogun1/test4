let body = document.querySelector('body');
let is_mode = localStorage.getItem('style_mode') || false;
$(document).ready(() => {
    if (!is_mode) {
        setStyleMode('light')
        return;
    }
    body.classList.add(is_mode);
})
$(document).on('click', '[style-mode]', () => {
    if (is_mode === 'light' && body.classList.contains('light')){
        setStyleMode('dark')
        return;
    }
    if (is_mode === 'dark' && body.classList.contains('dark')){
        setStyleMode('light');
        return;
    }
})
function setStyleMode(mode){
    if(is_mode !== mode || !is_mode){
        if(body.classList.contains(is_mode)){
            body.classList.remove(is_mode);
        }
        is_mode = mode;
        localStorage.setItem('style_mode', is_mode);
        setTimeout(() => {
            body.classList.add(is_mode);
        }, 300);
    }
}