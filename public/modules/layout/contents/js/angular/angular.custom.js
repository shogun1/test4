app.controller('Appcontroller', ['$scope', '$sce', '$http', function($scope, $sce, $http) {
    var vm = this;
    $scope.data = {};
    this.loadData = function(api_method, api_url, api_data) {
	    //$("#waiting").show();
	    try {
			var _token = localStorage.get('_token');
		} catch(e){
			var _token = '';
		}

        $http({
	        method: api_method,
	        url: api_url,
	        data: $.param(api_data),
	        headers : {
		        "Content-Type": "application/	",
		        "Authorization": "Bearer " + _token
		        }
        }).then(function(response){
	        if (response.data.status == 200)
		        $scope.data = response.data.data;
		    else
		    {
			    toastr.error(response.data.message);
		    }
        }, function errorCallback(response){
	        toastr.error(response.data.message);
        });
    };
    
    $scope.toTrustedHTML = function( html ){
	    return $sce.trustAsHtml( html );
	}
	try {
		vm.loadData(api_method, api_url, api_data);
	} catch(e){}
}]);