function create_editor(o)
{
    tinyMCE.init({
        mode : "exact",
        //theme : "advanced",
        theme : "modern",
        elements : o,
        height: "400px",
        plugins: [
	         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
	         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
	         "table contextmenu directionality emoticons paste textcolor code"
		],//responsivefilemanager
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | fontselect fontsizeselect",
		toolbar2: "image | media | link unlink anchor | print preview code  | youtube | qrcode | flickr | picasa | forecolor backcolor | easyColorPicker",
		font_formats: 'Arial=arial;Times News Roman=Times News Roman',
		fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        //external_filemanager_path: "/template/filemanager/",
        //filemanager_title: 'Quản lý thư viện',
        file_picker_types: 'image',
        relative_urls: false,
        remove_script_host: false,
        //external_plugins: { "filemanager" : "/filemanager/plugin.min.js"},        
        images_upload_handler: function (blobInfo, success, failure) {
			xhr = new XMLHttpRequest();
		    xhr.withCredentials = false;
		    blob = blobInfo.blob();
		    success("data:"+blob.type+";base64,"+blobInfo.base64());
	  	},
    });
}