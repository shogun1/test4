function appbarCall(e){
    let aside = document.querySelector('#aside');
    let content = document.querySelector('#content');
    if(e.dataset.toggle === 'false'){
        aside.classList.add('active');
        content.classList.add('active');
        e.dataset.toggle = true;
        return;
    }
    if (e.dataset.toggle){
        
        aside.classList.remove('active');
        content.classList.remove('active');
        e.dataset.toggle = false;
        return;
    }
}

var popupID = null;

class POPUP {
    constructor(){
        this.id;
    }
    
    open(id){
        if(this.id){
            this.close();
            return;
        }
        document.querySelector(id).classList.add('active');
        this.id = id;
        return;
    }

    close(){
        document.querySelector(this.id).classList.remove('active');
        this.id = null;
        return;
    }
}

class COPY{
    get(id){
        let target = document.querySelector(id);
        target.select();
        document.execCommand("copy");
    }
}

var _popup = new POPUP();
var _copy = new COPY();