@extends('layouts.app')
@section('content')
    <!-- Breaking News Start -->
    <div class="container-fluid mt-5 mb-3 pt-3">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                        <div class="section-title border-right-0 mb-0" style="width: 180px;">
                            <h4 class="m-0 text-uppercase font-weight-bold">Tranding</h4>
                        </div>
                        <div class="owl-carousel tranding-carousel position-relative d-inline-flex align-items-center bg-white border border-left-0"
                            style="width: calc(100% - 180px); padding-right: 100px;">
                            <div class="text-truncate"><a class="text-secondary text-uppercase font-weight-semi-bold"
                                    href="">Lorem ipsum dolor sit amet elit. Proin interdum lacus eget ante tincidunt,
                                    sed faucibus nisl sodales</a></div>
                            <div class="text-truncate"><a class="text-secondary text-uppercase font-weight-semi-bold"
                                    href="">Lorem ipsum dolor sit amet elit. Proin interdum lacus eget ante tincidunt,
                                    sed faucibus nisl sodales</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breaking News End -->


    <!-- News With Sidebar Start -->
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <!-- News Detail Start -->
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="{{ $page->post_img }}" style="object-fit: cover;">
                        <div class="bg-white border border-top-0 p-4">
                            <div class="mb-3">
                                @foreach($page->Categories($page->post_categories) as $cate)
                                    <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                        href="">{{ $cate->name }}</a>
                                @endforeach
                                <span class="ml-3"><i class="far fa-eye mr-2"></i>{{ $page->view_count }}</span>
                                <span class="ml-3"><i class="fas fa-calendar-alt mr-2"></i>{{ \Carbon\Carbon::createFromTimestamp(strtotime($page->updated_at))->format('d-m-Y') }}</span>
                            </div>
                            <h1 class="mb-3 text-secondary text-uppercase font-weight-bold">{{ $page->post_title }}</h1>
                            <p>{!! $page->post_content !!}</p>
                        </div>
                    </div>
                    <!-- News Detail End -->
                </div>

                <div class="col-lg-4">
                    <!-- Popular News Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Bài viết liên quan</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                            @if(!is_null($pages))
                            @foreach ($pages as $value)
                            <div class="d-flex align-items-center bg-white mb-3" style="height: 110px;">
                                <img class="img-fluid" src="{{ $value->post_img }}" alt="{{ $value->post_title }}" style="width: 100px; height: auto">
                                <div
                                    class="w-100 h-100 px-3 d-flex flex-column justify-content-center border border-left-0">
                                    <div class="mb-2">
                                        @foreach($value->Categories($value->post_categories) as $cate)
                                        <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                            href="">{{ $cate->name }}</a>
                                        @endforeach
                                        <a class="text-body" href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                                    </div>
                                    <a class="h6 m-0 text-secondary text-uppercase font-weight-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ _substr($value->post_title, 40) }}</a>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <!-- Popular News End -->

                    <!-- Tags Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Thẻ</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                            <div class="d-flex flex-wrap m-n1">
                                @foreach($keyword as $key => $value)
                                <a href="{{ route('keyword', ['slug' => $key]) }}" class="btn btn-sm btn-outline-secondary m-1">{{ $value }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- Tags End -->
                </div>
            </div>
        </div>
    </div>
    <!-- News With Sidebar End -->
@endsection
