@extends('admin.app')
@push('css')
<link href="{{ asset('assets\libs\bootstrap-datepicker\bootstrap-datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('assets\libs\switchery\switchery.min.css') }}" rel="stylesheet" type="text/css">
@endpush
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Cập nhật thông tin thành viên</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @include('admin.boxes.notify')

            <!-- Form row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="pull-in">
                                <form id="basic-form" method="POST" action="#">
                                    @csrf
                                    <div>
                                        <h3>Tài khoản</h3>
                                        <section>
                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="username">Tên tài khoản *</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control required" id="username" name="username"
                                                        value="{{ $user->username }}" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="password"> Mật khẩu *</label>
                                                <div class="col-lg-10">
                                                    <input id="password" name="password" type="password"
                                                        class="required form-control">

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="confirm">Nhập lại mật khẩu
                                                    *</label>
                                                <div class="col-lg-10">
                                                    <input id="confirm" name="password_confirmation" type="password"
                                                        class="required form-control">
                                                </div>
                                            </div>

                                        </section>
                                        <h3>Thông tin</h3>
                                        <section>
                                            <div class="form-group row">

                                                <label class="col-lg-2 control-label" for="name"> Họ *</label>
                                                <div class="col-lg-10">
                                                    <input id="name" name="first_name" value="{{ $user->first_name }}"
                                                        type="text" class="required form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label " for="surname"> Tên
                                                    *</label>
                                                <div class="col-lg-10">
                                                    <input id="surname" name="last_name" value="{{ $user->last_name }}"
                                                        type="text" class="required form-control">

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="email">Email *</label>
                                                <div class="col-lg-10">
                                                    <input id="email" disabled name="email" value="{{ $user->email }}"
                                                        type="text" class="required email form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="phone">Số điện thoại *</label>
                                                <div class="col-lg-10">
                                                    <input id="phone" name="phone" value="{{ $user->phone }}"
                                                        type="text" class="required phone form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label" for="address">Địa chỉ *</label>
                                                <div class="col-lg-10">
                                                    <input id="address" name="address" value="{{ $user->address }}"
                                                        type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label">Ngày sinh *</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="birthday"
                                                            value="{{ date('m/d/Y', strtotime($user->birthday)) }}"
                                                            placeholder="mm/dd/yyyy" data-provide="datepicker"
                                                            data-date-autoclose="true">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i
                                                                    class="mdi mdi-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-2 control-label">Ảnh cá nhân</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <input type="button" id="lfm1" data-input="example1"
                                                            data-preview="holder1" value="Image">
                                                        <input id="example1" class="form-control"
                                                            value="{{ $user->image }}" type="text" name="image">
                                                    </div>
                                                    <img src="{{ $user->image }}" id="holder1"
                                                        style="margin-top:15px;max-height:100px;">
                                                </div>
                                            </div>
                                        </section>
                                        <h3>Hoàng thành</h3>
                                        <section>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox-h" name="active" checked value="1"
                                                            type="checkbox">
                                                        <label for="checkbox-h">
                                                            Tôi đồng ý với các điều khoản và điều kiện.
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>
@endsection
@push('js')
<!--Form Wizard-->
<script src="{{ asset('assets\libs\jquery-steps\jquery.steps.min.js') }}"></script>
<!-- Init js-->
<script src="{{ asset('assets\js\pages\form-wizard.init.js') }}"></script>
<script src="{{ asset('assets\libs\bootstrap-datepicker\bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets\libs\switchery\switchery.min.js') }}"></script>
<script src="{{ asset('assets\js\pages\form-advanced.init.js') }}"></script>

@endpush
