@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Danh sách thành viên</h4>
                        <a href="{{ route('admin.users.add') }}"
                            class="button-title float-right btn btn-outline-primary waves-effect width-md waves-light">Thêm thành viên
                            </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @include('admin.boxes.notify')
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                    <thead>
                                        <tr>
                                            <th>Email</th>
                                            <th>Số điện thoại</th>
                                            <th>Tên tài khoản</th>
                                            {{-- <th>2FA</th> --}}
                                            <th>Trạng thái</th>
                                            <th>Quyền</th>
                                            <th>Ngày khởi tạo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $value)
                                        <tr>
                                            <td><a href="{{ route('admin.users.edit', ['id' => $value->id]) }}"
                                                    class="text-primary">{{$value->email}}</a></td>
                                            <td>{{ $value->phone}}</td>
                                            <td>{{ $value->username }}</td>
                                            <td>{!! $status[$value->status] !!}</td>
                                            <td>
                                                @foreach(json_decode($value->roles) as $role)
                                                <span
                                                    class="badge badge-secondary">{{ config('roles.' . $role) }}</span>
                                                @endforeach
                                            </td>
                                            <td>{{ $value->created_at }}</td>
                                            <td style="white-space: nowrap; width: 1%;">
                                                <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                                    <div class="btn-group btn-group-sm" style="float: none;"><a
                                                            href="{{ route('admin.users.delete', ['id' => $value->id]) }}"
                                                            onclick="return confirm('Are you sure to delete the user ?')"><button
                                                                type="button"
                                                                class="tabledit-edit-button btn btn-danger"
                                                                style="float: none;"><span
                                                                    class="mdi mdi-delete"></span></button></a></div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- end .table-responsive-->
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->
</div>
@endsection
