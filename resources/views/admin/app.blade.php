<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Dashboard | Mauevents</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Mauevents" name="description">
    <meta content="Coderthemes" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('image/logo.png') }}">

    <!-- Plugins css-->
    @stack('css')
    <!-- App css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet">
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    {{-- <link href="{{ asset('theme/css/vendors.css') }}" rel="stylesheet" type="text/css"> --}}
</head>

<body>

    <!-- Begin page -->
    <div id="wrapper">


        <!-- Topbar Start -->
        <div class="navbar-custom">
            <ul class="list-unstyled topnav-menu float-right mb-0">

                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#"
                        role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{ Auth::user()->image }}" alt="{{ Auth::user()->username }}"
                            class="rounded-circle">
                        <span class="pro-user-name ml-1">
                            {{ Auth::user()->username }} <i class="mdi mdi-chevron-down"></i>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Xin chào !</h6>
                        </div>

                        <!-- item-->
                        <a href="{{ route('admin.profile') }}" class="dropdown-item notify-item">
                            <i class="mdi mdi-account-outline"></i>
                            <span>Thông tin cá nhân</span>
                        </a>

                        <div class="dropdown-divider"></div>

                        <!-- item-->
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button class="dropdown-item notify-item">
                                <i class="mdi mdi-logout-variant"></i>
                                <span>Đăng xuất</span>
                            </button>
                        </form>

                    </div>
                </li>


            </ul>

            <!-- LOGO -->
            <div class="logo-box">
                <a href="{{ route('home') }}" class="logo text-center logo-dark">
                    <span class="logo-lg">
                        <img src="{{ asset('image/logo.png') }}" alt="" height="65">
                    </span>
                    <span class="logo-sm">
                        <!-- <span class="logo-lg-text-dark">V</span> -->
                        <img src="{{ asset('image/logo.png') }}" alt="" height="65">
                    </span>
                </a>

                <a href="{{ route('home') }}" class="logo text-center logo-light">
                    <span class="logo-lg">
                        <img src="{{ asset('image/logo.png') }}" alt="demo_khach" height="65">
                    </span>
                    <span class="logo-sm">
                        <!-- <span class="logo-lg-text-dark">V</span> -->
                        <img src="{{ asset('image/logo.png') }}" alt="demo_khach" height="65">
                    </span>
                </a>
            </div>

            <!-- LOGO -->


            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile waves-effect">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>
            </ul>
        </div>
        <!-- end Topbar -->
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">

            <div class="slimscroll-menu">

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <ul class="metismenu" id="side-menu">

                        <li class="menu-title">Navigation</li>
                        @foreach($admin_menu as $key => $value)
                        @if ($role_sidebar == 'super-admin')
                        <li>
                            <a href="{{ isset($value['sub']) ? 'javascript: void(0);' : $value['url']  }}" class="waves-effect">
                                <i class="{{ $value['icon'] }}"></i>
                                <span> {{ $value['title'] }} </span>
                                @if(isset($value['sub']))
                                <span class="menu-arrow"></span>
                                @endif
                            </a>
                            @if(isset($value['sub']))
                            <ul class="nav-second-level" aria-expanded="false">
                                @foreach($value['sub'] as $value_sub)
                                <li><a href="{{ $value_sub['url'] }}">{{ $value_sub['title'] }}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @elseif(in_array($value['name'], $permissions_sidebar))
                        <li>
                            <a href="{{ isset($value['sub']) ? 'javascript: void(0);' : $value['url']  }}" class="waves-effect">
                                <i class="{{ $value['icon'] }}"></i>
                                <span> {{ $value['title'] }} </span>
                                @if(isset($value['sub']))
                                <span class="menu-arrow"></span>
                                @endif
                            </a>
                            @if(isset($value['sub']))
                            <ul class="nav-second-level" aria-expanded="false">
                                @foreach($value['sub'] as $value_sub)
                                @if(in_array($value_sub['name'], $permissions_sidebar))
                                <li><a href="{{ $value_sub['url'] }}">{{ $value_sub['title'] }}</a></li>
                                @endif
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endif
                        @endforeach
                    </ul>

                </div>
                <!-- End Sidebar -->

                <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        @yield('content')

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

    </div>
    <!-- END wrapper -->


    <!-- Right Sidebar -->
    <div class="right-bar">
        <div class="rightbar-title">
            <a href="javascript:void(0);" class="right-bar-toggle float-right">
                <i class="mdi mdi-close"></i>
            </a>
            <h4 class="font-17 m-0 text-white">Theme Customizer</h4>
        </div>
        <div class="slimscroll-menu">

            <div class="p-4">
                <div class="alert alert-warning" role="alert">
                    <strong>Customize </strong> the overall color scheme, layout, etc.
                </div>
                <div class="mb-2">
                    <img src="{{ asset('assets/images/layouts/light.png') }}" class="img-fluid img-thumbnail" alt="">
                </div>
                <div class="custom-control custom-switch mb-3">
                    <input type="checkbox" class="custom-control-input theme-choice" id="light-mode-switch" checked="">
                    <label class="custom-control-label" for="light-mode-switch">Light Mode</label>
                </div>

                <div class="mb-2">
                    <img src="{{ asset('assets/images/layouts/dark.png') }}" class="img-fluid img-thumbnail" alt="">
                </div>
                <div class="custom-control custom-switch mb-3">
                    <input type="checkbox" class="custom-control-input theme-choice" id="dark-mode-switch"
                        data-bsstyle="assets/css/bootstrap-dark.min.css" data-appstyle="assets/css/app-dark.min.css">
                    <label class="custom-control-label" for="dark-mode-switch">Dark Mode</label>
                </div>

                <div class="mb-2">
                    <img src="{{ asset('assets/images/layouts/rtl.png') }}" class="img-fluid img-thumbnail" alt="">
                </div>
                <div class="custom-control custom-switch mb-5">
                    <input type="checkbox" class="custom-control-input theme-choice" id="rtl-mode-switch"
                        data-appstyle="assets/css/app-rtl.min.css">
                    <label class="custom-control-label" for="rtl-mode-switch">RTL Mode</label>
                </div>

            </div>
        </div> <!-- end slimscroll-menu-->
    </div>
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- Vendor js -->
    <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

    @stack('js')


    <!-- App js -->
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
</body>

</html>
