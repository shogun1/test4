@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">All Administered</h4>
                        <a href="{{ route('admin.permissions.user.add') }}"
                            class="button-title float-right btn btn-outline-primary waves-effect width-md waves-light">Add
                            user</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @include('admin.boxes.notify')
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered mb-0 table-nowrap" id="btn-editable">
                                    <thead>
                                        <tr>
                                            <th>{{ __('Username') }}</th>
                                            <th>{{ __('Permission') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Delete') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($users) > 0)
                                        @foreach($users as $value)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.permissions.user.edit', ['id' => $value->id]) }}"
                                                    title="Edit order">{{ $value->email }}</a>
                                            </td>
                                            <td>
                                                @if(@$value->Role()->name)
                                                <span class="badge badge-primary">{{ $value->Role()->name }}</span>
                                                @endif
                                            </td>
                                            <td>{{ $value->created_at }}</td>
                                            <td style="white-space: nowrap; width: 1%;">
                                                <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                                    <div class="btn-group btn-group-sm" style="float: none;"><a
                                                            href="{{ route('admin.permissions.user.delete', ['id' => $value->id]) }}"
                                                            onclick="return confirm('Are you sure to delete the permission ?')"><button
                                                                type="button"
                                                                class="tabledit-edit-button btn btn-danger"
                                                                style="float: none;"><span
                                                                    class="mdi mdi-delete"></span></button></a></div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="2">{{ __('Items not found.') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- end .table-responsive-->
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->
</div>
@endsection
