@extends('admin.app')
@section('content')
<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Edit user</h4>
                        {{-- <div class="page-title-right">
                            <ol class="breadcrumb p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">batdongsan24h.vn</a></li>
                                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                <li class="breadcrumb-item active">General Elements</li>
                            </ol>
                        </div> --}}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @include('admin.boxes.notify')

            <!-- Form row -->
            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <form method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" id="title" value="{{ $user->email }}"
                                        name="username" placeholder="Add user">
                                </div>
                                <div class="form-group">
                                    <label for="">Choose the right</label>
                                    @foreach(@$roles as $value)
                                    <div class="custom-control custom-radio ml-1">
                                        <input type="radio" id="{{ $value->slug }}"
                                            {{ $user->permission == $value->slug ? 'checked' : '' }}
                                            value="{{ $value->slug }}" name="permission" class="custom-control-input">
                                        <label class="custom-control-label text-xs"
                                            for="{{ $value->slug }}">{{ $value->name }}</label>
                                    </div>
                                    @endforeach
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end col -->

            </div>

        </div>
        <!-- end container-fluid -->

    </div>
    <!-- end content -->



    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    2021 © batdongsan24h.vn
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>
@endsection
