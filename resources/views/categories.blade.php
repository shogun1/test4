@extends('layouts.app')
@section('content')
    <!-- News With Sidebar Start -->
    <div class="container-fluid mt-5 pt-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        @if(isset($type_categories))
                        <div class="col-12">
                            <div class="section-title">
                                <h4 class="m-0 text-uppercase font-weight-bold">Danh mục: {{ $type_categories->name }}</h4>
                                {{-- <a class="text-secondary font-weight-medium text-decoration-none" href="">View All</a> --}}
                            </div>
                        </div>
                        @endif
                        @foreach($pages as $value)
                        <div class="col-lg-6">
                            <div class="position-relative mb-3">
                                <img class="img-fluid w-100" src="{{ $value->post_img }}"
                                    style="object-fit: cover;">
                                <div class="bg-white border border-top-0 p-4">
                                    <div class="mb-2">
                                        @foreach($value->Categories($value->post_categories) as $cate)
                                        <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                            href="">{{ $cate->name }}</a>
                                        @endforeach
                                        <a class="text-body" href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                                        <small class="ml-3"><i class="far fa-eye mr-2"></i>{{ $value->view_count }}</small>
                                    </div>
                                    <a class="h4 d-block mb-3 text-secondary text-uppercase font-weight-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ _substr($value->post_title, 50) }}</a>
                                    <p class="m-0">{!! _substr($value->post_content, 100) !!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="mx-auto">
                            {{ $pages->links() }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">

                    <!-- Popular News Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Bài viết nổi bật</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                            @foreach ($hang_muc_noi_bac->Pages($hang_muc_noi_bac->slug) as $value)
                            <div class="d-flex align-items-center bg-white mb-3" style="height: 110px;">
                                <img class="img-fluid" src="{{ $value->post_img }}" alt="{{ $value->post_title }}" style="width: 100px; height: auto">
                                <div
                                    class="w-100 h-100 px-3 d-flex flex-column justify-content-center border border-left-0">
                                    <div class="mb-2">
                                        @foreach($value->Categories($value->post_categories) as $cate)
                                        <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                            href="">{{ $cate->name }}</a>
                                        @endforeach
                                        <a class="text-body" href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                                    </div>
                                    <a class="h6 m-0 text-secondary text-uppercase font-weight-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ _substr($value->post_title, 40) }}</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- Popular News End -->

                    <!-- Tags Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Thẻ</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                            <div class="d-flex flex-wrap m-n1">
                                @foreach($keyword as $key => $value)
                                <a href="{{ route('keyword', ['slug' => $key]) }}" class="btn btn-sm btn-outline-secondary m-1">{{ $value }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- Tags End -->
                </div>
            </div>
        </div>
    </div>
    <!-- News With Sidebar End -->
@endsection
