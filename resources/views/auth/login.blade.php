@extends('layouts.app_auth')

@section('content')
@include('admin.boxes.notify')
<div class="card-body">
    <form action="{{ route('login') }}" method="POST" class="p-2">
        @csrf
        <div class="form-group mb-3">
            <label for="emailaddress">Email Address :</label>
            <input class="form-control" name="email" type="email" id="emailaddress" required=""
                placeholder="">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group mb-3">
            <label for="password">Password :</label>
            <input class="form-control" type="password" name="password" required="" id="password"
                placeholder="Enter your password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        {{-- <div class="form-group mb-4">
            <div class="checkbox checkbox-success">
                <input id="remember" type="checkbox" checked="">
                <label for="remember">
                    Remember me
                </label>
                <a href="pages-recoverpw.html" class="text-muted float-right">Forgot your password?</a>
            </div>
        </div> --}}

        <div class="form-group row text-center mt-4 mb-4">
            <div class="col-12">
                <button class="btn btn-md btn-block btn-primary waves-effect waves-light" type="submit">Sign In</button>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-sm-12 text-center">
                <p class="text-muted mb-0">Don't have an account? <a href="{{ route('register') }}"
                        class="text-dark m-l-5"><b>Sign Up</b></a></p>
            </div>
        </div>
    </form>

</div>
@endsection
