@extends('layouts.app_auth')

@section('content')
@include('admin.boxes.notify')
<div class="card-body">
    <form action="{{ route('register') }}" method="POST" class="p-2">
        @csrf
        <div class="form-group mb-3">
            <label for="emailaddress">Phone Number :</label>
            <input class="form-control" name="phone" type="text" value="{{ old('phone') }}" id="emailaddress" required=""
                placeholder="Enter your Phone Number">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group mb-3">
            <label for="emailaddress">Username :</label>
            <input class="form-control" name="username" type="text" value="{{ old('username') }}" id="username" required=""
                placeholder="Enter your username">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group mb-3">
            <label for="emailaddress">Email Address :</label>
            <input class="form-control" name="email" type="email" value="{{ old('email') }}" id="emailaddress" required=""
                placeholder="Enter your email address">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group mb-3">
            <label for="password">Password :</label>
            <input class="form-control" type="password" name="password" required="" id="password"
                placeholder="Enter your password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group mb-3">
            <label for="password">Confirm Password :</label>
            <input class="form-control" type="password" name="password_confirmation" required="" id="password"
                placeholder="Enter your confirm password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group row text-center mt-4 mb-4">
            <div class="col-12">
                <button class="btn btn-md btn-block btn-primary waves-effect waves-light" type="submit">Register</button>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-sm-12 text-center">
                <a href="{{ route('login') }}">Already have account?</a>
            </div>
        </div>
    </form>

</div>
@endsection
