@extends('layouts.app')
@section('content')
    <!-- Contact Start -->
    <div class="container-fluid pt-3 gioi_thieu_mobile">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title mb-0">
                        <h4 class="m-0 text-uppercase font-weight-bold">GIỚI THIỆU</h4>
                    </div>
                    <div class="bg-white border border-top-0 p-4 mb-3">
                        <div class="row">
                            {{-- <h6 class="text-uppercase font-weight-bold">NHIỀU ƯU ĐIỂM</h6> --}}
                            <p class="my-0">{{ $settings['site_description'] }}</p>
                            <a href="tel:{{ $settings['site_phone'] }}" class="navbar-brand d-block d-lg-none tel">
                                Hotline: <span class="text-danger text-uppercase font-weight-normal" style="color: #fd0f50 !important">0357.32.72.72</span>
                            </a>
                            {{-- <div class="col-xl-4 col-lg-4 col-md-6 text-center">
                                <div class="d-flex align-items-center mb-2" style="display: inline-flex  !important;">
                                    <i class="fa fa-map-marker-alt text-primary mr-2"></i>
                                    <h6 class="font-weight-bold mb-0">Địa chỉ</h6>
                                </div>
                                <p class="m-0"><a href="tel:{{ $settings['site_address'] }}"> <span class="text-dark">{{ $settings['site_address'] }}</span> </a></p>
                            </div> --}}
                            {{-- <div class="col-xl-6 col-lg-6 col-md-6 col-6 text-center">
                                <div class="d-flex align-items-center mb-2" style="display: inline-flex  !important;">
                                    <i class="fa fa-envelope-open text-primary mr-2"></i>
                                    <h6 class="font-weight-bold mb-0">Email</h6>
                                </div>
                                <p class="m-0"><a href="tel:{{ $settings['site_email'] }}"> <span class="text-dark">{{ $settings['site_email'] }}</span> </a></p>
                            </div> --}}
                            {{-- <div class="col-xl-6 col-lg-6 col-md-6 col-6 text-center">
                                <div class="d-flex align-items-center mb-2" style="display: inline-flex  !important;">
                                    <i class="fa fa-phone-alt text-primary mr-2"></i>
                                    <h6 class="font-weight-bold mb-0">Số điện thoại</h6>
                                </div>
                                <p class="m-0"><a href="tel:{{ $settings['site_phone'] }}"> <span class="text-dark">{{ $settings['site_phone'] }}</span> </a></p>
                            </div> --}}
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[0]->image }}" alt="xe 4 chỗ">
                                </div>
                                <h6 class="">Xe 4 chỗ</h6>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[1]->image }}" alt="xe 5 chỗ">
                                </div>
                                <h6 class="">Xe 5 chỗ</h6>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[2]->image }}" alt="xe 7 chỗ">
                                </div>
                                <h6 class="">Xe 7 chỗ</h6>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[3]->image }}" alt="xe 16 chỗ">
                                </div>
                                <h6 class="">Xe 16 chỗ</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->


    <!-- Main News Slider Start -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 px-0 hang_muc_slide">
                <div class="owl-carousel main-carousel position-relative">
                    @foreach($hang_muc_slide->Pages($hang_muc_slide->slug) as $value)
                    <div class="position-relative overflow-hidden" style="height: 500px;">
                        <img class="img-fluid h-100" src="{{ $value->post_img }}"
                            style="object-fit: cover;">
                        <div class="overlay">
                            <div class="mb-2">
                                @foreach($value->Categories($value->post_categories) as $cate)
                                <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                    href="{{ route('categories', ['slug' => $cate->slug]) }}">{{ $cate->name }}</a>
                                @endforeach
                                <a class="text-white" href="">{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</a>
                            </div>
                            <a class="h2 m-0 text-white text-uppercase font-weight-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ $value->post_title }}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-5 px-0 hang_muc_top">
                <div class="row mx-0">
                    @foreach($hang_muc_top->Pages($hang_muc_top->slug) as $value)
                    <div class="col-md-6 px-0">
                        <div class="position-relative overflow-hidden" style="height: 250px;">
                            <img class="img-fluid w-100 h-100" src="{{ $value->post_img }}"
                                style="object-fit: cover;">
                            <div class="overlay">
                                <div class="mb-2">
                                    @foreach($value->Categories($value->post_categories) as $cate)
                                    <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                        href="{{ route('categories', ['slug' => $cate->slug]) }}">{{ $cate->name }}</a>
                                    @endforeach
                                    <a class="text-white" href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                                </div>
                                <a class="h6 m-0 text-white text-uppercase font-weight-semi-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ $value->post_title }}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Main News Slider End -->


    <!-- Breaking News Start -->
    <div class="container-fluid bg-dark py-3 mb-3 hang_muc_uu_tien">
        <div class="container">
            <div class="row align-items-center bg-dark">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                        <div class="bg-primary text-dark text-center font-weight-medium py-2" style="width: 170px;">
                            bài viết ưu tiên</div>
                        <div class="owl-carousel tranding-carousel position-relative d-inline-flex align-items-center ml-3"
                            style="width: calc(100% - 170px); padding-right: 90px;">
                            @foreach($hang_muc_uu_tien->Pages($hang_muc_uu_tien->slug) as $value)
                            <div class="text-truncate"><a class="text-white text-uppercase font-weight-semi-bold"
                                    href="{{ route('news', ['slug' => $value->slug]) }}">{{ $value->post_title }}</a></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breaking News End -->


     <!-- Contact Start -->
     <div class="container-fluid pt-3 gioi_thieu_desktop">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title mb-0">
                        <h4 class="m-0 text-uppercase font-weight-bold">GIỚI THIỆU</h4>
                    </div>
                    <div class="bg-white border border-top-0 p-4 mb-3">
                        <div class="row">
                            {{-- <h6 class="text-uppercase font-weight-bold">NHIỀU ƯU ĐIỂM</h6> --}}
                            <p class="my-0">{{ $settings['site_description'] }}</p>
                            <a href="tel:{{ $settings['site_phone'] }}" class="navbar-brand d-block d-lg-none tel">
                                Hotline: <span class="text-danger text-uppercase font-weight-normal" style="color: #fd0f50 !important">0357.32.72.72</span>
                            </a>
                            {{-- <div class="col-xl-4 col-lg-4 col-md-6 text-center">
                                <div class="d-flex align-items-center mb-2" style="display: inline-flex  !important;">
                                    <i class="fa fa-map-marker-alt text-primary mr-2"></i>
                                    <h6 class="font-weight-bold mb-0">Địa chỉ</h6>
                                </div>
                                <p class="m-0"><a href="tel:{{ $settings['site_address'] }}"> <span class="text-dark">{{ $settings['site_address'] }}</span> </a></p>
                            </div> --}}
                            {{-- <div class="col-xl-6 col-lg-6 col-md-6 col-6 text-center">
                                <div class="d-flex align-items-center mb-2" style="display: inline-flex  !important;">
                                    <i class="fa fa-envelope-open text-primary mr-2"></i>
                                    <h6 class="font-weight-bold mb-0">Email</h6>
                                </div>
                                <p class="m-0"><a href="tel:{{ $settings['site_email'] }}"> <span class="text-dark">{{ $settings['site_email'] }}</span> </a></p>
                            </div> --}}
                            {{-- <div class="col-xl-6 col-lg-6 col-md-6 col-6 text-center">
                                <div class="d-flex align-items-center mb-2" style="display: inline-flex  !important;">
                                    <i class="fa fa-phone-alt text-primary mr-2"></i>
                                    <h6 class="font-weight-bold mb-0">Số điện thoại</h6>
                                </div>
                                <p class="m-0"><a href="tel:{{ $settings['site_phone'] }}"> <span class="text-dark">{{ $settings['site_phone'] }}</span> </a></p>
                            </div> --}}
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[0]->image }}" alt="xe 4 chỗ">
                                </div>
                                <h6 class="">Xe 4 chỗ</h6>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[1]->image }}" alt="xe 5 chỗ">
                                </div>
                                <h6 class="">Xe 5 chỗ</h6>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[2]->image }}" alt="xe 7 chỗ">
                                </div>
                                <h6 class="">Xe 7 chỗ</h6>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-2 text-center col-6 pt-4">
                                <div class="d-flex align-items-center mb-2">
                                    <img class="rounded img-fluid d-block" src="{{ $ImageFooter[3]->image }}" alt="xe 16 chỗ">
                                </div>
                                <h6 class="">Xe 16 chỗ</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->

    <!-- Featured News Slider Start -->
    <div class="container-fluid">
        <div class="container">
            <div class="section-title">
                <h4 class="m-0 text-uppercase font-weight-bold">Bài viết xem nhiều nhất</h4>
            </div>
            <div class="owl-carousel news-carousel carousel-item-4 position-relative">
                @foreach($pages_top_views as $value)
                <div class="position-relative overflow-hidden" style="height: 300px;">
                    <img class="img-fluid h-100" src="{{ $value->post_img }}"
                        style="object-fit: cover;">
                    <div class="overlay">
                        <div class="mb-2">
                            @foreach($value->Categories($value->post_categories) as $cate)
                            <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                href="{{ route('categories', ['slug' => $cate->slug]) }}">{{ $cate->name }}</a>
                            @endforeach
                            <a class="text-white" href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                        </div>
                        <a class="h6 m-0 text-white text-uppercase font-weight-semi-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ $value->post_title }}</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Featured News Slider End -->


    <!-- News With Sidebar Start -->
    <div class="container-fluid pt-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title">
                                <h4 class="m-0 text-uppercase font-weight-bold">Bài viết mới nhất</h4>
                                {{-- <a class="text-secondary font-weight-medium text-decoration-none" href="">View All</a> --}}
                            </div>
                        </div>
                        @foreach($pages_update as $value)
                        <div class="col-lg-6">
                            <div class="position-relative mb-3">
                                <img class="img-fluid w-100" src="{{ $value->post_img }}"
                                    style="object-fit: cover;">
                                <div class="bg-white border border-top-0 p-4">
                                    <div class="mb-2">
                                        @foreach($value->Categories($value->post_categories) as $cate)
                                        <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                            href="{{ route('categories', ['slug' => $cate->slug]) }}">{{ $cate->name }}</a>
                                        @endforeach
                                        <a class="text-body" href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                                        <small class="ml-3"><i class="far fa-eye mr-2"></i>{{ $value->view_count }}</small>
                                    </div>
                                    <a class="h4 d-block mb-3 text-secondary text-uppercase font-weight-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ _substr($value->post_title, 50) }}</a>
                                    <p class="m-0">{!! strip_tags(_substr($value->post_content, 100)) !!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-lg-4">
                    <!-- Popular News Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Bài viết nổi bật</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                            @foreach ($hang_muc_noi_bac->Pages($hang_muc_noi_bac->slug) as $value)
                            <div class="d-flex align-items-center bg-white mb-3" style="height: 110px;">
                                <img class="img-fluid" src="{{ $value->post_img }}" alt="{{ $value->post_title }}" style="width: 100px; height: auto">
                                <div
                                    class="w-100 h-100 px-3 d-flex flex-column justify-content-center border border-left-0">
                                    <div class="mb-2">
                                        @foreach($value->Categories($value->post_categories) as $cate)
                                        <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                            href="{{ route('categories', ['slug' => $cate->slug]) }}">{{ $cate->name }}</a>
                                        @endforeach
                                        <a class="text-body" href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                                    </div>
                                    <a class="h6 m-0 text-secondary text-uppercase font-weight-bold" href="{{ route('news', ['slug' => $value->slug]) }}">{{ _substr($value->post_title, 40) }}</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- Popular News End -->

                    <!-- Tags Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Thẻ</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                            <div class="d-flex flex-wrap m-n1">
                                @foreach($keyword as $key => $value)
                                <a href="{{ route('keyword', ['slug' => $key]) }}" class="btn btn-sm btn-outline-secondary m-1">{{ $value }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- Tags End -->
                </div>
            </div>
        </div>
    </div>
    <!-- News With Sidebar End -->
@endsection
