<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Taxivungtau</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="taxivungtau" name="keywords">
    <meta content="taxivungtau" name="description">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- social icon -->
    <link href="{{ asset('home/css/social.css?version=9') }}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('home/lib/owlcarousel/assets/owl.carousel.min.css?version=9') }}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('home/css/style.css?version=9') }}" rel="stylesheet">
    @include('layouts.seo')
    <style>
        span.text-danger.text-uppercase.font-weight-normal {
            font-weight: 600 !important;
        }
        @media (max-width: 768px) {
            .hang_muc_top {
                display: none;
            }
            .hang_muc_slide {
                display: none;
            }
            .hang_muc_uu_tien {
                display: none;
            }
            .gioi_thieu_desktop {
                display: none;
            }
        }
        @media (min-width: 960px) {
            .gioi_thieu_mobile {
                display: none;
            }
        }
    </style>
</head>

<body>
    <div class="row">
        <div class="col-3">
            <div class="hotline-phone-ring-wrap">
                <div class="hotline-phone-ring">
                    <div class="hotline-phone-ring-circle"></div>
                    <div class="hotline-phone-ring-circle-fill"></div>
                    <div class="hotline-phone-ring-img-circle"> <a href="tel:{{ $settings['site_phone'] }}" class="pps-btn-img"> <img
                                src="https://netweb.vn/img/hotline/icon.png" alt="so dien thoai" width="50"> </a></div>
                </div>
                <div class="hotline-bar">
                    <a href="tel:{{ $settings['site_phone'] }}"> <span class="text-hotline">{{ $settings['site_phone'] }}</span> </a>
                </div>

            </div>
        </div>
        <div class="col-3">
            <div class="float-icon-hotline">
                <ul class="left-icon hotline">
                    <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton"
                            href="{{ $settings['address_zalo'] }}"><i
                                class="fa fa-zalo animated infinite tada"></i><span>Zalo</span></a></li>
                    {{-- <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton"
                            href="{{ $settings['address_message'] }}"><i
                                class="fa fa-messenger animated infinite tada"></i><span>Facebook</span></a></li> --}}
                </ul>
            </div>
        </div>
    </div>



    <!-- Topbar Start -->
    <div class="container-fluid d-none d-lg-block">

        <div class="row align-items-center bg-white py-3 px-lg-5">
            <div class="col-lg-4">
                <a href="{{ url('/') }}" class="navbar-brand p-0 d-none d-lg-block">
                    <h1 class="m-0 display-4 text-uppercase text-primary">Taxi<span
                            class="text-secondary font-weight-normal"> VŨNG TÀU</span></h1>
                </a>
            </div>
            <div class="col-lg-8 text-center text-lg-right">
                <a href="https://htmlcodex.com"><img class="img-fluid" src="{{ asset('home/img/ads-728x90.png') }}"
                        alt=""></a>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg bg-dark navbar-dark py-2 py-lg-0 px-lg-5">
            <div class="">
                <a href="{{ url('/') }}" class="navbar-brand d-block d-lg-none">
                    <h1 class="m-0 display-4 text-uppercase text-primary">Taxi<span
                            class="text-white font-weight-normal"> VŨNG TÀU</span></h1>
                </a>
                <a href="tel:{{ $settings['site_phone'] }}" class="navbar-brand d-block d-lg-none tel">
                    Hotline: <span class="text-danger text-uppercase font-weight-normal" style="color: #fd0f50 !important">0357.32.72.72</span>
                </a>
            </div>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between px-0 px-lg-3" id="navbarCollapse">
                <div class="navbar-nav mr-auto py-0">
                    <a href="{{ url('/') }}" class="nav-item nav-link active">trang chủ</a>
                    @foreach ($categories as $key => $value)
                        <div class="nav-item dropdown">
                            <a href="{{ route('categories', ['slug' => $value->slug]) }}" target="_blank"
                                class="nav-link {{ !is_null($value->Pages($value->id)) ? 'dropdown-toggle' : '' }}"
                                data-toggle="dropdown">{{ $value->name }}</a>
                            @if (!is_null($value->Pages($value->id)))
                                @foreach ($value->Pages($value->id) as $value_page)
                                    <div class="dropdown-menu rounded-0 m-0">
                                        <a href="{{ route('news', ['slug' => $value_page->slug]) }}"
                                            class="dropdown-item">{{ $value_page->post_title }}</a>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                    <a href="{{ route('contact') }}" class="nav-item nav-link">Liên hệ</a>
                </div>
                <form action="{{ route('search') }}" method="get">
                    <div class="input-group ml-auto d-none d-lg-flex" style="width: 100%; max-width: 300px;">
                        <input type="text" name="search" class="form-control border-0" placeholder="Từ khóa tìm kiếm">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text bg-primary text-dark border-0 px-3"><i
                                    class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </nav>
    </div>
    <!-- Navbar End -->

    @yield('content')

    <!-- Footer Start -->
    <div class="container-fluid bg-dark pt-5 px-sm-3 px-md-5 mt-5">
        <div class="row py-4">
            <div class="col-lg-3 col-md-6 mb-5">
                <h5 class="mb-4 text-white text-uppercase font-weight-bold">THÔNG TIN LIÊN HỆ</h5>
                <p class="font-weight-medium text-white"><i
                        class="fa fa-map-marker-alt mr-2"></i>{{ $settings['site_address'] }}</p>
                <p class="font-weight-medium text-white"><i class="fa fa-phone-alt mr-2"></i>{{ $settings['site_phone'] }}</p>
                <p class="font-weight-medium text-white"><i class="fa fa-envelope mr-2"></i>{{ $settings['site_email'] }}</p>
                {{-- <h6 class="mt-4 mb-3 text-white text-uppercase font-weight-bold">Follow Us</h6>
                <div class="d-flex justify-content-start">
                    <a class="btn btn-lg btn-secondary btn-lg-square mr-2" href="#"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-lg btn-secondary btn-lg-square mr-2" href="#"><i
                            class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-lg btn-secondary btn-lg-square mr-2" href="#"><i
                            class="fab fa-linkedin-in"></i></a>
                    <a class="btn btn-lg btn-secondary btn-lg-square mr-2" href="#"><i class="fab fa-instagram"></i></a>
                    <a class="btn btn-lg btn-secondary btn-lg-square" href="#"><i class="fab fa-youtube"></i></a>
                </div> --}}
            </div>
            <div class="col-lg-3 col-md-6 mb-5">
                <h5 class="mb-4 text-white text-uppercase font-weight-bold">Bài viết nổi bật</h5>
                @foreach ($post_hang_muc_noi_bac as $value)
                    <div class="mb-3">
                        <div class="mb-2">
                            @foreach ($value->Categories($value->post_categories) as $cate)
                                <a class="badge badge-primary text-uppercase font-weight-semi-bold px-2"
                                    href="{{ route('categories', ['slug' => $cate->slug]) }}">{{ $cate->name }}</a>
                            @endforeach
                            <a class="text-white"
                                href=""><small>{{ \Carbon\Carbon::createFromTimestamp(strtotime($value->updated_at))->format('d-m-Y') }}</small></a>
                        </div>
                        <a class="small text-white text-uppercase font-weight-medium"
                            href="{{ route('news', ['slug' => $value->slug]) }}">{{ _substr($value->post_title, 50) }}</a>
                    </div>
                @endforeach
            </div>
            <div class="col-lg-3 col-md-6 mb-5">
                <h5 class="mb-4 text-white text-uppercase font-weight-bold">Danh mục</h5>
                <div class="m-n1">
                    @if (!is_null($categories))
                        @foreach ($categories as $key => $value)
                            <a href="{{ route('categories', ['slug' => $value->slug]) }}"
                                class="btn btn-sm btn-secondary m-1">{{ $value->name }}</a>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-5">
                <h5 class="mb-4 text-white text-uppercase font-weight-bold">Hình ảnh</h5>
                <div class="row">
                    @foreach($ImageFooter as $value)
                    <div class="col-4 mb-3">
                        <a href="{{ $value->image }}" target="_blank"><img class="w-100" src="{{ $value->image }}" alt="{{ $value->name }}"></a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid py-4 px-sm-3 px-md-5" style="background: #111111;">
        <p class="m-0 text-center text-white">2021 &copy; <a href="#">taxivungtau.com</a></p>
    </div>
    <!-- Footer End -->



    <!-- Back to Top -->
    <a href="#" class="btn btn-primary btn-square back-to-top"><i class="fa fa-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('home/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('home/lib/owlcarousel/owl.carousel.min.js') }}"></script>

    <!-- Template Javascript -->
    <script src="{{ asset('home/js/main.js') }}"></script>
</body>

</html>
