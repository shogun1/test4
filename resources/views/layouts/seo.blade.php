    <meta name='Title' content="{{ isset($post['seo_title']) ? $post['seo_title'] : $settings['title_website'] }}">
    <meta name='Description' content="{{ isset($post['seo_description']) ? $post['seo_description'] : $settings['site_description'] }}">
    <meta name='Keywords' content="{{ isset($post['seo_keywords']) ? $post['seo_keywords'] : $settings['site_keywords'] }}">
    <meta name='Copyright' content="Mauevents">
    <meta name='Classification' content="Personal">
    <meta name='Rating' content="General">
    <meta name='Revisit-After' content="7 Days">
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv='Content-Type' content="text/html; charset=utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="DC.Description" content="{{ isset($post['seo_description']) ? $post['seo_description'] : $settings['site_description'] }}">
    <meta property="og:site_name" content="{{ isset($post['seo_title']) ? $post['seo_title'] : $settings['title_website'] }}" />
    <meta property="og:image" content="{{ asset(isset($post['seo_image']) ? $post['seo_image'] : $settings['site_default_thumbnail']) }}" />
    <meta property="og:image:width" content="490px" />
    <meta property="og:image:height" content="292px" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ isset($post['seo_title']) ? $post['seo_title'] : $settings['title_website'] }}" />
    <meta property="og:description" content="{{ isset($post['seo_description']) ? $post['seo_description'] : $settings['site_description'] }}">
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta name="geo.placename" content="Viet Nam" />
    <meta name="geo.region" content="vn" />
    <meta name="DC.Language" content="vn" >
    <link rel="alternate" href="{{ url('/') }}" hreflang="vn">
    <meta name="robots" content="noodp,index,follow">
    <meta property="fb:app_id" content="">
    <meta name="alexaVerifyID" content="">
    <link rel="shortcut icon" href="{{ asset($settings['favicon']) }}" type="image/x-icon" />
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type"   : "Organization",
            "url"     : "{{ url('/') }}",
            "logo"    : "{{ asset($settings['site_logo']) }}",
            "contactPoint": [
            {
                "@type": "ContactPoint",
                "telephone": "{{ $settings['site_phone'] }}",
                "contactType": "customer service"
            }
            ]
        }
    </script>
    {!! $settings['google_analytics'] !!}
