-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th9 21, 2021 lúc 03:31 PM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `batdongsan24_vn`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_delete` int(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `departments`
--

INSERT INTO `departments` (`id`, `name`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'Giám đốc kinh doanh', 1, '2021-09-19 06:22:18', '2021-09-19 07:15:03'),
(6, 'Trưởng phòng KD 1', 0, '2021-09-19 08:44:47', '2021-09-19 08:44:47'),
(7, 'Trưởng phòng KD 2', 0, '2021-09-19 08:45:01', '2021-09-19 08:45:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `folder`
--

CREATE TABLE `folder` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT 0,
  `level` int(1) DEFAULT 0,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_folder` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_delete` int(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `folder`
--

INSERT INTO `folder` (`id`, `parent_id`, `level`, `user_id`, `name`, `slug`, `user_folder`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 1, 'Chung cư Vinhome Symphony', 'chung-cu-vinhome-symphony', '29', 0, '2021-09-04 23:50:37', '2021-09-20 08:51:47'),
(2, 0, 0, 1, 'Chung cư Thành phố Thuận', 'chung-cu-thanh-pho-thuan', '9', 0, '2021-09-04 23:51:24', '2021-09-16 07:14:07'),
(3, 1, 1, 1, 'Khu A - Vinhome Symphony', 'khu-a-vinhome-symphony', '2', 0, '2021-09-04 23:54:38', '2021-09-17 00:44:10'),
(4, 1, 1, 1, 'Khu B - Vinhome Symphony', 'khu-b-vinhome-symphony', '3', 0, '2021-09-04 23:54:49', '2021-09-17 00:44:27'),
(5, 3, 2, 1, 'Căn hộ 302', 'can-ho-302', '2', 0, '2021-09-04 23:55:07', '2021-09-17 00:44:21'),
(6, 4, 2, 1, 'Căn hộ 186', 'can-ho-186', '4,8', 0, '2021-09-04 23:55:22', '2021-09-05 00:39:47'),
(7, 2, 1, 1, 'khu C - Chung cư Thành phố Thuận', 'khu-c-chung-cu-thanh-pho-thuan', '5,9', 0, '2021-09-04 23:55:46', '2021-09-05 00:40:02'),
(8, 2, 1, 1, 'Khu D - Chung cư Thành phố Thuận', 'khu-d-chung-cu-thanh-pho-thuan', '6,9', 0, '2021-09-04 23:55:56', '2021-09-05 00:40:10'),
(9, 7, 2, 1, 'Căn hộ 200', 'can-ho-200', '1,9', 0, '2021-09-04 23:56:19', '2021-09-05 00:40:16'),
(10, 8, 2, 1, 'Căn hộ 600', 'can-ho-600', '7,11', 0, '2021-09-04 23:56:28', '2021-09-05 00:40:24'),
(11, 0, 0, 28, 'test', 'test', '1,2', 0, '2021-09-14 08:15:54', '2021-09-16 07:17:14'),
(12, 0, 0, 1, '123', '123', '29', 1, '2021-09-20 08:51:58', '2021-09-20 08:55:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `author` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `jobs`
--

INSERT INTO `jobs` (`id`, `author`, `title`, `slug`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tuyển nhân viên kinh doanh', 'tuyen-nhan-vien-kinh-doanh', '<p><strong>Cơ Hội V&agrave; Quyền Lợi</strong><br />Chủ động về thu nhập, thu nhập kh&ocirc;ng giới hạn dựa v&agrave;o năng lực của chuy&ecirc;n vi&ecirc;n hoạch định t&agrave;i ch&iacute;nh (hỗ trợ kinh doanh, thưởng th&aacute;ng, thưởng qu&yacute;, thưởng năm<br />L&agrave;m việc trong m&ocirc;i trường năng động, cơ sở vật chất hiện đại, kh&ocirc;ng gian mở<br />Hỗ trợ đồng phục vest d&agrave;nh cho chuy&ecirc;n vi&ecirc;n hoạch định t&agrave;i ch&iacute;nh mới<br />Được trang bị c&ocirc;ng cụ l&agrave;m việc hiện đại (ipad) nhằm ph&acirc;n t&iacute;ch v&agrave; cung cấp ch&iacute;nh x&aacute;c nhu cầu t&agrave;i ch&iacute;nh của kh&aacute;ch h&agrave;ng<br />Trải nghiệm những sự kiện, chuyến du lịch trong nước, đẳng cấp quốc tế d&agrave;nh cho những c&aacute; nh&acirc;n c&oacute; th&agrave;nh t&iacute;ch xuất sắc</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Y&ecirc;u Cầu C&ocirc;ng Việc</strong><br />Tuổi từ 24-40<br />Tốt nghiệp Cao Đẳng trở l&ecirc;n.<br />Từng l&agrave; nh&acirc;n vi&ecirc;n kinh doanh với it nhất từ 1,2 năm kinh nghiệm&nbsp;<br />Đang l&agrave;m việc v&agrave; sinh sống tại TP. HCM&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<p>&nbsp;</p>\r\n<p><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></p>\r\n<p>1877 Quốc lộ 1k , P An B&igrave;nh Dĩ An B&igrave;nh Dương<br />Điện thoại: 0919909902 - 0888882478 (A. Minh)<br />Email: thicongcontainer@gmail.com</p>', '/laravel-filemanager/photos/1/Tuyển dụng/tuyển-nhân-viên-kinh-doanh.jpg', '2020-10-30 20:32:54', '2020-10-30 20:32:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `media`
--

CREATE TABLE `media` (
  `media_id` bigint(20) UNSIGNED NOT NULL,
  `media_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_extension` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `media_width` double DEFAULT 0,
  `media_height` double DEFAULT 0,
  `media_style` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_size` double NOT NULL DEFAULT 0,
  `mime_type` varchar(170) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_source` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_large` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_alt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_folder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_author` bigint(20) NOT NULL DEFAULT 0,
  `is_delete` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `media`
--

INSERT INTO `media` (`media_id`, `media_name`, `media_title`, `media_extension`, `media_width`, `media_height`, `media_style`, `media_size`, `mime_type`, `media_type`, `media_source`, `media_large`, `media_thumb`, `media_url`, `media_alt`, `media_description`, `media_folder`, `media_author`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'cartoon-avatar-transparent-image_size_374x426', 'cartoon-avatar-transparent-image', 'png', 374, 426, 'landscape', 134221, 'image/png', 'image', 'http://127.0.0.1:8000/uploads/media/cartoon-avatar-transparent-image_size_374x426.png', NULL, 'http://127.0.0.1:8000/uploads/media/cartoon-avatar-transparent-image_size_374x426.png_thumb.png', 'http://127.0.0.1:8000/uploads/media/cartoon-avatar-transparent-image_size_374x426.png', 'cartoon-avatar-transparent-image', '', NULL, 1, 0, '2021-09-20 13:51:59', NULL),
(2, 'mau-can-ho-chung-cu-dep-60m2-amitkedar-1_size_800x533', 'mau-can-ho-chung-cu-dep-60m2-amitkedar-1', 'jpeg', 800, 533, 'landscape', 51586, 'image/jpeg', 'image', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-1_size_800x533.jpeg', NULL, 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-1_size_800x533.jpeg_thumb.jpeg', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-1_size_800x533.jpeg', 'mau-can-ho-chung-cu-dep-60m2-amitkedar-1', '', '1', 1, 0, '2021-09-20 13:52:10', NULL),
(3, 'mau-can-ho-chung-cu-dep-60m2-amitkedar-2_size_800x533', 'mau-can-ho-chung-cu-dep-60m2-amitkedar-2', 'jpeg', 800, 533, 'landscape', 38905, 'image/jpeg', 'image', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-2_size_800x533.jpeg', NULL, 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-2_size_800x533.jpeg_thumb.jpeg', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-2_size_800x533.jpeg', 'mau-can-ho-chung-cu-dep-60m2-amitkedar-2', '', '1', 1, 0, '2021-09-20 13:52:10', NULL),
(4, 'mau-can-ho-chung-cu-dep-60m2-amitkedar-3_size_800x530', 'mau-can-ho-chung-cu-dep-60m2-amitkedar-3', 'jpeg', 800, 530, 'landscape', 35333, 'image/jpeg', 'image', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-3_size_800x530.jpeg', NULL, 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-3_size_800x530.jpeg_thumb.jpeg', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-amitkedar-3_size_800x530.jpeg', 'mau-can-ho-chung-cu-dep-60m2-amitkedar-3', '', '1', 8, 0, '2021-09-20 13:52:10', NULL),
(5, 'mau-can-ho-chung-cu-dep-60m2-zetwix-2_size_600x800', 'mau-can-ho-chung-cu-dep-60m2-zetwix-2', 'jpeg', 600, 800, 'landscape', 55878, 'image/jpeg', 'image', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-2_size_600x800.jpeg', NULL, 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-2_size_600x800.jpeg_thumb.jpeg', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-2_size_600x800.jpeg', 'mau-can-ho-chung-cu-dep-60m2-zetwix-2', '', '3', 1, 0, '2021-09-20 13:52:18', NULL),
(6, 'mau-can-ho-chung-cu-dep-60m2-zetwix-3_size_600x800', 'mau-can-ho-chung-cu-dep-60m2-zetwix-3', 'jpeg', 600, 800, 'landscape', 54052, 'image/jpeg', 'image', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-3_size_600x800.jpeg', NULL, 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-3_size_600x800.jpeg_thumb.jpeg', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-3_size_600x800.jpeg', 'mau-can-ho-chung-cu-dep-60m2-zetwix-3', '', '3', 1, 0, '2021-09-20 13:52:18', NULL),
(7, 'mau-can-ho-chung-cu-dep-60m2-zetwix-4_size_600x800', 'mau-can-ho-chung-cu-dep-60m2-zetwix-4', 'jpeg', 600, 800, 'landscape', 47917, 'image/jpeg', 'image', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-4_size_600x800.jpeg', NULL, 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-4_size_600x800.jpeg_thumb.jpeg', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-60m2-zetwix-4_size_600x800.jpeg', 'mau-can-ho-chung-cu-dep-60m2-zetwix-4', '', '3', 2, 0, '2021-09-20 13:52:18', NULL),
(8, 'images_size_296x171', 'images', 'png', 296, 171, 'landscape', 8826, 'image/png', 'image', 'http://127.0.0.1:8000/uploads/media/images_size_296x171.png', NULL, 'http://127.0.0.1:8000/uploads/media/images_size_296x171.png_thumb.png', 'http://127.0.0.1:8000/uploads/media/images_size_296x171.png', 'images', '', NULL, 8, 0, '2021-09-20 14:16:38', NULL),
(9, 'mau-can-ho-chung-cu-dep-can-ho-nho-muravina-8_size_900x455', 'mau-can-ho-chung-cu-dep-can-ho-nho-muravina-8', 'jpeg', 900, 455, 'landscape', 40019, 'image/jpeg', 'image', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-can-ho-nho-muravina-8_size_900x455.jpeg', NULL, 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-can-ho-nho-muravina-8_size_900x455.jpeg_thumb.jpeg', 'http://127.0.0.1:8000/uploads/media/mau-can-ho-chung-cu-dep-can-ho-nho-muravina-8_size_900x455.jpeg', 'mau-can-ho-chung-cu-dep-can-ho-nho-muravina-8', '', NULL, 2, 0, '2021-09-20 14:52:45', NULL),
(10, 'images_size_296x171-2', 'images', 'png', 296, 171, 'landscape', 8826, 'image/png', 'image', 'http://127.0.0.1:8000/uploads/media/full_images_size_296x171-2.png', 'http://127.0.0.1:8000/uploads/media/large_images_size_296x171-2.png', 'http://127.0.0.1:8000/uploads/media/thumbnail_images_size_296x171-2.png', 'http://127.0.0.1:8000/uploads/media/full_images_size_296x171-2.png', 'images', '', NULL, 1, 0, '2021-09-21 13:28:32', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_02_100001_create_filemanager_table', 2),
(5, '2021_08_09_164408_update_user', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `role_id`, `module`, `created_at`, `updated_at`) VALUES
(12, 29, 'media_manager_access', '2021-09-19 05:43:00', '2021-09-19 05:43:00'),
(13, 29, 'media_setting_access', '2021-09-19 05:43:00', '2021-09-19 05:43:00'),
(14, 29, 'users_get_add', '2021-09-19 05:43:00', '2021-09-19 05:43:00'),
(20, 32, 'media_manager_access', '2021-09-19 06:51:20', '2021-09-19 06:51:20'),
(21, 31, 'users_others_delete', '2021-09-19 08:43:10', '2021-09-19 08:43:10'),
(22, 31, 'permissions_access', '2021-09-19 08:43:10', '2021-09-19 08:43:10'),
(23, 31, 'roles_access', '2021-09-19 08:43:10', '2021-09-19 08:43:10'),
(24, 31, 'roles_get_add', '2021-09-19 08:43:10', '2021-09-19 08:43:10'),
(25, 31, 'departments_access', '2021-09-19 08:43:10', '2021-09-19 08:43:10'),
(26, 31, 'departments_get_add', '2021-09-19 08:43:10', '2021-09-19 08:43:10'),
(396, 34, 'media_manager_access', '2021-09-20 05:41:36', '2021-09-20 05:41:36'),
(397, 34, 'media_setting_access', '2021-09-20 05:41:36', '2021-09-20 05:41:36'),
(539, 33, 'media_manager_access', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(540, 33, 'media_setting_get_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(541, 33, 'media_setting_post_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(542, 33, 'users_access', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(543, 33, 'user_others_access', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(544, 33, 'users_get_add', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(545, 33, 'users_post_add', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(546, 33, 'users_get_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(547, 33, 'user_others_get_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(548, 33, 'users_post_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(549, 33, 'user_others_post_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(550, 33, 'users_delete', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(551, 33, 'users_others_delete', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(552, 33, 'permissions_manager_access', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(553, 33, 'roles_access', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(554, 33, 'roles_get_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(555, 33, 'roles_post_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(556, 33, 'roles_delete', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(557, 33, 'departments_access', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(558, 33, 'departments_get_add', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(559, 33, 'departments_post_add', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(560, 33, 'departments_get_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(561, 33, 'departments_post_edit', '2021-09-20 06:11:23', '2021-09-20 06:11:23'),
(562, 33, 'departments_delete', '2021-09-20 06:11:23', '2021-09-20 06:11:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `policy`
--

CREATE TABLE `policy` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bonus` double NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `policy`
--

INSERT INTO `policy` (`id`, `name`, `bonus`, `created_at`, `updated_at`) VALUES
(1, 'Gold', 10, '2021-08-09 01:53:15', '2021-08-09 01:53:15'),
(2, 'Bronze', 20, '2021-08-09 01:54:33', '2021-08-09 01:54:33'),
(3, 'Sliver', 30, '2021-08-09 01:54:50', '2021-08-09 01:54:50'),
(4, 'Platinum', 40, '2021-08-09 01:55:19', '2021-08-09 01:55:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_delete` int(1) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `is_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super admin', 'super-admin', 0, '2021-09-19 12:13:20', '0000-00-00 00:00:00', NULL),
(29, 'KD 1', 'kd-1', 0, '2021-09-19 12:51:29', '2021-09-19 05:51:29', NULL),
(31, 'KD 2', 'kd-2', 0, '2021-09-19 06:35:34', '2021-09-19 06:35:34', NULL),
(32, 'KD 3', 'kd-3', 0, '2021-09-19 06:51:20', '2021-09-19 06:51:20', NULL),
(33, 'Administrators', 'administrators', 0, '2021-09-19 09:22:16', '2021-09-19 09:22:16', NULL),
(34, 'test', 'test', 1, '2021-09-20 12:42:01', '2021-09-20 05:42:01', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles_group`
--

CREATE TABLE `roles_group` (
  `id` int(11) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_delete` int(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles_group`
--

INSERT INTO `roles_group` (`id`, `department_id`, `role_id`, `is_delete`, `created_at`, `updated_at`) VALUES
(13, 7, 31, 0, '2021-09-19 08:45:01', '2021-09-19 08:45:01'),
(14, 7, 32, 0, '2021-09-19 08:45:01', '2021-09-19 08:45:01'),
(23, 6, 29, 0, '2021-09-20 08:51:35', '2021-09-20 08:51:35'),
(24, 6, 33, 0, '2021-09-20 08:51:35', '2021-09-20 08:51:35');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `setting_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setting_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `setting_name`, `setting_value`) VALUES
(1, 'title_website', 'Demo đối tác'),
(3, 'site_logo', '/laravel-filemanager/photos/1/logo/TAXIVUNGTAU (2) (1).png'),
(4, 'favicon', '/laravel-filemanager/photos/1/logo/TAXIVUNGTAU (2) (1).png'),
(5, 'seo_separator', NULL),
(6, 'site_email', 'tranthaingoc0147@gmail.com'),
(7, 'site_phone', '0899784382'),
(8, 'tawk_to_id', NULL),
(9, 'site_logo_dark', NULL),
(10, 'is_maintenance', '0'),
(11, 'maintenance_content', '<p>Website đang bảo tr&igrave; để n&acirc;ng cấp nhằm n&acirc;ng cao chất lượng dịch vụ. Vui l&ograve;ng quay lại sau. Xin c&aacute;o lỗi v&igrave; sự bất tiện n&agrave;y.</p>'),
(12, 'is_website_notice', '1'),
(13, 'website_notice', '<p>Th&ocirc;ng b&aacute;o: Do hệ thống website đang trong qu&aacute; tr&igrave;nh n&acirc;ng cấp, đ&ocirc;i khi xảy ra c&aacute;c sự cố ảnh hưởng đến truy cập của qu&yacute; kh&aacute;ch h&agrave;ng. K&iacute;nh mong qu&yacute; kh&aacute;ch h&agrave;ng th&ocirc;ng cảm !</p>'),
(14, 'maintenance_allowed_ip', '[]'),
(15, 'maintenance_expired', '2019-08-15T12:00'),
(16, 'site_facebook', NULL),
(17, 'site_description', 'Taxi Vũng Tàu cảm ơn quý khách đã tin tưởng sử dụng dịch vụ di chuyển. Taxi Vũng Tàu là 1 trong những đơn vị taxi tại khu vực Bà Rịa - Vũng Tàu với phương châm an toàn, tiết kiệm, nhanh chóng… đặt mục tiêu trên sự quan tâm của khách hàng, phục vụ tận tâm, cùng với các đối tác tài xế chuyên nghiệp nhiều năm được đào tạo qua các lớp tập huấn nghiệp vụ, đạo đức nghề nghiệp tốt. Với Uy tín nhiều năm trên thị trường chúng tôi luôn sẵn sàng để phục vụ quý khách trên mọi nẻo đường, đi đến nơi về nhà an toàn, trọn niềm vui trên từng chuyến đi.'),
(18, 'site_keywords', 'taxi vũng tàu'),
(19, 'site_default_thumbnail', '/laravel-filemanager/photos/1/logo/TAXIVUNGTAU (2) (1).png'),
(20, 'google_analytics', NULL),
(21, 'logo_slogan', NULL),
(22, 'site_twitter', NULL),
(23, 'site_telegram', NULL),
(24, 'aio_key', '828c310bcb846fc4b04a4d783cd33686'),
(25, 'commission', '20'),
(26, 'seo_use_meta_keyword', '1'),
(27, 'site_address', 'Vũng Tàu'),
(28, 'site_zalo', NULL),
(29, 'site_youtube', NULL),
(30, 'phone_branch_1', '0899784382'),
(31, 'site_address_branch', 'Bà Rịa - Vũng Tàu'),
(32, 'site_description', 'Taxi Vũng Tàu cảm ơn quý khách đã tin tưởng sử dụng dịch vụ di chuyển. Taxi Vũng Tàu là 1 trong những đơn vị taxi tại khu vực Bà Rịa - Vũng Tàu với phương châm an toàn, tiết kiệm, nhanh chóng… đặt mục tiêu trên sự quan tâm của khách hàng, phục vụ tận tâm, cùng với các đối tác tài xế chuyên nghiệp nhiều năm được đào tạo qua các lớp tập huấn nghiệp vụ, đạo đức nghề nghiệp tốt. Với Uy tín nhiều năm trên thị trường chúng tôi luôn sẵn sàng để phục vụ quý khách trên mọi nẻo đường, đi đến nơi về nhà an toàn, trọn niềm vui trên từng chuyến đi.'),
(33, 'site_address_en', 'Giao Chau Building 3rd , 102 Le Hong Phong St, Ward 4, Ba Ria Vung Tau Province.'),
(34, 'site_address_branch_en', '33 Thep Moi St, Ward 12, Tan Binh District, Ho Chi Minh City'),
(35, 'site_description_en', 'Vutatech is a top of the best Blockchain companies. More than 30 Blockchain projects within a year, we are confident to bring you the best solutions with high security level in the shortest time.'),
(37, 'site_linkedin', NULL),
(38, 'site_linkedin', NULL),
(39, 'phone_branch_2', '0899784382'),
(40, 'address_zalo', 'https://zalo.me/0899784382'),
(41, 'address_message', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `setting_media`
--

CREATE TABLE `setting_media` (
  `id` int(11) NOT NULL,
  `setting_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `setting_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `setting_media`
--

INSERT INTO `setting_media` (`id`, `setting_name`, `setting_value`, `created_at`, `updated_at`) VALUES
(1, 'media_large_auto_resize_width', '600', NULL, '2021-09-21 06:26:21'),
(2, 'media_large_auto_resize_height', '500', NULL, '2021-09-21 06:26:21'),
(3, 'media_auto_watermark', 'http://127.0.0.1:8000/storage/media_setting/2021-09-13/488508-facebook-png-clip-art-thumbnail.png', NULL, '2021-09-13 07:30:47'),
(4, 'media_user_permission_delete', '1', NULL, '2021-09-21 06:26:21'),
(5, 'media_thumbnail_auto_resize_width', '300', NULL, '2021-09-21 06:26:21'),
(6, 'media_thumbnail_auto_resize_height', '300', NULL, '2021-09-21 06:26:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `social_users`
--

CREATE TABLE `social_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `social_users`
--

INSERT INTO `social_users` (`id`, `user_id`, `site_name`, `site_link`, `created_at`, `updated_at`) VALUES
(1, 8, 'site_facebook', 'Facebook', NULL, NULL),
(2, 8, 'site_zalo', 'Zalo', NULL, NULL),
(3, 9, 'site_facebook', 'facebook', NULL, NULL),
(4, 10, 'site_facebook', 'facebook', NULL, NULL),
(5, 11, 'site_facebook', 'facebook', NULL, NULL),
(6, 11, 'site_zalo', 'zalo', NULL, NULL),
(7, 12, 'site_facebook', 'facebook', NULL, NULL),
(8, 12, 'site_zalo', 'zalo', NULL, NULL),
(9, 13, 'site_facebook', 'facebook', NULL, NULL),
(10, 14, 'site_facebook', 'facebook', NULL, NULL),
(11, 15, 'site_facebook', 'facebook', NULL, NULL),
(12, 15, 'site_zalo', 'zalo', NULL, NULL),
(13, 1, 'site_facebook', 'facebook', NULL, NULL),
(14, 1, 'site_zalo', 'zalo', NULL, NULL),
(15, 1, 'site_linkedin', 'LinkedIn', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `role` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_media_manager` int(1) NOT NULL DEFAULT 0,
  `is_delete` int(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `image`, `email`, `birthday`, `phone`, `address`, `gender`, `status`, `role`, `permission`, `type`, `policy_id`, `email_verified_at`, `password`, `is_media_manager`, `is_delete`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ThaiNgoc', 'http://127.0.0.1:8000/storage/avatar/2021-09-12/743667-flat-3840x2160-forest-deer-4k-5k-iphone-wallpaper-abstract-11925.jpg', 'admin@gmail.com', '1998-04-14', '0899784312', 'Ba Ria - Vung Taus', 'male', 1, '1', 'super-admin', 'system', NULL, NULL, '$2y$10$yCg34KScd9EXPp2cRJN6LO.VhdcFaHXJQjlTibLJXOHfGAST6rOhe', 1, 0, NULL, '2021-09-20 06:26:21', '2021-09-20 06:26:21'),
(2, 'sale 1', '/storage/avatar/2021-08-31/952492-Cartoon-Avatar-Transparent-Image.png', 'sale1@gmail.com', '2020-11-11', '1234567890', '123', 'male', 1, '33', 'kinh-doanh-2', 'system', NULL, NULL, '$2y$10$pZka.A5lBP4lu9xt2DbTx.qgyvf8u41sMpmB5MYQtcBycXa.uFGde', 0, 0, NULL, '2021-09-19 22:28:24', '2021-09-19 22:28:24'),
(3, 'sale 2', '/storage/avatar/2021-08-31/178262-Cartoon-Avatar-Transparent-Image.png', 'sale2@gmail.com', '2021-01-01', '12345678902', '123', 'male', 1, NULL, 'kinh-doanh-2', 'system', NULL, NULL, '$2y$10$9DnZ5NuWgUwJtIM3svEiTOFTgcxCmRkzfSxz0uMyRbEMu.OY6I8t6', 0, 0, NULL, '2021-08-30 19:43:40', '2021-09-12 05:17:01'),
(4, 'sale 3', '/storage/avatar/2021-08-31/737740-Cartoon-Avatar-Transparent-Image.png', 'sale3@gmail.com', '2021-01-01', '12345678903', '123', 'male', 1, NULL, 'kinh-doanh-1', 'system', NULL, NULL, '$2y$10$YYK1MmJtkg5ZRg6TJGvC1OJ.oB7pQLgyorP.4SaaNWxrMif1LfqOS', 0, 0, NULL, '2021-08-30 19:44:12', '2021-08-30 19:59:38'),
(5, 'sale 4', '/storage/avatar/2021-08-31/881014-Cartoon-Avatar-Transparent-Image.png', 'sale4@gmail.com', '2021-01-01', '12345678904', '123', 'male', 1, NULL, 'kinh-doanh-2', 'system', NULL, NULL, '$2y$10$snqI3RNznOHWarjs/iAW8OtXDZ0r5EwezOBP/6.BhUguDBn5cXvD2', 0, 0, NULL, '2021-08-30 19:53:30', '2021-08-30 20:00:13'),
(6, 'sale 5', '/storage/avatar/2021-08-31/560443-Cartoon-Avatar-Transparent-Image.png', 'sale5@gmail.com', '2021-01-01', '12345678905', '123', 'male', 1, NULL, 'kinh-doanh-2', 'system', NULL, NULL, '$2y$10$SGf6uJ5e2abd6gBk2WKNpOJtdT0ioIfqJxrY.E8S96ILqHASlFBxu', 0, 0, NULL, '2021-08-30 19:54:21', '2021-08-30 20:00:15'),
(7, 'sale 6', '/storage/avatar/2021-08-31/564410-Cartoon-Avatar-Transparent-Image.png', 'sale6@gmail.com', '2021-01-01', '12345678906', '123', 'male', 1, '32', 'kinh-doanh-2', 'system', NULL, NULL, '$2y$10$n/sI9Fz9NlQyq3bxpjCfTOR9h5Rp86nDTSJLW0qKsACk/FXmwVK8O', 0, 0, NULL, '2021-09-19 09:11:35', '2021-09-19 09:11:35'),
(8, 'super mod 1', '/storage/avatar/2021-08-31/218618-Cartoon-Avatar-Transparent-Image.png', 'mod1@gmail.com', '2021-01-01', '12345678907', '123', 'male', 1, '29', 'super-admin', 'system', NULL, NULL, '$2y$10$8nLp0askhfSyKaurbugl8unA8M7DY/cxz44CxHX/dbY6FdO3ExKCu', 1, 0, NULL, '2021-09-19 08:56:56', '2021-09-19 08:56:56'),
(9, 'super mod 2', 'http://127.0.0.1:8000/storage/avatar/2021-09-12/473256-flat-3840x2160-forest-deer-4k-5k-iphone-wallpaper-abstract-11925.jpg', 'mod2@gmail.com', '2021-01-01', '12345678908', '123', 'male', 1, NULL, NULL, 'member', '1', NULL, '$2y$10$bHjVSxqW9ZEW/PT4wyHAk.NRjWIrGw4jzdlhisG4FPovVivAqNMTu', 0, 0, NULL, '2021-09-12 06:40:57', '2021-09-12 06:40:57'),
(28, 'dev', 'http://127.0.0.1:8000/storage/avatar/2021-09-14/331398-facebook-png-clip-art-thumbnail.png', 'dev@gmail.com', '1990-12-12', '1234567890', NULL, 'male', 1, NULL, 'super-admin', 'member', '2', NULL, '$2y$10$Zxn0DwmkFvAR7JHR8kkSU.nH24GfrIFqy4XTJiroQPXsbns3UFj66', 1, 0, NULL, '2021-09-19 21:57:52', '2021-09-19 21:57:52'),
(29, 'test1', 'http://127.0.0.1:8000/storage/avatar/2021-09-19/678278-Cartoon-Avatar-Transparent-Image.png', 'test1@gmail.com', '1999-12-12', '0933411292', 'Ấp Nhân Phước, xã Xuyên Mộc, huyện Xuyên Mộc, tỉnh Bà Rịa-Vũng Tàu', 'male', 0, '1', NULL, 'system', NULL, NULL, '$2y$10$Ookrt0T3KkOSHkyCEiTWBOPt7Rh0bRFLBVwIJO8A/u1hpD8Lu6xwi', 1, 0, NULL, '2021-09-19 22:27:43', '2021-09-19 22:27:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_media`
--

CREATE TABLE `user_media` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'manager, receiver',
  `folder` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user_media`
--

INSERT INTO `user_media` (`id`, `user_id`, `permission`, `type`, `folder`, `created_at`, `updated_at`) VALUES
(1, 4, 'giam-doc-kinh-doanh', 'receiver', 'type-3', '2021-08-28 08:12:14', '2021-08-28 08:12:14'),
(2, 5, 'giam-doc-kinh-doanh', 'receiver', 'type-3', '2021-08-28 08:12:14', '2021-08-28 08:12:14');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `folder`
--
ALTER TABLE `folder`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`),
  ADD UNIQUE KEY `media_id` (`media_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `policy`
--
ALTER TABLE `policy`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles_group`
--
ALTER TABLE `roles_group`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `setting_media`
--
ALTER TABLE `setting_media`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `social_users`
--
ALTER TABLE `social_users`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `user_media`
--
ALTER TABLE `user_media`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `folder`
--
ALTER TABLE `folder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `media`
--
ALTER TABLE `media`
  MODIFY `media_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=563;

--
-- AUTO_INCREMENT cho bảng `policy`
--
ALTER TABLE `policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT cho bảng `roles_group`
--
ALTER TABLE `roles_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT cho bảng `setting_media`
--
ALTER TABLE `setting_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `social_users`
--
ALTER TABLE `social_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `user_media`
--
ALTER TABLE `user_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
