<?php
return [
	[
		'name' => 'dashboard',
		'priority' => 1,
		'title' => 'Bảng điều khiển',
		'url' => '/partner/dashboard',
		'icon' => 'fas fa-tachometer-alt',
	],
	[
		'name' => 'media',
		'priority' => 2,
		'title' => 'Thư viện hình ảnh',
		'url' => '/partner/media',
		'icon' => 'fa fa-image fa-fw',
    ],
    [
		'name' => 'profile',
		'priority' => 2,
		'title' => 'Thông tin cá nhân',
		'url' => '/partner/profile',
		'icon' => 'fa fa-user fa-fw',
    ],
    [
		'name' => 'profile',
		'priority' => 2,
		'title' => 'Danh sách phòng',
		'url' => '/partner/room',
		'icon' => 'fa fa-bed fa-fw',
    ],
	[
		'name' => 'settings',
		'priority' => 102,
		'title' => 'Settings',
		'url' => '/partner/settings',
		'icon' => 'fa fa-cogs fa-fw',
		'sub' => [
			['priority' => 6, 'name' => 'general', 'title' => 'General', 'url' => '/partner/settings'],
			['priority' => 7, 'name' => 'notice', 'title' => 'Notice - Maintenance', 'url' => '/partner/settings/notice'],
			['priority' => 7, 'name' => 'seo', 'title' => 'SEO OnPage', 'url' => '/partner/settings/seo'],
			['priority' => 8, 'name' => 'menu', 'title' => 'Menu', 'url' => '/partner/settings/menu']
		]
	],
];
