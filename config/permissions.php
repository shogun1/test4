<?php

return [
    'modules' => [
        'Media Manager' => ['media_manager_access'],
        'Media Setting' => [''
                            , '', '', '',
                            'media_setting_get_edit', 
                            '',
                            'media_setting_post_edit', 
                            '', '', ''],

        'Users' => ['users_access', 
                    'user_others_access',
                    'users_get_add', 
                    'users_post_add', 
                    'users_get_edit', 
                    'user_others_get_edit',
                    'users_post_edit', 
                    'user_others_post_edit',
                    'users_delete',
                    'users_others_delete'],

        'Permissions' => ['permissions_manager_access'],
        
        'Roles' => ['roles_access', '', 
                    'roles_get_add', 
                    'roles_post_add', 
                    'roles_get_edit', '', 
                    'roles_post_edit', '', 
                    'roles_delete',
                    ''],
                    
        'Departments' => ['departments_access', '',
                        'departments_get_add', 
                        'departments_post_add',
                        'departments_get_edit', '',
                        'departments_post_edit', '',
                        'departments_delete', '']
    ]
];