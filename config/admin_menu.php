<?php
return [
    [
        'name' => 'dashboard_access',
        'priority' => 1,
        'title' => 'Dashboard',
        'url' => '/admin/dashboard',
        'icon' => 'home',
    ],
    [
        'name' => 'location_access',
        'priority' => 1,
        'title' => 'Location',
        'url' => '/admin/location',
        'icon' => 'home',
    ],
    [
        'name' => 'project_access',
        'priority' => 2,
        'title' => 'Projects',
        'url' => '/admin/project',
        'icon' => 'grid',
        'sub' => [
            [
                'priority' => 1,
                'name' => 'all_projects',
                'title' => 'All Projects',
                'url' => '/admin/project/all'
            ],
            [
                'priority' => 5,
                'name' => 'add_project',
                'title' => 'Add Project',
                'url' => '/admin/project/add'
            ],
            [
                'priority' => 15,
                'name' => 'project_group',
                'title' => 'Project Group',
                'url' => '#/admin/project-group/all'
            ],
            [
                'priority' => 20,
                'name' => 'project_tag',
                'title' => 'Project Tag',
                'url' => '#/admin/project-tag/all'
            ],
        ]
    ],
    [
        'name' => 'property_access',
        'priority' => 0,
        'title' => 'Properties',
        'url' => '/admin/property',
        'icon' => 'layers',
        'sub' => [
            [
                'priority' => 1,
                'name' => 'all_properties',
                'title' => 'All Properties',
                'url' => '#/admin/property/all'
            ],
            [
                'priority' => 5,
                'name' => 'add_property',
                'title' => 'Add Property',
                'url' => '#/admin/property/add'
            ],
            [
                'priority' => 10,
                'name' => 'property_type',
                'title' => 'Property Type',
                'url' => '/admin/property-type/all'
            ],
            [
                'priority' => 15,
                'name' => 'property_group',
                'title' => 'Property Group',
                'url' => '/admin/property-group/all'
            ],
            [
                'priority' => 20,
                'name' => 'property_tag',
                'title' => 'Property Tag',
                'url' => '/admin/property-tag/all'
            ],
        ]
    ],
	[
		'name' => 'media_manager_access',
		'priority' => 2,
		'title' => 'Media Manager',
		'url' => '/admin/media',
		'icon' => 'camera',
		'sub' => [
			['priority' => 1, 'name' => 'media_access', 'title' => 'Media', 'url' => '/admin/media'],
            ['priority' => 2, 'name' => 'media_folder_access', 'title' => 'Media Folder', 'url' => '/admin/media/folder'],
			['priority' => 5, 'name' => 'media_setting_get_edit', 'title' => 'Media Setting', 'url' => '/admin/media/setting'],
		]
	],
	[
		'name' => 'users_access',
		'priority' => 20,
		'title' => 'User Manager',
		'url' => '/admin/users',
		'icon' => 'user',
	],
	[
		'name' => 'permissions_manager_access',
		'priority' => 69,
		'title' => 'Permissions',
		'url' => '/admin/permissions',
		'icon' => 'lock',
		'sub' => [
			['priority' => 1, 'name' => 'roles_access', 'title' => 'Roles', 'url' => '/admin/roles'],
			['priority' => 2, 'name' => 'departments_access', 'title' => 'Departments', 'url' => '/admin/departments'],
		]
	]
];
